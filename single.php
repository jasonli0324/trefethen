<?php 
	get_header(); 
	 
	if( have_posts() ) the_post(); 
	
	$id = get_the_ID();
	$title = get_the_title();
	$date = get_the_date();
	$cats = wp_get_post_categories( $id );
	$cat_count = count($cats);
	$photo = get_field('blog_post_photo');
	$photoUrl = $photo['url'];
	$imageSubtext = get_field('blog_post_photo_description');
	$posts = get_field('blog_post_featured_posts');
	$k = 1;
?>

<section class="single-post pb-xxl mb-xxxl-l pb-m-l">
	<div class="mxw-1952 mxw-1162-ds wrapper-small mh-auto ph-m">
		<div class="mb-xl">
			<?php custom_breadcrumbs(); ?>
		</div>
		<?php if ($title) : ?>
			<h1 class="mb-l h2 f-title fw-300 lh2 tc-primary-text"><?php echo $title; ?></h1>
		<?php endif; ?>
		<?php if($cat_count > 0) : ?>
			<ul class="f-secondary lh4 ls-tinier">
				<?php foreach($cats as $cat) : ?>
					<?php $cat_name = get_category($cat)->name; ?>
					<li class="dib"><?php echo $cat_name; ?><?php if($k < $cat_count) echo ', '?></li>
				<?php $k++; endforeach; ?>
			</ul>
		<?php endif; ?>
		<?php if ($date) : ?>
			<p class="mb-xxl f-secondary f6-s lh4 fw-300 ls-tinier"><?php echo $date; ?></p>
		<?php endif; ?>
	</div>
	<div class="mxw-1952 wrapper-small mh-auto ph-m-ns">
		<div class="mb-s-s mb-m bg-cover bg-center single-post__image" style="background-image: url('<?php echo $photoUrl; ?>')"></div>
		<?php if($imageSubtext) : ?>
			<p class="pl-s-s pl-m f-secondary f7 tc-brown"><?php echo $imageSubtext; ?></p>
		<?php endif; ?>
	</div>

	<?php FLEX::blocks(); ?>

	<?php if($posts > 0) : ?>
		<div class="mxw-670 mh-auto mb-xxl mb-xxxl-l ph-m-s pb-m-l">
			<p class="pb-s bdb-1 f-secondary uppercase ls-medium">More from the Journal</p>
			<ul class="mb-xl">
				<?php foreach($posts as $post) : ?>
					<?php 
						$ID = $post->ID;
						$date = get_the_date('', $ID);
						$title = get_the_title($ID);
						$url = get_permalink($ID);
					?>
					<li class="pv-m bdb-1">
						<a href="<?php echo $url; ?>">
							<p class="mb-s f-secondary f7"><?php echo $date; ?></p>
							<h4 class="mb-s tc-link f3 f2-l"><?php echo $title; ?></h4>
						</a>
					</li>
				<?php endforeach; ?>
			</ul>
			<?php $post_type_archive = get_post_type_archive_link('post'); ?>
			<a href="<?php echo $post_type_archive; ?>" class="relative dib lh4 f-secondary fw-300 h-tc-link-hover td-40 featured-link">See All Posts <span class="pl-xs pl-s-ns tc-link icon-link-arrow"></span></a>
		</div>
	<?php endif; ?>
</section>
<?php get_footer(); ?>