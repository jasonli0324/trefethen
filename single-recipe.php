<?php

	if  ( have_posts() ) {
		while( have_posts() ) {
			the_post();
		}
	}

	$postType = get_post_type_object(get_post_type());
	if ($postType) {
		$postType = esc_html($postType->labels->singular_name);
	} else {
		$postType = '';
	}

	get_header();

	$title = get_field('recipe_title');
	$photos = get_field('recipe_photos');
	$photo = $photos['recipe_main_photo'];
	$introduction = get_field('recipe_introduction');
	$instructions = get_field('recipe_instructions');
	$recipe_pdf = get_field('recipe_card');
	$recipe_servings = get_field('recipe_servings');
	$featured_wines = get_field('featured_wines');
	$instructions_amount = count(get_field('recipe_instructions'));
	$suggested_recipes = get_field('suggested_recipes');
?>

<section class="mxw-1952 mxw-1162-ds wrapper-small mh-auto ph-m single-recipe">
	<div class="mb-xxl">
		<?php custom_breadcrumbs(); ?>
	</div>
	<div class="flex-ns pb-xxxl bdb-1">
		<div class="shrink-0 pb-xl-l single-recipe__half-container">
			<h1 class="mb-xl mb-xxxl-l h2 f-title fw-300 tc-primary-text"><?php echo $title; ?></h1>
			<?php if($recipe_servings || $recipe_pdf) : ?>
				<div class="flex mb-xl">
					<?php if($recipe_servings) : ?>
						<p class="shrink-0 w-p-50 f3 f2-l f-italic tc-brown">Serves <?php echo $recipe_servings; ?></p>
					<?php endif; ?>
					<?php if($recipe_pdf) : ?>
						<div class="shrink-0 w-p-50">
							<a href="<?php echo $recipe_pdf; ?>" target="blank" class="f-secondary fw-300 lh4"><span class="pr-s-ns f7-s icon-print"></span> <span class="relative pr-m pr-xl-l f6-s featured-link">Print Recipe</span></a>
						</div>
					<?php endif; ?>
				</div>
			<?php endif; ?>
			<div class="mb-l mb-xl-l f-secondary f6-s lh4 fw-300 fs-small"><?php echo $introduction; ?></div>
			<div class="flex items-center">
				<?php 
					$chef_name = get_field('chef_name', 'options');
					$chef_photo = get_field('chef_photo', 'options');
					$chef_signature = get_field('chef_signature', 'options');
					$chef_contact = get_field('chef_contact', 'options');
				?>
				<?php if($chef_photo): ?>
					<div class="shrink-0 bd-round bg-cover bg-center single-recipe__chef-photo" style="background-image: url('<?php echo $chef_photo; ?>')"></div>
				<?php endif; ?>
				<div class="ml-m">
					<?php if($chef_signature): ?>
						<img src="<?php echo $chef_signature; ?>" class="db max-h-50 mb-xs single-recipe__chef-signature" alt="Chef Signature">
					<?php endif; ?>
					<?php if($chef_contact): ?>
						<a href="mailto:<?php echo $chef_contact; ?>" class="relative pr-m pr-xl-l f-secondary f6-s fw-300 h-tc-link-hover td-40 featured-link">Have a question? Message <?php if($chef_name) { echo $chef_name; } ?></a>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<div class="dn-s shrink-0 single-recipe__half-container">
			<div class="bg-cover bg-center single-recipe__image" style="background-image: url('<?php echo $photo; ?>')"></div>
		</div>
	</div>
	<div class="flex-ns pt-xxl pt-xxxl-l pb-xxxl bdb-1">
		<div class="shrink-0 w-p-40-ns pr-xl-m pr-xxxl-l">
			<?php if( have_rows('recipe_ingredient_list') ) : ?>
				<div class="pb-xl-s">
					<?php while ( have_rows('recipe_ingredient_list') ) : the_row(); ?>
						<p class="mb-m f3 f2-l f-italic tc-brown"><?php the_sub_field('group'); ?></p>
						<?php if( have_rows('ingredients') ) : ?>
							<ul class="mb-xl bullet-list">
								<?php while ( have_rows('ingredients') ) : the_row(); ?>
									<li class="relative mb-xs f-secondary f6-s fw-300"><?php the_sub_field('recipe_ingredient'); ?></li>
								<?php endwhile; ?>
							</ul>
						<?php endif; ?>
					<?php endwhile; ?>
				</div>
			<?php endif; ?>
		</div>
		<div class="shrink-0 w-p-60-ns pr-xxxl-l">
			<?php if( have_rows('recipe_instructions') ) : ?>
				<?php $k = 1; ?>
				<p class="mb-m f3 f2-l f-italic tc-brown">Preparation</p>
				<ul class="mb-xxl mb-xxxl-l pb-m-l single-recipe__instructions">
					<?php while ( have_rows('recipe_instructions') ) : the_row(); ?>
						<li class="relative <?php if ($instructions_amount > $k) { echo 'mb-l'; }?> f6-s fw-300 single-recipe__instructions-item">
							<span class="absolute top-0 left-0 h5"><?php echo $k; ?></span><span class="f-secondary lh4 fs-small"><?php the_sub_field('recipe_step'); ?></span>
						</li>
					<?php $k++; endwhile; ?>
				</ul>
			<?php endif; ?>
			<?php if($featured_wines) : ?>
				<?php 
					$w = 1;
					$wines_amount = count($featured_wines);
				?>
				<p class="mb-m f3 f2-l f-italic tc-brown">Pairs Well With</p>
				<ul>
					<?php foreach($featured_wines as $wine) : ?>
						<?php
							$wineID = $wine;
							$name = get_field('wine_name', $wineID);
							$vintage = get_field('dates', $wineID)['vintage'];
							$photo = get_field('bottle_photos', $wineID)['primary_photo'];
							$url = get_permalink($wineID);
						?>
						<li class="<?php if($wines_amount > $w) {echo 'mb-l mb-xl-l'; } ?> single-recipe__wine">
							<a href="<?php echo $url; ?>" class="flex items-center">
								<div class="mr-xl bg-center bg-cover single-recipe__wine-cover" style="background-image: url('<?php echo $photo; ?>')"></div>
								<h3 class="h5"><?php echo $name; ?> <?php if($vintage) {echo $vintage; }?></h3>
							</a>
						</li>
					<?php $w++; endforeach; ?>
				</ul>
			<?php endif; ?>
		</div>
	</div>
	<?php 
		$recipes_amount = count($suggested_recipes); 
		$r = 1;
	?>
	<?php if($suggested_recipes) : ?>
		<div class="pt-m single-recipe__list">
			<p class="mb-xl pb-s f-secondary ls-medium uppercase">Other recipes you will enjoy</p>
			<div class="flex-l items-center">
				<ul class="w-p-100 flex flex-wrap mb-l-nl pr-xl-l">
					<?php foreach($suggested_recipes as $recipe_id) : ?>
						<?php
							$recipe_photos = get_field('recipe_photos', $recipe_id);
							$recipe_photo = $recipe_photos['recipe_thumbnail_photo'];
							$recipe_title = get_the_title($recipe_id);
							$recipe_link = get_permalink($recipe_id);
						?>
						<li class="w-p-100-nl <?php echo $recipes_amount > $r ? 'mb-xxl-s' : ''; ?>">
							<?php if ($recipe_link) : ?>
								<a href="<?php echo $recipe_link; ?>" class="db mb-m bg-cover bg-center single-recipe__list-image" style="background-image: url('<?php echo $recipe_photo; ?>')"></a>
							<?php else : ?>
								<div class="db mb-m bg-cover bg-center single-recipe__list-image" style="background-image: url('<?php echo $recipe_photo; ?>')"></div>
							<?php endif; ?>
							<?php if ( $recipe_title ) : ?>
								<h3 class="lh2 h5"><?php echo $recipe_title; ?></h3>
							<?php endif; ?>
						</li>
					<?php $r++; endforeach; ?>
				</ul>
				<?php 
					$args = [
						'post_type' => 'page',
						'meta_key' => '_wp_page_template',
						'meta_value' => 'page-recipes.php'
					];
					$parent_page = get_posts( $args )[0];
					$parent_page_url = get_permalink($parent_page->ID);
				?>
        <div class="relative dib-nl <?php echo $recipes_amount < 3 ? 'w-150-l' : 'w-60-l'; ?> single-recipe__list-link">
          <a href="<?php echo $parent_page_url; ?>" class="dib f-secondary f6-nl lh4 fw-300 "><span class="underline">More Recipes</span><span class="<?php echo $recipes_amount < 3 ? 'pl-xs pl-m-ns' : 'pl-xs-nl db-l'; ?> tc-link icon-link-arrow"></span></a>
        </div>
			</div>
		</div>
	<?php endif; ?>
</section>

<?php get_footer(); ?>