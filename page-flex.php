<?php

/**
* Template Name: Flexible Layout
*
* This is the primary template used for most pages as it allows flexible page building with
* the preconfigured content block components.
*/


if ( have_posts() ) while( have_posts() ) the_post();

	$disable_header = get_field('disable_header');
	$disable_footer = get_field('disable_footer');
	get_header();
	
?>

<?php FLEX::style(); ?>

<div class="bg-white page-<?php echo $post->post_name; ?>">
	<?php if(!is_front_page()) : ?>
		<div class="mxw-1952 mxw-1162-ds wrapper-small mh-auto mb-xl ph-m">
			<?php custom_breadcrumbs(); ?>
		</div>
	<?php endif; ?>
	
	<?php FLEX::blocks(); ?>

	<?php 
		$has_email_signup = get_field('has_email_signup');
		if ( $has_email_signup ) {
			FLEX::part('email_signup'); 
		}
	?>
</div>


<?php FLEX::script(); ?>

<?php get_footer(); 