<?php global $theme_text_domain; ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="UTF-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">

	<title><?php the_title(); ?></title>

	<?php // mobile meta 
	?>
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<?php // icons & favicons 
	?>
	<link rel="apple-touch-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/apple-touch-icon.png">
	<link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.png">

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

	<?php // ADD CUSTOM FONT LOAD BELOW 
	?>
	<link rel="stylesheet" href="https://use.typekit.net/oht0cqk.css">

	<?php // wordpress head functions 
	?>
	<?php wp_head(); ?>
	<?php // end of wordpress head 
	?>

	<?php global $post; ?>
	<meta name="robots" content="index, follow">
	<meta property="og:locale" content="en_US" />
	<meta property="og:type" content="website" />
	<meta property="og:title" content="<?php echo get_the_title() . ' - ' . get_field('winery_name', 'option'); ?>" />
	<meta property="og:url" content="<?php the_permalink(); ?>" />
	<meta property="og:site_name" content="<?php echo get_field('winery_name', 'option'); ?>" />
	<meta property="og:image:width" content="1200" />
	<meta property="og:image:height" content="630" />
	<meta name="twitter:card" content="summary_large_image" />

	<meta name="twitter:title" content="<?php echo get_the_title() . ' - ' . get_field('winery_name', 'option'); ?>" />
	<meta name="twitter:site" content="<?php echo get_field('winery_name', 'option'); ?>" />

	<?php
	$address = get_field('address', 'option');
	$business_hours = get_field('business_hours', 'option');
	if (is_front_page()) {

	?>
		<meta property="og:description" content="<?php echo get_field('winery_description', 'option'); ?>" />
		<meta property="og:image" content="<?php echo get_field('winery_image', 'option'); ?>" />
		<meta name="twitter:description" content="<?php echo get_field('winery_description', 'option'); ?>" />
		<meta name="twitter:image" content="<?php echo get_field('winery_image', 'option'); ?>" />
	<?php } ?>
	<script type="application/ld+json">
		{
			"@context": "http://schema.org",
			"@type": [
				"Winery",
				"TouristAttraction"
			],
			"name": "<?php echo get_field('winery_name', 'option'); ?>",
			"description": "<?php echo get_field('winery_description', 'option'); ?>",
			"address": {
				"@type": "PostalAddress",
				"addressLocality": "<?php echo $address['address_2']['city']; ?>",
				"addressCountry": "<?php echo $address['address_2']['state']; ?>",
				"postalCode": "<?php echo $address['address_2']['zip_code']; ?>",
				"streetAddress": "<?php echo $address['street_address']; ?>"
			},
			"publicAccess": true,
			"openingHours": [
				"Monday <?php echo $business_hours['monday']; ?>",
				"Tuesday <?php echo $business_hours['tuesday']; ?>",
				"Wednesday <?php echo $business_hours['wednesday']; ?>",
				"Thursday <?php echo $business_hours['thursday']; ?>",
				"Friday <?php echo $business_hours['friday']; ?>",
				"Saturday <?php echo $business_hours['saturday']; ?>",
				"Sunday <?php echo $business_hours['sunday']; ?>"
			],
			"touristType": [
				"Wine tourism",
				"Cultural tourism"
			],
			"telephone": ["<?php echo get_field('phone', 'option'); ?>"],
			"email": "<?php echo get_field('email_address', 'option'); ?>",
			"image": "<?php echo get_field('winery_image', 'option'); ?>",
			"priceRange": "<?php echo get_field('price_range', 'option'); ?>",
			"servesCuisine": "<?php echo get_field('servesCuisine', 'option') ? 'Yes' : 'No'; ?>"
		}
	</script>
	<script type="application/ld+json">
		{
			"@context": "https://schema.org/",
			"@type": "WebSite",
			"name": "<?php echo get_field('winery_name', 'option'); ?>",
			"url": "<?php the_permalink(); ?>",
			"potentialAction": {
				"@type": "SearchAction",
				"target": "<?php echo home_url(); ?>/?s={search_term_string}",
				"query-input": {
					"@type": "PropertyValueSpecification",
					"valueRequired": true,
					"valueName": "search_term_string"
				}
			}
		}
	</script>
	<?php if ('event' == get_post_type()) {
		$dateTime = get_field('event_date_time', get_the_ID());
		$venue = get_field('venue_details', get_the_ID());
		$ticket = get_field('event_tickets', get_the_ID());
		$title = get_field('event_title', get_the_ID());
	?>
		<meta property="og:description" content="<?php the_field('event_description'); ?>" />
		<meta property="og:image" content="<?php $photos = get_field('event_photos'); echo $photos['event_main_photo']; ?>" />
		<meta name="twitter:description" content="<?php the_field('event_description'); ?>" />
		<meta name="twitter:image" content="<?php $photos = get_field('event_photos'); echo $photos['event_main_photo']; ?>" />
		<script type="application/ld+json">
			{
				"@context": "https://schema.org",
				"@type": "Event",
				"name": "<?php echo $title; ?>",
				"description": "<?php the_field('event_description'); ?>",
				"image": "<?php $photos = get_field('event_photos');
									echo $photos['event_main_photo']; ?>",
				"startDate": "<?php if ($dateTime['all_day_event'] == false) echo $dateTime['event_start_time'];
											else echo $dateTime['event_date'];
											?>",
				"endDate": "<?php if ($dateTime['all_day_event'] == false) echo $dateTime['event_end_time'];
										else echo $dateTime['event_date'];
										?>",
				"eventStatus": "https://schema.org/EventScheduled",
				"eventAttendanceMode": "https://schema.org/OfflineEventAttendanceMode",
				"location": {
					"@type": "Place",
					"name": "<?php echo $venue['event_venue_name']; ?>",
					"address": {
						"@type": "PostalAddress",
						"streetAddress": "<?php echo $venue['event_venue_address']['streetaddress']; ?>",
						"addressLocality": "<?php echo $venue['event_venue_address']['addresslocality']; ?>",
						"addressRegion": "<?php echo $venue['event_venue_address']['addressregion']; ?>",
						"postalCode": "<?php echo $venue['event_venue_address']['postalcode']; ?>",
						"addressCountry": "<?php echo $venue['event_venue_address']['addresscountry']; ?>"
					}
				},
				"offers": {
					"@type": "Offer",
					"name": "<?php echo $title; ?>",
					"price": "<?php echo $ticket['event_cost']; ?>",
					"priceCurrency": "USD",
					"url": "<?php the_permalink(); ?>",
					"validFrom": "2020-01-01",
					"availability": "https://schema.org/InStock"
				},
				"performer": {
					"@type": "PerformingGroup",
					"name": "<?php echo get_field('winery_name', 'option'); ?>"
				},
				"organizer": {
					"@type": "Organization",
					"name": "<?php echo get_field('winery_name', 'option'); ?>",
					"url": "<?php the_permalink(get_the_ID()); ?>"
				}
			}
		</script>
	<?php } ?>
	<?php if ('recipe' == get_post_type()) {
		$photos = get_field('recipe_photos', get_the_ID());
		$steps = get_field('recipe_instructions', get_the_ID());
	?>
		<meta property="og:description" content="<?php echo get_field('recipe_introduction', get_the_ID()); ?>" />
		<meta property="og:image" content="<?php echo $photos['recipe_main_photo'] ?>" />
		<meta name="twitter:description" content="<?php echo get_field('recipe_introduction', get_the_ID()); ?>" />
		<meta name="twitter:image" content="<?php echo $photos['recipe_main_photo'] ?>" />
		<script type="application/ld+json">
			{
				"@context": "https://schema.org/",
				"@type": "Recipe",
				"name": "<?php echo get_field('recipe_title', get_the_ID()); ?>",
				"image": "<?php echo $photos['recipe_main_photo'] ?>",
				"description": "<?php echo get_field('recipe_introduction', get_the_ID()); ?>",
				"keywords": "<?php echo get_field('recipe_title', get_the_ID()); ?>",
				"author": {
					"@type": "Person",
					"name": "<?php the_author(); ?>"
				},
				"datePublished": "<?php the_date(); ?>",
				"recipeIngredient": [
					<?php $ingredient_lists = get_field('recipe_ingredient_list', get_the_ID());
					$ingredient_string = array();
					foreach ($ingredient_lists as $ingredients) :
						foreach ($ingredients['ingredients'] as $ingredient) :
							$ingredient_string[] = '"' . addslashes($ingredient['recipe_ingredient']) . '"';
						endforeach;
					endforeach;
					echo implode(",", $ingredient_string);
					?>
				],
				"recipeInstructions": [
					<?php
					$recipeInstructions = array();
					foreach ($steps as $step) {
						$recipeInstructions[] =
							'{
								"@type": "HowToStep",
								"text": "' . $step['recipe_step'] . '"
							}';
					}
					echo implode(",", $recipeInstructions);
					?>
				],
				"aggregateRating": {
					"@type": "AggregateRating",
					"ratingValue": "5",
					"bestRating": "5",
					"worstRating": "0",
					"ratingCount": "1"
				}
			}
		</script>
	<?php } ?>
	<?php if ('wine' == get_post_type()) {
		$is_gift_card = (get_field('wine_card_type', get_the_ID()) == 'featured');
		$bottle_photos = get_field('bottle_photos', get_the_ID());
		$gift_pack_photos = get_field('gift_pack_photos', get_the_ID());
		$vivino_rating = get_field('vivino_rating', get_the_ID());
		$vivino_count = get_field('vivino_count', get_the_ID());
		$dates = get_field('dates');
	?>
		<meta property="og:description" content="<?php echo get_field('description', get_the_ID()); ?>" />
		<meta property="og:image" content="<?php $src = 'standard' == get_field('wine_card_type', get_the_ID()) ? $bottle_photos['primary_photo'] : $gift_pack_photos['gift_pack_image']; echo $src; ?>" />
		<meta name="twitter:description" content="<?php echo get_field('description', get_the_ID()); ?>" />
		<meta name="twitter:image" content="<?php $src = 'standard' == get_field('wine_card_type', get_the_ID()) ? $bottle_photos['primary_photo'] : $gift_pack_photos['gift_pack_image']; echo $src; ?>" />
		<script type="application/ld+json">
			{
				"@context": "https://schema.org/",
				"@type": "Product",
				"name": "<?php if (!$is_gift_card) {
										echo get_field('wine_name', get_the_ID()) . ' ' . $dates['vintage'];
									} else {
										echo get_field('wine_name', get_the_ID());
									}
									?>",
				"image": "<?php
									$src = 'standard' == get_field('wine_card_type', get_the_ID()) ? $bottle_photos['primary_photo'] : $gift_pack_photos['gift_pack_image'];
									echo $src;
									?>",
				"description": "<?php echo get_field('description', get_the_ID()); ?>",
				"brand": "<?php echo get_field('winery_name', 'option'); ?>",
				"sku": "<?php echo get_field('sku', get_the_ID()); ?>",
				"mpn": "<?php echo get_field('sku', get_the_ID()); ?>",
				"offers": {
					"@type": "Offer",
					"url": "<?php the_permalink(get_the_ID()); ?>",
					"priceCurrency": "USD",
					"price": "<?php echo get_field('price', get_the_ID()); ?>",
					"priceValidUntil": "1000",
					"availability": "https://schema.org/InStock",
					"itemCondition": "https://schema.org/NewCondition"
				},
				"aggregateRating": {
					"@type": "AggregateRating",
					"ratingValue": "<?php echo $vivino_rating; ?>",
					"bestRating": "5",
					"worstRating": "0",
					"ratingCount": "<?php echo $vivino_count; ?>",
					"reviewCount": "1"
				}
			}
		</script>
	<?php } ?>
	<?php if ('post' == get_post_type()) { ?>
		<meta property="og:description" content="<?php echo get_field('seo_description', get_the_ID()); ?>" />
		<meta property="og:image" content="<?php echo get_field('blog_post_photo', get_the_ID())['url']; ?>" />
		<meta name="twitter:description" content="<?php echo get_field('seo_description', get_the_ID()); ?>" />
		<meta name="twitter:image" content="<?php echo get_field('blog_post_photo', get_the_ID())['url']; ?>" />
		<script type="application/ld+json">
			{
				"@context": "https://schema.org",
				"@type": "BlogPosting",
				"mainEntityOfPage": {
					"@type": "WebPage",
					"@id": "<?php the_permalink(get_the_ID()); ?>"
				},
				"headline": "<?php the_title(); ?>",
				"description": "<?php echo get_field('seo_description', get_the_ID()); ?>",
				"image": "<?php echo get_field('blog_post_photo', get_the_ID()) ?>",
				"author": {
					"@type": "Person",
					"name": "<?php the_author_meta('display_name', $post->post_author); ?>"
				},
				"publisher": {
					"@type": "Organization",
					"name": "<?php echo get_field('winery_name', 'option'); ?>",
					"logo": {
						"@type": "ImageObject",
						"url": "<?php echo get_field('main_navigation_logo', 'option'); ?>",
						"width": 270,
						"height": 80
					}
				},
				"datePublished": "<?php echo get_the_time('Y-m-d', $post->ID);; ?>",
				"dateModified": "<?php echo get_the_time('Y-m-d', $post->ID);; ?>"
			}
		</script>
	<?php } ?>
	<?php if ('page' == get_post_type() && !is_front_page()) { ?>
		<?php if (get_field('meta_description') != '') { ?>
			<meta property="og:description" content="<?php echo get_field('meta_description'); ?>" />
			<meta name="twitter:description" content="<?php echo get_field('meta_description'); ?>" />
		<?php } ?>
		<?php if (get_field('featured_image') != '') { ?>
			<meta property="og:image" content="<?php echo get_field('featured_image'); ?>" />
			<meta name="twitter:image" content="<?php echo get_field('featured_image'); ?>" />
		<?php } ?>
	<?php } ?>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
	<link href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css" />
	<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
	
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
	<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
	<?php // If defined, add tock code snippet here 
	?>
	<?php if (get_field('use_winedirect_tock', 'options')) : ?>
		<?php echo get_field('reservation_widget', 'options'); ?>
	<?php endif; ?>

	<?php // 3rd Party Integration
	$gtm = get_field('gtm', 'options');
	?>

	<?php if (!$gtm['use_google_tag_manager']) : ?>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<?php
		$google_analytics = get_field('google_analytics', 'options');
		$fb_pixel_id = get_field('facebook_pixel', 'options');
		if (!empty($google_analytics)) : ?>
			<script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo $google_analytics ?>"></script>
			<script>
				window.dataLayer = window.dataLayer || [];

				function gtag() {
					dataLayer.push(arguments);
				}
				gtag('js', new Date());
				gtag('config', '<?php echo $google_analytics ?>');
			</script>
		<?php endif; ?>
		<?php if (!empty($fb_pixel_id)) : ?>
			<!-- Facebook Pixel Code -->
			<script>
				! function(f, b, e, v, n, t, s) {
					if (f.fbq) return;
					n = f.fbq = function() {
						n.callMethod ?
							n.callMethod.apply(n, arguments) : n.queue.push(arguments)
					};
					if (!f._fbq) f._fbq = n;
					n.push = n;
					n.loaded = !0;
					n.version = '2.0';
					n.queue = [];
					t = b.createElement(e);
					t.async = !0;
					t.src = v;
					s = b.getElementsByTagName(e)[0];
					s.parentNode.insertBefore(t, s)
				}(window, document, 'script',
					'https://connect.facebook.net/en_US/fbevents.js');
				fbq('init', '<?php echo $fb_pixel_id; ?>');
				fbq('track', 'PageView');
			</script>
			<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=<?php echo $fb_pixel_id; ?>&ev=PageView&noscript=1" /></noscript>
			<!-- End Facebook Pixel Code -->
		<?php endif; ?>
	<?php endif; ?>

	<!-- google font -->
	<?#php
		#$fonts = get_field('site_fonts', 'options');
		#echo '<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=' . $fonts['headline_font']['font'] . ':300,400,500,600,700,800,900">';
		#echo '<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=' . $fonts['body_font']['font'] . ':300,400,500,600,700,800,900">';
	?>

	<?php
		// prep theme option variables
		$site_base = get_field('site_base_colors', 'options');
		$site_primary = get_field('site_primary_accent', 'options');
		$site_secondary = get_field('site_secondary_accent', 'options');
		$header_colors = get_field('header_colors', 'options');
		if (get_field('anchor_style', 'options') == 'primary') $anchor_style = $site_primary['primary_accent_color'];
		if (get_field('anchor_style', 'options') == 'secondary') $anchor_style = $site_secondary['secondary_accent_color'];
		if (get_field('anchor_style', 'options') == 'inherit') $anchor_style = 'inherit';
	?>

	<?php // below is ABSOLUTELY NEEDED FOR THEME TO WORK CORRECTLY!! 
	?>

	<?php // If defined, add global custom css here 
	?>
	<?php if (get_field('global_custom_css', 'options')) : ?>
		<style>
			<?php echo get_field('global_custom_css', 'options'); ?>
		</style>
	<?php endif; ?>


	<?php
	// check if header disabled
	global $disable_header;
	$body_class = (isset($disable_header) && $disable_header) ? 'header--disabled' : '';
	?>


</head>

<body <?php body_class($body_class); ?>>


	<?php
	if (!isset($disable_header) || (isset($disable_header) && !$disable_header)) FLEX::part('header_simple');  // Uses header nav part defined in /inc/parts/
	?>

	<!-- end page header -->