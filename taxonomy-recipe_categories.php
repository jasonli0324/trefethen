<?php
  /**
   * The main template file
   *
   * This is the most generic template file in a WordPress theme
   * and one of the two required files for a theme (the other being style.css).
   * It is used to display a page when nothing more specific matches a query.
   * E.g., it puts together the home page when no home.php file exists.
   *
   * @link https://codex.wordpress.org/Template_Hierarchy
   *
   * @package Highway29Creative
   */

  get_header(); 
?>

<section>
  <div class="mxw-1952 mxw-1162-ds wrapper-small mh-auto mb-xxl ph-m">
    <?php custom_breadcrumbs(); ?>
  </div>
  <?php 
    $title = single_cat_title('', false);
    $intro = term_description();
    include( locate_template( 'inc/partials/page_header/page_header.php', false, false ) ); 
  ?>
	<div class="mxw-1952 mxw-1162-ds wrapper-small mh-auto ph-m">
		<?php 
      $shop_filters = array(get_term_by('slug', 'wines', 'recipe_categories'));
      $recipes_filter = true;
      $current_recipe_cat = get_query_var( 'term' );
      $cat_parent_id = get_term_by('slug', $current_recipe_cat, 'recipe_categories')->parent;
      $current_cat_parent = get_term($cat_parent_id)->slug;
		?>
		<?php include( locate_template( 'inc/partials/shop_filters/shop_filters.php', false, false ) ); ?>
	</div>
  <?php 
    if (have_posts()) :
  ?>
    <div class="mxw-1952 mxw-1162-ds wrapper-small mh-auto mb-xxl mb-xxxl-l pb-m-l ph-m">
      <ul id="category-post-content" class="flex flex-wrap recipes-list">
        <?php 
          while(have_posts()) : the_post(); 
            $title = get_field('recipe_title');
            $url = get_permalink();
            $thumbnail = get_field('recipe_photos')['recipe_thumbnail_photo'];
            $categories = wp_get_post_terms($post->ID, $taxonomy);
            $categories_amount = count($categories);
            $j = 1;
          
            include( locate_template( 'inc/partials/recipe_card/recipe_card.php', false, false ) );
          endwhile; 
          wp_reset_postdata(); 
        ?>
      </ul>
      <ul id="category-post-content-ajax" class="flex flex-wrap recipes-list"></ul>
    </div>
	<?php endif; ?>
	
  <?php FLEX::blocks(); ?>
</section>

<?php get_footer();?>
