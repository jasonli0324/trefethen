<?php 

	/**
	* Template Name: Events Index Layout
	*
	* This is the template used for Shop page
	*/

	get_header();

?>

<section class="mb-xxl mb-xxxl-l pb-m-l">
  <div class="mxw-1952 mxw-1162-ds wrapper-small mh-auto mb-xl ph-m">
    <?php custom_breadcrumbs(); ?>
  </div>
  <?php include( locate_template( 'inc/partials/page_header/page_header.php', false, false ) ); ?>
  <?php
    $members_only = get_field('show_only_member_events');
    
    $post_args = array(
      'post_type' => 'event',
      'posts_per_page' => -1,
      'meta_query'	=> array()
    );
    
    if ($members_only) {
      $members_arg = array(
        'key'	  	=> 'members_only',
        'value'	  	=> true,
        'compare' 	=> 'LIKE',
      );

      array_push($post_args['meta_query'], $members_arg);
    }

    $events = new WP_Query($post_args);
    if ($events->have_posts()) :
  ?>
    <ul class="flex flex-wrap mxw-1952 mxw-1162-ds wrapper-small mh-auto ph-m events-list">
      <?php while($events->have_posts()) : $events->the_post(); ?>
        <?php 
          $title = get_field('event_title');
          $url = get_permalink();
          $thumbnail = get_field('event_photos')['event_thumbnail_photo'];
          $date = get_field('event_date_time')['event_date'];
          $no_year_date = explode(',', $date)[0];
          $place_name = get_field('venue_details')['event_venue_name'];
          $place_address_arr = get_field('venue_details')['event_venue_address'];
          $streetaddress = $place_address_arr['streetaddress'];
          $addresslocality = $place_address_arr['addresslocality'];
          $addressregion = $place_address_arr['addressregion'];
          $postalcode = $place_address_arr['postalcode'];
          $addresscountry = $place_address_arr['addresscountry'];
          $place_address = $streetaddress . ' ' . $addresslocality . ', ' . $addressregion . ' ' . $postalcode . ', ' . $addresscountry;
          $address_empty = empty($streetaddress) && empty($addresslocality) && empty($addressregion) && empty($postalcode) && empty($addresscountry);
          ?>
        <li class="w-p-100-nl mb-xxl mb-xxxl-l pb-m-l">
          <a href="<?php echo $url; ?>">
            <?php if ($thumbnail) : ?>
              <div class="mb-m mb-l-l bg-cover bg-center events-list__image" style="background-image: url('<?php echo $thumbnail; ?>')"></div>
            <?php endif; ?>
            <?php if ($date) : ?>
              <p class="mb-s f-secondary tc-link lh4"><?php echo $no_year_date; ?></p>
            <?php endif; ?>
            <?php if ($title) : ?>
              <h2 class="mb-m h5 events-list__title"><?php echo $title; ?></h2>
            <?php endif; ?>
					<?php $is_home = strpos($place_name, 'Trefethen'); ?>
            <?php if ($address_empty) : ?>
              <span class="f-secondary f6-s fw-300 lh4 ls-tinier"><span class="dib mr-s <?php echo is_numeric($is_home) ? 'icon-trefethen-location f3' : 'icon-location' ?>"></span><?php echo $place_name; ?></span>
            <?php else : ?>
              <a href="https://maps.google.com/?q=term<?php echo $place_address; ?>" target="blank" class="f-secondary f6-s fw-300 lh4 ls-tinier"><span class="dib mr-s <?php echo is_numeric($is_home) ? 'icon-trefethen-location f3' : 'icon-location' ?>"></span><?php echo $place_name; ?></a>
            <?php endif; ?>
          </a>
        </li>
      <?php endwhile; wp_reset_postdata(); ?>
    </ul>
  <?php endif; ?>

  <?php FLEX::blocks(); ?>
</section>

<?php get_footer();?>