<?php 

	/**
	* Template Name: Shop Layout
	*
	* This is the template used for Shop page
	*/

	get_header();

	$post_args = array(
		'post_type' => 'wine',
		'posts_per_page' => -1,
		'meta_query'	=> array(
			'relation'		=> 'AND',
			array(
				'key' => 'wine_card_type',
				'value' => 'library',
				'compare' => 'NOT LIKE',
			),
		)
	);
	$wines = get_posts( $post_args );
	$wine_types = array();
	$wine_varietals = array();
	$vintage = array();
	$prices = array();
  $wine_sizes = array();
	// $page = get_page_by_path('wine');
	foreach ($wines as $wine) {
		$has_sku = get_field('sku', $wine->ID);
		if($has_sku) {
			$wine_card_type = get_field('wine_card_type', $wine->ID);
			
			$price = get_field('price', $wine->ID);
			if ($price !== 0) array_push($prices, $price);

			if($wine_card_type === 'featured') {
				array_push($wine_types, 'Gifting');
				continue;
			}
			
			$type_varietal = get_field('type_varietal', $wine->ID);
			$wine_size = get_field('type_varietal', $wine->ID)['size'];
			array_push($wine_sizes, $wine_size);

			$type_varietal_object = get_field_object('type_varietal', $wine->ID);
			for ($i = 1; $i < count($type_varietal_object['sub_fields']); $i++) {
				if ($type_varietal_object['sub_fields'][$i]['name'] !== 'size') {
					$conditional_logic = $type_varietal_object['sub_fields'][$i]['conditional_logic'];
					if (is_array($conditional_logic) && $type_varietal['wine_type'] === $conditional_logic[0][0]['value']) {
						if ($type_varietal_object['sub_fields'][$i]['name'] === 'rose_varietals' && $type_varietal[$type_varietal_object['sub_fields'][$i]['name']] ==='Pinot Noir') {
							continue;
						}
						
						$varietal = array(
							'key' => $type_varietal_object['sub_fields'][$i]['name'],
							'title'=> $type_varietal[$type_varietal_object['sub_fields'][$i]['name']]
						);
						array_push($wine_varietals, $varietal);
					};
				};
			}
			$dates = get_field('dates', $wine->ID);

			if($dates) {
				array_push($vintage, $dates['vintage']);
			}
			array_push($wine_types, $type_varietal['wine_type']);
		}
	}

	$wine_varietals = array_unique($wine_varietals, SORT_REGULAR);
	$wine_types = array_unique($wine_types);
	rsort($wine_types, SORT_STRING);
	$item = array_splice($wine_types, 1, 2);
	array_splice( $wine_types, 3, 0, $item );
	$item2 = array_splice($wine_types, 4);
	array_splice( $wine_types, 2, 0, $item2 );

  $wine_sizes = array_unique($wine_sizes);
	$vintages = array_unique($vintage);
	$prices = array_unique($prices);
	$prices = array_filter($prices);
	$max = max($prices);
	$min = min($prices);
	// $min = min($prices) ? min($prices) : 0;
	$shop_filters = get_field('shop_filters', $post->ID);
	$shop_banners = get_field('shop_banners', $post->ID);
	$count = 0;

	$page_title = get_field('shop_headline', $post->ID);
	$page_intro = get_field('shop_copy', $post->ID);
	
	$has_cross_promo = get_field('shop_cross_promotion');
	if ($has_cross_promo) {
		$cross_promo = get_field('shop_cross_promotion')['cross_promotion'];
	}
?>
<div class="bg-white page-shop">
	<div class="js-loading-animation fixed top-0 left-0 w-p-100 vh-100 td-40 hidden loading-animation">
		<div class="flex justify-center items-center h-p-100">
			<div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
		</div>
	</div>
	<div class="mxw-1952 mxw-1162-ds wrapper-small mh-auto ph-m">
		<div class="mb-xl"><?php custom_breadcrumbs(); ?></div>
		<?php if($page_title): ?>
			<h1 class="mb-l-nl mb-s h1 f-title fw-300 tc-primary-text"><?php echo $page_title;?></h1>
		<?php endif;?>
		<?php if($page_intro): ?>
			<div class="w-p-100 mxw-670 mb-m mb-l-m mb-xl-l tc-brown f3 f2-l"><?php echo $page_intro;?></div>
		<?php endif;?>
		<div class="js-initial-prices" data-min="<?php echo $min; ?>" data-max="<?php echo $max; ?>"></div>
		<?php include( locate_template( 'inc/partials/shop_filters/shop_filters.php', false, false ) ); ?>
  </div>
	<div id="category-post-content" class="page-shop__content">
		<section class="mxw-1952 mxw-1162-ds wrapper-small mh-auto ph-m">
			<section class="flex justify-between-nl flex-wrap page-shop__shop-list">
				<?php 
					$has_banner = false; 
					
					foreach($wines as $index => $wine) : 
						$wineID = $wine->ID;
						$vivino_rating = get_field('vivino_rating', $wineID);
						$vivino_count = get_field('vivino_count', $wineID);
						$price = get_field('price', $wineID);
						$dates = get_field('dates', $wineID);
						$bottle_photos = get_field('bottle_photos', $wineID);
						$gift_pack_photos = get_field('gift_pack_photos', $wineID);
						$badge_message = get_field('badge_message', $wineID);
						$vivino_url = get_field('vivino_score', $wineID);
						$wine_sku = get_field('sku', $wineID);
						
						if($wine_sku) {
							include( locate_template( 'inc/partials/wine_card/wine_card.php', false, false ) );
						}
						
						if ($has_cross_promo && get_field('wine_card_type', $wineID) === 'featured' && !$has_banner ) : 
							$block_class_identifier = 'cross-promotion';
							$block_class_identifier = 'cross-promotion';
							$promotion_copy = $cross_promo['cross_promotion_copy'];
							$promotion_image = $cross_promo['cross_promotion_image'];
							$link = $cross_promo['cross_promotion_link'];
							$promotion_link_copy = $link['title'];
							$promotion_link_url = $link['url'];
							$promotion_full_width = $cross_promo['cross_promotion_full_width'];	
				?>
			</section>
		</section>
		
		<div class="content-block">
			<?php include( locate_template( 'inc/partials/cross_promotion/cross_promotion.php', false, false ) ); ?>
		</div>

		<section class="mxw-1952 mxw-1162-ds wrapper-small mh-auto ph-m">
			<section class="flex justify-between-nl flex-wrap page-shop__shop-list">
				<?php 
						$has_banner = true; 
						endif; 
					endforeach;
				?>
			</section>
		</section>
	</div>
	<div id="category-post-content-ajax" class="flex justify-between-nl flex-wrap mxw-1952 mxw-1162-ds wrapper-small mh-auto ph-m page-shop__content"></div>
	
	<?php FLEX::blocks(); ?>

	<?php 
		$has_email_signup = get_field('has_email_signup');
		if ( $has_email_signup ) {
			FLEX::part('email_signup'); 
		}
	?>
</div>
<?php get_footer();?>
