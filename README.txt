
THEME GENERAL INFORMATION
--------------------------------------------------------------------
Created by: Highway 29 Creative




INTRODUCTION
----------------------------------

This theme was built by Highway 29 Creative, this readme file is to explain the general setup of the theme and how modifications should be approached.  Please note that there are README.txt files through the theme build to help you better understand the structure and functionality.  It is our hope that this theme should be easy to maintain and modify by any competent web developer.




DEPENDANCIES
----------------------------------

The Advanced Custom Field PRO plugin (Version 5.6.7 or above) is absolutely essential to the functionality of this theme.  It will not function properly without ACF PRO installed and activated.

This theme is built as an NPM project and does utilize some scripts and dev dependencies for enhanced built functionality, HOWEVER these functions are not absolutely required in order to work with and modify this theme.  If not utilizing NPM and the scripts defined in package.json, then the only functionality you have to duplicate is the ability to compile SASS scss files (located in /assets/styles/scss/) and minify and combine Javascript files (located in /assets/javascript/).  See README files in those respective directories for more details on their structure and possible alternatives to using NPM.

To use the pre-configured build tools you need to do a couple things first.  First NPM must be installed.

https://www.npmjs.com/get-npm

Once NPM is installed navigate to the theme directory in terminal or a command line tool and enter the following command.

npm install

When you do this the script will create a 'node_modules' directory in the theme root and download a number of files (a good bit actually).  The files in here are development dependencies only and can be ignored via gitignore if the theme is in a repository.  The 'node_modules' folder also does NOT need to be on the web server as none of these files are accessed in that manner, they are only used on a local computer for running the various build scripts.

Once dependencies are loaded you can run any of the scripts defined in the package.json file in the root of this theme.  Script options are...

npm run scss			-> compiles assets/styles/scss/style.scss
npm run scss:login		-> compiles assets/styles/scss/login-styles.scss
npm run scss:editor		-> compiles assets/styles/scss/editor-style.scss
npm run scss:acf		-> compiles assets/styles/scss/acf-admin-style.scss

npm run js 				-> combines and minifies javascript files in assets/javascrpt/src (see js readme)

npm run watch:js 		-> watches for changes in js src folder and then runs 'npm run js'
npm run watch:scss 		-> watches for changes in scss folder and then runs 'npm run scss'




OVERVIEW
----------------------------------

This theme is built upon the concept of reusable content blocks.  Utilizing the ACF flexible content functionality, blocks are built that can be used in different arrangements throughout the site.  Most content on the site (with the exception perhaps of the post type singles) is displayed with the use of these content blocks.  Blocks installed in the theme can be found in /inc/blocks/.

ACF JSON is used for the definition of all ACF groups.  Utilization of the JSON file improves site performance by reducing DB queries.  These JSON defined groups are accessible and editable by using the ACF Sync functionality.  Once synced a group can be modified through the WP admin interface.  Please note however that once updated through the admin interface the JSON file will be modified, which might cause git conflicts if ACF field modification is done on a staging or production site that is typically updated through git pushes.




STRUCTURE
----------------------------------

Only three primary directories should be located off the root of the theme.  These directories and sub directories are...

	/ acf-theme-json
	/ assets
		/ fonts
		/ images
		/ javascript
		/ styles
	/ inc
		/ blocks
		/ core
		/ parts


Below is a general description of the purpose of each directory.  There are README.txt files located in most of these directories to provide additional information.


/acf-theme-json  
-->  This directory contains the ACF JSON files used to define all ACF groups.

/assets  
-->  This directory is the home to all global non PHP based content used by the theme.

/assets/fonts  
-->  Theme typography and icon fonts should be stored here.

/assets/images  
-->  Optimized images that are used in the theme are located here, most images should be database defined and changeable through the WP admin interface however images that are hard coded into the theme should be located here.

/assets/javascript
-->  Both source and minified javascript used by the theme is located here.  NOTE that some content blocks might have their own javascript located within the block directories.

/assets/styles
--> All theme style files are located here.  There is a scss folder that contains the SASS build that is then compiled into the various CSS files located in the styles directory.  Special note regarding /assets/styles/scss/blocks/, content blocks that are loaded on the site have their scss file copied from the block directory and then placed here for compilation.



/inc
--> Setup and content block files are located here.

/inc/blocks
--> This is where content blocks are stored.

/inc/core
--> Primary theme setup code located here.

/inc/parts
--> Used for common, non block reusable sections.


