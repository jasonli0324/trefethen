<?php

if  ( have_posts() ) {
	while( have_posts() ) {
		the_post();
	}
}

$postType = get_post_type_object(get_post_type());
if ($postType) {
    $postType = esc_html($postType->labels->singular_name);
}
else $postType = '';

$clubs = get_posts([
	'posts_per_page'     => -1,
	'post_type' => 'club'
]);

?>

<?php get_header(); ?>

<div class="content_block single-club">
	<div class="inner">
		<div class="club-container">
			<h1 class="club-name"><?php echo get_field('club_club_name')?>&nbsp;Club</h1>
			<div class="club-photo" style="background-image: url(<?php $photos = get_field('club_club_photos'); echo $photos['club_main_photo'];?>);"></div>
			<div class="club-information">
				<div class="club-overview">
					<h2>Join Our Family</h2>
					<div class="desc"><?php echo get_field('club_club_description_detail', false, false);?></div>
				</div>
				<div class="club-details">
                    <div class="details">
                        <?php $detail1 = get_field('club_club_detail_1');?>
						<?php if(!empty($detail1)):?>
							<div class="detail-1 detail">
								<div class="card"><?php echo $detail1['club_club_detail_1_line_1'];?></div>
								<div class="cta_text"><?php echo $detail1['club_club_detail_1_line_2'];?></div>
							</div>
						<?php endif;?>
						<?php $detail2 = get_field('club_club_detail_2');?>
						<?php if(!empty($detail2)):?>
							<div class="detail-2 detail">
								<div class="card"><?php echo $detail2['club_club_detail_2_line_1'];?></div>
								<div class="cta_text"><?php echo $detail2['club_club_detail_2_line_2'];?></div>
							</div>
						<?php endif;?>
						<?php $detail3 = get_field('club_club_detail_3');?>
						<?php if(!empty($detail3)):?>
							<div class="detail-3 detail">
								<div class="card"><?php echo $detail3['club_club_detail_3_line_1'];?></div>
								<div class="cta_text"><?php echo $detail3['club_club_detail_3_line_2'];?></div>
							</div>
						<?php endif;?>
					</div>
					<div class="cta-group">
						<a class="button primary" href="<?php echo get_field('club_club_sign_up_link');?>">JOIN CLUB</a>
						<a class="button" href="<?php echo get_field("club_club_sign_up_link")?>">GIFT</a>
					</div>
				</div>
			</div>
		</div>
		<div class="compare-clubs">
			<h2 class="block-title">Compare Clubs</h2>
			<div class="club-names" style="grid-template-columns: repeat(<?php echo sizeof($clubs)?>, 1fr);">
				<?php $first = true; foreach($clubs as $club):?>
				<div class="cta_text club-name <?php echo $first ? 'active' : ''?>" data-wine_id="<?php echo $club->ID?>"><span><?php echo get_field('club_club_name', $club->ID);?></span></div>
				<?php $first = false; endforeach;?>
			</div>
			<div class="comparing-categories" style="grid-template-columns: repeat(<?php echo 1 + sizeof($clubs)?>, 1fr);">
				<div class="compare-option">&nbsp;</div>
				<div class="compare-option">Complementary Tastings with Guests</div>
				<div class="compare-option">Shipping Discounts</div>
				<div class="compare-option">Tickets to Annual Dinner</div>
				<div class="compare-option">Annual Gift</div>
				<div class="compare-option">15% off Online and In-store Purchases</div>
				<div class="compare-option">First Pick of our Lastest Products</div>

				<?php $index = 2; $first = true; ?>
				<?php foreach($clubs as $club):
					$categoryValues = get_field('club_categories', $club->ID); ?>
					<div class="compare-value value-<?php echo $club->ID?> <?php echo $first ? 'active' : ''?>">
						<div class="club-title"><h3><?php echo get_field('club_club_name', $club->ID); ?></h3></div>
						<div class="club-link"><a class="button primary" href="<?php echo get_field('club_club_sign_up_link', $club->ID); ?>">JOIN</a></div>
					</div>
					<div class="compare-value value-<?php echo $club->ID?> <?php echo $first ? 'active' : ''?>">
						<div class="sm_desc"><?php echo $categoryValues['complementary_tastings_with_guests']; ?></div>
					</div>
					<div class="compare-value value-<?php echo $club->ID?> <?php echo $first ? 'active' : ''?>">
						<div class="sm_desc"><?php echo $categoryValues['shipping_discounts']; ?></div>
					</div>
					<div class="compare-value value-<?php echo $club->ID?> <?php echo $first ? 'active' : ''?>">
						<div class="sm_desc"><?php echo $categoryValues['tickets_to_annual_dinner']; ?></div>
					</div>
					<div class="compare-value value-<?php echo $club->ID?> <?php echo $first ? 'active' : ''?>">
						<div class="sm_desc"><?php echo $categoryValues['annual_gift']; ?></div>
					</div>
					<div class="compare-value value-<?php echo $club->ID?> <?php echo $first ? 'active' : ''?>">
						<div class="sm_desc"><?php echo $categoryValues['15%_off_online_and_in-store_purchases'] ? 'Yes' : 'No'; ?></div>
					</div>
					<div class="compare-value value-<?php echo $club->ID?> <?php echo $first ? 'active' : ''?>">
						<div class="sm_desc"><?php echo $categoryValues['first_pick_of_our_lastest_products'] ? 'Yes' : 'No'; ?></div>
					</div>
				<?php $index ++; $first = false; endforeach;?>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>