<?php

if  ( have_posts() ) {
	while( have_posts() ) {
		the_post();
	}
}

$postType = get_post_type_object(get_post_type());
if ($postType) {
    $postType = esc_html($postType->labels->singular_name);
}
else $postType = '';

?>

<?php get_header(); ?>

<div class="content_block single-experience">
	<div class="inner">
		<div class="current-experience">
			<h1 class="title"><?php echo get_field('visit_experience_title');?></h1>
			<div class="highlight-photo" style="background-image: url(<?php $photos = get_field('visit_experience_photos'); echo $photos['visit_experience_main_photo']['url'];?>);"></div>
			<div class="experience-details">
				<div class="details">
					<h2 class="overview">Overview</h2>
					<div class="desc"><?php echo get_field('visit_experience_description', false, false);?></div>
					<h3 class="summary">Quick Summary</h3>
					<?php
						$cost = get_field('visit_experience_cost_summary');
						$payment = get_field('visit_experience_payment_details');
						$size = get_field('visit_experience_party_sizes');
					?>
					<?php if(!empty($cost)):?>
						<div class="sm_desc">COST  <span><?php echo $cost;?></span></div>
					<?php endif;?>
					<?php if(!empty($payment)):?>
						<div class="sm_desc">PAYMENT  <span><?php echo $payment;?></span></div>
					<?php endif;?>
					<?php if(!empty($size)):?>
						<div class="sm_desc">PARTY SIZE <span><?php echo $size;?></span></div>
					<?php endif;?>
				</div>
				<div class="reservation">
					<?php if( get_field('visit_experience_use_winedirect_tock')):?>
						<a class="button primary" href="<?php echo get_field('reservation_link', 'options');?>" target="_blank" data-tock-reserve="true" data-tock-experience="<?php echo get_field('visit_experience_reservation_widget');?>">MAKE A RESERVATION</a>
					<?php else:?>
						<a class="button primary" href="<?php echo get_field('visit_experience_reservation_link')?>" target="_blank">MAKE A RESERVATION</a>
					<?php endif;?>
				</div>
			</div>
		</div>
	</div>
	<div class="other-experiences">
		<div class="inner">
			<h2 class="other">Other Options</h2>
			<?php $experiences = get_posts(
							[
								'posts_per_page' => -1,
								'post_type'      => 'experience',
								'exclude' =>[ get_the_ID() ]
							]
						);?>
			<?php foreach($experiences as $experience):?>
                <?php $photos = get_field('visit_experience_photos', $experience->ID);?>
                <div class="experience">
                    <div class="experience-thumbnail" style="background-image: url(<?php echo $photos['visit_experience_thumbnail_photo']['url'];?>);"></div>
                    <div class="experience-information">
                        <h3 class="title"><?php echo get_field('visit_experience_title', $experience->ID);?></h3>
                        <div class="desc"><?php echo get_field('visit_experience_description', $experience->ID, false);?></div>
                        <div class="sm_desc"><?php echo get_field('visit_experience_cost_summary', $experience->ID);?></div>
                    </div>
                    <div class="cta-group">
						<?php if( get_field('visit_experience_use_winedirect_tock', $experience->ID)):?>
                            <div class="button primary reserve-button">
                                <?php echo get_field('visit_experience_reservation_widget', $experience->ID);?>
                            </div>
                        <?php else:?>
                            <a class="button primary" href="<?php echo get_field('visit_experience_reservation_link', $experience->ID)?>" target="_blank">RESERVE</a>
                        <?php endif;?>
                        <a class="button" href="<?php echo get_permalink($experience->ID);?>">MORE INFO</a>
                    </div>
                </div>
            <?php endforeach;?>
		</div>
	</div>
</div>

<?php get_footer(); ?>