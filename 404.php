<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Highway29Creative
 */

get_header();
if  ( have_posts() ) while( have_posts() ) the_post(); 
?>

<div class="bg-white page-404">
	<?php 
		if( have_rows('content_blocks', 'options') ) :
			// loop through content blocks
				while ( have_rows('content_blocks', 'options') ) : the_row();
				
					$file = 'inc/blocks/' . get_row_layout() . '/' . get_row_layout() . '.php';
		    	if (file_exists(get_stylesheet_directory() . '/' . $file)) include( locate_template( $file, false, false ) ); 
		    	else echo '';

			endwhile;
		else:
			echo '';
		endif;
	?>
</div>

<?php get_footer(); ?>