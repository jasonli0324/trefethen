<?php
/**
 * This file is left as default
 */
 
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Highway29Creative
 */

get_header(); ?>

	<div class="content">

		<!-- Basic container -->
		<section class="content_block">
			<div class="inner">

		<?php
		if ( have_posts() ) : ?>

			<h1 class="page-title"><?php printf( esc_html__( 'Search Results for: %s', 'sfs' ), '<span>' . get_search_query() . '</span>' ); ?></h1>

			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post(); 

				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				?>

				<div class="post" id="post-<?php the_ID(); ?>">

					<h2 class="large nomargin"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>"><?php the_title(); ?></a></h2>
					
					<?php

					if ( function_exists('the_excerpt') && is_search() ) {
						the_excerpt();
					} ?>
					<p>
						<?php the_time('F jS, Y') ?> &nbsp;
					</p>
					
				</div>
				
				<hr>

				<?php
			endwhile;

			the_posts_navigation();

		else :

			//get_template_part( 'template-parts/content', 'none' );
			echo 'no results';

		endif; ?>

			</div>
		</section>

	</div>

<?php
//get_sidebar();
get_footer();