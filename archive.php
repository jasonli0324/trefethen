<?php
  /**
   * The main template file
   *
   * This is the most generic template file in a WordPress theme
   * and one of the two required files for a theme (the other being style.css).
   * It is used to display a page when nothing more specific matches a query.
   * E.g., it puts together the home page when no home.php file exists.
   *
   * @link https://codex.wordpress.org/Template_Hierarchy
   *
   * @package Highway29Creative
   */

  get_header(); 
?>

  <div class="content || blog">
    <div class="banner"><h1><?php single_cat_title(); ?></h1></div>

    <div class="categories">
      <div class="categories_inner">
        <?php 
        /* Display Category List */
        $args = array(
          'taxonomy' => 'category',
          'hide_empty' => 0
        );
        $categories = get_categories($args);
        $active_category = get_queried_object();
        foreach($categories as $category): ?>
          <a href="<?php echo get_category_link($category->term_id)?>" class="category<?php if ($active_category->term_id == $category->term_id) echo ' active'; ?>">
            <div class="icon"><?php echo get_field('fa_icon', 'category_'.$category->term_id) ?></div>
            <?php echo $category->name?>
          </a>
        <?php endforeach; ?>
      </div>
    </div>

    <main id="main" class="site-main || content_block" role="main">
      <div class="inner">

        <div class="posts">
          <?php
          $count = 0;
          if ( have_posts() ) : ?>

            <div class="blog-posts">

              <?php /* Start the Loop */
              while ( have_posts() ) : the_post();
                $count ++;
                $page = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>

                <div class="blog-post<?php echo ($count == 1 && $page == 1) ? ' large' : ''; ?>">

                  <?php if ($count == 1 && $page == 1): ?>
                    <div class="post-thumbnail"><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'full' ); ?></a></div>
                    <div class="post-details">
                      <h2 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                      <div class="post-excerpt"><?php echo (strlen(wp_strip_all_tags(get_the_content())) > 150) ? substr(wp_strip_all_tags(get_the_content()), 0, 150).' ...' : get_the_content(); ?></div>
                      <div class="post-readmore"><a class="button" href="<?php the_permalink(); ?>">Read More</a></div>
                    </div>
                  <?php else: ?>
                    <div class="post-thumbnail"><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'medium_large' ); ?></a></div>
                    <div class="post-categories"><?php the_category(', '); ?></div>
                    <h2 class="post-title"><a href="<?php the_permalink(); ?>"><span class="underline"><?php the_title(); ?></span></a></h2>
                  <?php endif; ?>
                </div>

              <?php endwhile; ?>
            </div>

            <div class="blog-pagination">
              <?php the_posts_pagination( array(
                    'mid_size'  => 2,
                    'prev_text' => __( '<i class="far fa-chevron-left"></i>', $theme_text_domain ),
                    'next_text' => __( '<i class="far fa-chevron-right"></i>', $theme_text_domain ),
                ) ); ?>
            </div>
          <?php else: ?>
            <p class="no-posts">No posts found.</p>
          <?php endif; ?>
        </div>

      </div>

    </main><!-- #main -->
  </div><!-- #primary -->

<?php
get_sidebar();
get_footer();
