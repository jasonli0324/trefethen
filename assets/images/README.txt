
IMAGES DIRECTORY GENERAL USAGE
--------------------------------------------------------------------
Created by: Highway 29 Creative

Optimized images that are used in the theme are located here, most images should be database defined and changeable through the WP admin interface however, images that are hard coded into the theme should be located here.

When possible, try to utilize SVG and compressed JPG format images over PNG's.

These images should be optimized for web delivery.  This means adjusting image size to that which is appropriate and then compressing the image as much as is possible without quality loss.  An excellent on-line resource for this is https://tinypng.com/.
