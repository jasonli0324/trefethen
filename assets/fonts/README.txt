
FONTS DIRECTORY GENERAL USAGE
--------------------------------------------------------------------
Created by: Highway 29 Creative

Theme typography and icon fonts should be stored here unless webfonts are coming from an external source like Typekit or Google Fonts.  Sources like that are actually more ideal then locally stored fonts as they are served from a CDN and if it's a common font might also possibly already be cached on the users device.

For icon use, instead of loading an entire collection of fonts that is considerably larger then needed, consider the use of a custom icon font generator.  Services such as...

https://icomoon.io/
http://fontastic.me/

These services allow you to pick icons from multiple common icon libraries and also allows you to upload SVG files so they can be made part of a custom icon font for your site.

If that is done load the font files here and then update /assets/styles/scss/global/_iconfont.scss and be sure it is imported via the style.scss file.
