/********************************************************
* Document Ready
********************************************************/
jQuery(document).ready(function($) {
    window.$ = jQuery;


    /*************************************************
     Fade out cover once page ready
    *************************************************/
    $('#cover').fadeOut(900);

  


















    /*************************************************
     DO NOT REMOVE
     Check to see if current page has page_init 
     and then if so fires it.
    *************************************************/
    if (typeof page_init == 'function') page_init(); 


    /*************************************************
     DO NOT REMOVE
     Tell content blocks it's go time
    *************************************************/
    $(document).trigger('block_init');


});