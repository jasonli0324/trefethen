var varietal_ids = [];
var vintage_ids = [];
var filtering_price = [];
var min = 0;
var max = 200;
var vin65remote = {
  loadingGraphic: '<img src="https://assetss3.vin65.com/images/loading.gif">',
  product: {
    addToCartForm: function(website) {
      $("[v65remotejs=addToCartForm]").each(function(index, el) {
        var productSKU = "",
          currentForm = "";
        productSKU = $(this).attr("productSKU");
        currentForm = $(this);

        $.getJSON(website + "/index.cfm?method=remote.addToCartForm&productSKU=" + productSKU + "&?callback=?", function(
          response
        ) {
          currentForm.html(response);
          var alternateCheckText = currentForm
            .closest(".js-product")
            .find(".v65-widgetProduct-addToCart form:nth-child(3) .v65-widgetProduct-addToCart-unitDescription")
            .html();
          alternateCheckText = alternateCheckText ? alternateCheckText.replace("/ ", "") : "";
          var price1 = currentForm
            .closest(".js-product")
            .find(".v65-widgetProduct-addToCart form:first-child .v65-widgetProduct-addToCart-price")
            .html();
          var price2 = currentForm
            .closest(".js-product")
            .find(".v65-widgetAddToCartCase .v65-widgetProduct-addToCart-price")
            .html();
          var price3 = currentForm
            .closest(".js-product")
            .find(".v65-widgetProduct-addToCart form:nth-child(3) .v65-widgetProduct-addToCart-price")
            .html();
          var outofStock = currentForm
            .closest(".js-product")
            .find(".v65-widgetProduct-addToCart-outOfStockMessage")
            .html();
          currentForm
            .closest(".js-product")
            .find(".js-prick_check")
            .attr("data-price", price1);
          if (outofStock || currentForm.closest(".js-product").find(".v65-widgetProduct-addToCart").length < 1) {
            currentForm
              .closest(".js-product")
              .find(".add-to-cart-form")
              .html("<h2>Sold Out</h2>");
            currentForm
              .closest(".js-product")
              .find(".js-shop-add-to-cart-container")
              .hide();
            currentForm
              .closest(".js-product")
              .find(".js-card-sold-out")
              .removeClass("dn");
            currentForm
              .closest(".js-product")
              .find(".js-price-display")
              .hide();
            currentForm
              .closest(".js-product")
              .find(".js-wine-price")
              .hide();
          } else {
            if (price2) {
              var quantity = currentForm
                .closest(".js-product")
                .find(".v65-widgetProduct-addToCart-caseQuantity")
                .html();
              if (quantity) {
                $(".js-case_check")[0].dataset.type = quantity;
              }
              currentForm
                .closest(".js-product")
                .find(".js-shop-case-cart")
                .html("Buy a case - " + price2);
              currentForm
                .closest(".js-product")
                .find(".js-case_check")
                .attr("data-price", price2);
              currentForm
                .closest(".js-product")
                .find(".js-price-case")
                .html(price2 + quantity)
                .parent("li")
                .removeClass("dn");
            } else {
              currentForm
                .closest(".js-product")
                .find(".js-case_check")
                .hide();
              currentForm
                .closest(".js-product")
                .find(".js-shop-case-cart")
                .hide();
              currentForm
                .closest(".js-product")
                .find(".js-shop-case-cart-sold-out")
                .removeClass("dn");
              currentForm
                .closest(".js-product")
                .find(".per-case")
                .remove();
            }
            if (price3) {
              currentForm
                .closest(".js-product")
                .find(".js-alternate_check")
                .attr("data-price", price3)
                .html(alternateCheckText);
            }
          }
        });
      });
    }
  },
  usertools: {
    loginWidget: function(website, relocateTo) {
      $.getJSON(website + "/index.cfm?method=remote.loginWidget&relocateTo= " + relocateTo + "&?callback=?", function(
        response
      ) {
        $("[v65remotejs=loginWidget]").html(response);
        if ($(".v65-widgetLogin-login").length > 0) {
          $(".js-sign-in").show();
          $(".js-sign-up").show();
          $(".js-my-account").hide();
          $(".js-logout").hide();
          $(".js-order-history").hide();
          $(".js-my-account-name").hide();
        } else {
          $(".js-sign-in").hide();
          $(".js-sign-up").hide();
          $(".js-my-account").show();
          $(".js-logout").show();
          $(".js-order-history").show();
          $(".js-my-account-name").show();
          var name = $(".v65-widgetLogin-editProfile a")
            .html()
            .substring(6);
          $(".js-my-account-name").html(name + "'s account");
          $(".menu-account-mobile .main-menu-link").html(name + "'s account");
        }
      });
    }
  },
  form: {
    form: function(website, formName, remoteForm) {
      $.getJSON(website + "/index.cfm?method=remote.form&formName=" + formName + "&?callback=?", function(response) {
        $(remoteForm).html(response);
        $("textarea")
          .prev()
          .addClass("withTextarea");
        $(".v65-widgetForm fieldset div input").each(function() {
          $(this).attr(
            "placeholder",
            $.trim(
              $(this)
                .parent()
                .find("label")
                .html()
            )
          );
        });
        $(".v65-widgetForm fieldset div input[type=radio]")
          .parent()
          .find("label")
          .show();
        $(".v65-widgetForm fieldset div input[type=radio]")
          .parent()
          .css("margin-top", "20px");
        $(".v65-widgetForm fieldset div input[type=radio]").css("-webkit-appearance", "radio");
        $(".blk__v65 form button span").html("send");
        $(document).on("submit", $(remoteForm).find("form"), function(event) {
          event.preventDefault();
        });
        $(remoteForm)
          .find("button[type=submit]")
          .click(function() {
            vin65remote.form.processSubmitForm($(this).closest("form"), website);
          });
      });
    },
    processSubmitForm: function(form, website) {
      d = new Date();
      formLabelsAndValues = $(form).serialize();
      formDiv = $(form)
        .find("[name=FormDivID]")
        .val();
      $("#" + formDiv).html(vin65remote.loadingGraphic);
      $.getJSON(
        website + "/index.cfm?method=remote.processForm&" + formLabelsAndValues + "&timeStamp=" + d.getTime() + "&?callback=?",
        function(response) {
          try {
            $("#" + formDiv).hide();
            $("#" + formDiv)
              .replaceWith(response)
              .hide()
              .slideDown("slow");
            $(".error #LastName").attr("placeholder", "Last Name");
            $(".error #FirstName").attr("placeholder", "First Name");
            $(".error #MainPhone").attr("placeholder", "Phone");
            $(".error #Email").attr("placeholder", "Email");
            $(".blk__v65 form button span").html("send");
          } catch (err) {
            remoteForm = $("#" + formDiv).parent();
            $(document).on("submit", $(remoteForm).find("form"), function(event) {
              event.preventDefault();
            });
            $(remoteForm)
              .find("button[type=submit]")
              .click(function() {
                vin65remote.form.processSubmitForm($(this).closest("form"), website);
              });
          }
        }
      );
    }
  },
  cart: {
    init: function(website, dontShowIfZero) {
      var cookieListener = vin65remote.cart._setupCookieCheckListener(website, dontShowIfZero);
      window.addEventListener("message", cookieListener, false);
      vin65remote.cart._loadExternalScripts();
      vin65remote.cart._cookieListenerTimeout(website, dontShowIfZero);
    },
    _setupCookieCheckListener: function(website, dontShowIfZero) {
      thirdPartyCookiesSupported = "";
      return function(event) {
        if (event.origin == "https://assetss3.vin65.com") {
          if (event.data === "WD:3PCunsupported") {
            thirdPartyCookiesSupported = false;
          } else if (event.data === "WD:3PCsupported") {
            thirdPartyCookiesSupported = true;
          }
          vin65remote.cart.modalCart(website, dontShowIfZero);
          vin65remote.cart.addToCart(website);
        }
      };
    },
    _cookieListenerTimeout: function(website, dontShowIfZero) {
      setTimeout(function() {
        if (thirdPartyCookiesSupported === "") {
          // Cookie check hasn't been received yet, default to false and init the cart
          thirdPartyCookiesSupported = false;
          vin65remote.cart.modalCart(website, dontShowIfZero);
          vin65remote.cart.addToCart(website);
        }
      }, 5000);
    },
    _loadExternalScripts: function() {
      $.ajax({
        url: "https://assetss3.vin65.com/js/js.cookie.min.js",
        cache: true,
        dataType: "script"
      }).done(function() {
        $("body").append(
          '<iframe src="https://assetss3.vin65.com/thirdPartyCookieCheck/start.html" class="test" style="display: none"></iframe>'
        );
      });
    },
    modalCart: function(website, dontShowIfZero) {
      var orderID = vin65remote.cart._getOrderID();
      var url = website + "/index.cfm?method=remote.modalCart&dontShowIfZero=" + dontShowIfZero;
      if (thirdPartyCookiesSupported === false) {
        url += "&remoteOrderID=" + orderID + "&thirdPartyCookiesSupported=" + thirdPartyCookiesSupported;
      }
      $.getJSON(url + "&?callback=?", function(response) {
        vin65remote.cart._handleResponse(response);
        $(".view-cart-count").html($(".v65-widgetModalCart-itemCount").html());
      });
    },
    _getOrderID: function() {
      if (Cookies.get("ORDERID")) {
        return Cookies.get("ORDERID");
      } else if (vin65remote.utilities._getQueryStringValue("remoteOrderID")) {
        return vin65remote.utilities._getQueryStringValue("remoteOrderID");
      }
      return "";
    },
    _setOrderID: function(orderID) {
      Cookies.set("ORDERID", orderID, { expires: 7 });
    },
    _addOrderIDToLinks: function(orderID) {
      if (thirdPartyCookiesSupported === false) {
        $("a").each(function() {
          if (location.hostname !== this.hostname && this.hostname.length) {
            $(this).attr("href", function(i, linkURL) {
              if (linkURL.indexOf("remoteOrderID") === -1) {
                return linkURL + (linkURL.indexOf("?") != -1 ? "&remoteOrderID=" + orderID : "?remoteOrderID=" + orderID);
              }
            });
          }
        });
      }
    },
    toggleCart: function() {
      var cartVisible = $(".v65-widgetModalCart-dropdown").is(":visible");
      if (cartVisible == true) {
        vin65remote.cart.hideCart();
      } else {
        vin65remote.cart.showCart();
      }
    },
    showCart: function() {
      $(".v65-widgetModalCart-dropdown").slideDown("slow");
      $("[v65remotejs=modalCart]").addClass(".v65-widgetModalCart-selected");
    },
    hideCart: function() {
      $(".v65-widgetModalCart-dropdown").slideUp("slow");
      $("[v65remotejs=modalCart]").removeClass(".v65-widgetModalCart-selected");
    },
    addToCart: function(website) {
      $(document).ready(function() {
        $(document).on("submit", "[v65remotejs=addToCart]", function(event) {
          event.preventDefault();
          vin65remote.cart.processAddToCart(this, website);
        });
      });
    },
    processAddToCart: function(form, website) {
      try {
        vin65.quickView.closeQuickView();
      } catch (err) {
        /*Fail Silently*/
      }
      formLabelsAndValues = $(form).serialize();
      vin65remote.analytics.recordAddToCart(formLabelsAndValues);
      $(".v65-widgetModalCart-dropdown").hide();
      d = new Date();
      $(".v65-widgetModalCart-dropdown")
        .show()
        .html(vin65remote.loadingGraphic);
      $(".loading-gif")
        .show()
        .html(vin65remote.loadingGraphic);
      var action = $(form).context.action.split("?method=")[1];
      var orderID = vin65remote.cart._getOrderID();
      var url = website + "/index.cfm?method=" + action + "&modalLayout=1&" + formLabelsAndValues + "&timeStamp=" + d.getTime();
      if (thirdPartyCookiesSupported === false) {
        url += "&remoteOrderID=" + orderID + "&thirdPartyCookiesSupported=" + thirdPartyCookiesSupported;
      }
      $.getJSON(url + "&?callback=?", function(response) {
        vin65remote.cart._handleResponse(response);
        $(".loading-gif").hide();
        $(".product-modal").removeClass("dn");
        var str = $(".v65-widgetModalCart-itemCount").html();
        var errorMessage = $(".v65-widgetModalCart-itemMessageError").html();
        if (errorMessage) {
          $(".product-modal h3").html(errorMessage);
        } else {
          $(".product-modal h3").html("Added — Nice Choice!");
        }
        $(".view-cart-count").html(str);
      });
      return false;
    },
    _handleResponse: function(response) {
      if ((thirdPartyCookiesSupported === false) & vin65remote.utilities._isJSON(response)) {
        var parsedResponse = JSON.parse(response);
        $("[v65remotejs=modalCart]")
          .replaceWith('<div v65remotejs="modalCart">' + parsedResponse.content + "</div>")
          .hide();
        vin65remote.cart._setOrderID(parsedResponse.orderID);
        vin65remote.cart._addOrderIDToLinks(parsedResponse.orderID);
      } else {
        $("[v65remotejs=modalCart]")
          .replaceWith('<div v65remotejs="modalCart">' + response + "</div>")
          .hide();
      }
    }
  },
  analytics: {
    recordAddToCart: function(formLabelsAndValues) {
      try {
        if (hasGTM) {
          var eventCallback = function() {};
          $.getJSON("/index.cfm?method=cart.recordAddToCart" + "&timeStamp=" + d.getTime(), formLabelsAndValues, function(
            response
          ) {
            if (typeof response.itemDetails !== "undefined" && typeof response.itemDetails.id !== "undefined") {
              vin65remote.analytics.updateForCart(response, eventCallback);
            }
          });
        }
      } catch (err) {
        console.log(err);
      }
    },
    updateForCart: function(response, eventCallback) {
      if (typeof dataLayer === "undefined") return;

      var analyticsEvent = response.action === "add" ? "addToCart" : "removeFromCart";

      var payload = { event: analyticsEvent, ecommerce: {}, eventCallback: eventCallback };

      payload.ecommerce["currencyCode"] = request.currency;
      payload.ecommerce[response.action] = {
        products: [
          {
            name: response.itemDetails.name,
            id: response.itemDetails.id,
            price: response.itemDetails.price,
            brand: response.itemDetails.brand,
            category: response.itemDetails.category,
            variant: response.itemDetails.variant,
            quantity: response.itemDetails.quantity
          }
        ]
      };
      dataLayer.push(payload);
    }
  },
  utilities: {
    _isJSON: function(string) {
      try {
        JSON.parse(string);
      } catch (e) {
        return false;
      }
      return true;
    },
    _getQueryStringValue: function(key) {
      return decodeURIComponent(
        window.location.search.replace(
          new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"),
          "$1"
        )
      );
    }
  }
};
jQuery(document).ready(function($) {
  var popup_delay = $("#popup_delay").val();
  if ($.cookie("popup") == null) {
    setTimeout(function() {
      $(".pagepopup-container").addClass("modal-show");
      $.cookie("popup", 1, { expires: 1 });
    }, popup_delay * 1000);
  }

  /*********************************************
   * Trade & Media - start
   *********************************************/
  $(".download-all").on("click", function() {
    url = $(this).data("bottle_shot");
    a = document.createElement("a");
    a.style.display = "none";
    a.download = "";
    document.body.appendChild(a);
    if (url) {
      a.href = url;
      a.click();
      window.URL.revokeObjectURL(url);
    }

    url = $(this).data("shelf_talker");
    if (url) {
      a.href = url;
      a.click();
      window.URL.revokeObjectURL(url);
    }

    url = $(this).data("tech_sheet");
    if (url) {
      a.href = url;
      a.click();
      window.URL.revokeObjectURL(url);
    }

    url = $(this).data("label");
    if (url) {
      a.href = url;
      a.click();
      window.URL.revokeObjectURL(url);
    }
  });

  $(".show-more").on("click", function() {
    $(this)
      .closest(".loading-buttons")
      .siblings(".block-cards")
      .children(".extra-card")
      .removeClass("invisible");
    $(this)
      .closest(".loading-buttons")
      .siblings(".block-cards")
      .children(".extra-card")
      .addClass("visible");
    $(this)
      .siblings(".show-less")
      .css("display", "block");
    $(this).css("display", "none");
  });

  $(".show-less").on("click", function() {
    $(this)
      .closest(".loading-buttons")
      .siblings(".block-cards")
      .children(".extra-card")
      .removeClass("visible");
    $(this)
      .closest(".loading-buttons")
      .siblings(".block-cards")
      .children(".extra-card")
      .addClass("invisible");
    $(this)
      .siblings(".show-more")
      .css("display", "block");
    $(this).css("display", "none");
  });
  /*******************************************
   * Trade & Media - end
   *******************************************/

  /************************
   * compare clubs - start
   ************************/
  $(".club-names .club-name").on("click", function() {
    $(".club-names .club-name").removeClass("active");
    $(this).addClass("active");
    $(".comparing-categories .compare-value.active").removeClass("active");
    $(".comparing-categories .value-" + $(this).data("wine_id")).addClass("active");
  });
  /***********************
   * compare clubs - end
   ************************/

  /***************************
   * background video - start
   ***************************/
  $(".video-player").on("click", function() {
    if ($(this).hasClass("icon-pause")) {
      $(this).removeClass("icon-pause");
      $(this).addClass("icon-play");
      if ($(this).hasClass("youtube-video")) {
        $(this)
          .siblings(".video-container")
          .children(".background-video")
          .each(function() {
            this.contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', "*");
          });
      } else if ($(this).hasClass("vimeo-video")) {
        $(this)
          .siblings(".video-container")
          .children(".background-video")
          .each(function() {
            this.contentWindow.postMessage('{"method":"pause"}', "*");
          });
      }
    } else {
      $(this).removeClass("icon-play");
      $(this).addClass("icon-pause");
      if ($(this).hasClass("youtube-video")) {
        $(this)
          .siblings(".video-container")
          .children(".background-video")
          .each(function() {
            this.contentWindow.postMessage('{"event":"command","func":"playVideo","args":""}', "*");
          });
      } else if ($(this).hasClass("vimeo-video")) {
        $(this)
          .siblings(".video-container")
          .children(".background-video")
          .each(function() {
            this.contentWindow.postMessage('{"method":"play"}', "*");
          });
      }
    }
  });
  /*************************
   * background video - end
   *************************/

  /*************************
   * club schedules - start
   *************************/
  $(".js-schedule-btn").on("click", function() {
    $(this)
      .siblings(".js-schedule-list")
      .slideToggle(600);
    $(this)
      .find(".icon-plus")
      .toggleClass("active");
  });
  /*************************
   * club schedules - end
   *************************/

  /*******************************
   * purchase event ticket - start
   *******************************/
  $(".js-shop-add-to-cart button").on("click", function() {
    var str = $(".js-product-image").attr("data-img");
    $(this)
      .closest(".event-atc")
      .find(".v65-widgetProduct-addToCart form:first-child .add-to-cart")
      .click();
    $(".js-product-modal-img").css({
      "background-image": "url(" + str + ")",
      height: "300px",
      margin: "40px",
      "box-sizing": "border-box"
    });
    $(".js-overlay").addClass("active");
  });

  $(".quantity").on("keyup", function() {
    var $this = $(this);
    $this.val($this.val().replace(/[^\d]/g, ""));
    var quantity = $this.val();
    $(this)
      .closest(".event-atc")
      .find(".v65-widgetProduct-addToCart form:first-child .v65-widgetProduct-addToCart-quantity input")
      .val($this.val());
    if ($(".is_sticky").length > 0) {
      $(".quantity")
        .not(this)
        .each(function() {
          $(this).val(quantity);
        });
    }
  });
  /*******************************
   * purchase event ticket - end
   *******************************/

  var is_desktop = window.innerWidth > 1024;
  window.onresize = function() {
    var current_mode = window.innerWidth > 1024;
    if (current_mode && !is_desktop) {
      $(".main-menu").css("display", "flex");
      $("header")
        .siblings()
        .removeClass("d-none");
      $("header")
        .parents()
        .siblings()
        .removeClass("d-none");
      $("body").css("overflow-x", "hidden");
      $("html").css("overflow-x", "hidden");
      is_desktop = !is_desktop;
    } else if (!current_mode && is_desktop) {
      $(".main-menu").css("display", "none");
      $("#menu-btn").attr("checked", false);
      is_desktop = !is_desktop;
    }
  };

  jQuery("#category-post-content-ajax").hide();
  $(".photo-gallery.owl-carousel").owlCarousel({
    smartSpeed: 650,
    nav: false,
    autoplay: true,
    dots: true,
    items: 1,
    loop: true,
    lazyLoad: true
  });

  if (window.innerWidth < 1024) {
    $(".block-cards.owl-carousel").owlCarousel({
      dots: false,
      loop: true,
      stagePadding: 50,
      margin: 10,
      center: true,
      lazyLoad: true,
      responsive: {
        0: {
          items: 1,
          nav: false,
          dots: true
        },
        640: {
          items: 2,
          nav: true,
          dots: true,
          navText: ['<span class="icon icon-arrow-left"></span>', '<span class="icon icon-arrow-right"></span>']
        },
        1024: {
          items: 3,
          nav: false,
          loop: false
        }
      }
    });
  }
  /************************
   * owl carousel - end
   ************************/
  $(".product-modal").addClass("dn");
  min = parseInt($("#rangeSlider").data("min"));
  max = parseInt($("#rangeSlider").data("max"));
  if ($(".is_category_page").length > 0) {
    ajax_filter_category();
  }
  var rating = parseInt($(".js-rating-score span").html());
  $(".js-rating-score span").html(rating.toLocaleString());
  $(".js-rating-counts span").each(function() {
    var rating_wine = parseInt($(this).html());
    $(this).html(rating_wine.toLocaleString());
  });

  /************************
   * Filters - start
   ************************/
  var touchedFilter = false;

  // Mobile behaviour
  $(".js-wine-filter").on("click", function() {
    if ($(window).innerWidth() < 1024) {
      var list = $(this).siblings(".js-filter-dropdown");

      if ($(list).hasClass("active")) {
        $(list).removeClass("active");
        $(this).removeClass("active");
      } else {
        $(".js-filter-dropdown.active").removeClass("active");
        $(".js-wine-filter.active").removeClass("active");
        $(list).addClass("active");
        $(this).addClass("active");
      }

      if ($(list).hasClass("active")) {
        if (!$(".js-filters-container").hasClass("active")) {
          $(".js-filters-container").addClass("active");
        }
      } else {
        $(".js-filters-container").removeClass("active");
      }
    }
  });

  // Desktop behaviour
  $(".js-wine-filter").on("mouseenter", function() {
    if ($(window).innerWidth() >= 1024) {
      var list = $(this).siblings(".js-filter-dropdown");

      if (!$(this).hasClass("active")) {
        $(".js-filter-dropdown.active").removeClass("active");
        $(".js-wine-filter.active").removeClass("active");
        $(list).addClass("active");
        $(this).addClass("active");
      }

      if (!$(".js-filters-container").hasClass("active")) {
        $(".js-filters-container").addClass("active");
      }
    }
  });

  $(".js-filter-dropdown").on("mouseenter", function() {
    if ($(window).innerWidth() > 1024) {
      touchedFilter = true;
    }
  });

  if (!touchedFilter) {
    $(".js-filters-container").on("mouseleave", function() {
      if ($(window).innerWidth() > 1024) {
        $(".js-filter-dropdown.active").removeClass("active");
        $(".js-wine-filter.active").removeClass("active");
        $(".js-filters-container").removeClass("active");
      }
    });
  } else {
    $(".js-filter-dropdown").on("mouseleave", function() {
      if ($(window).innerWidth() > 1024) {
        $(".js-filter-dropdown.active").removeClass("active");
        $(".js-wine-filter.active").removeClass("active");
        $(".js-filters-container").removeClass("active");
      }
    });
  }

  $(".js-vintage-category").click(function() {
    if ($(this).hasClass("js-clicked")) {
      return false;
    } else {
      var currentID = $(this).data("id");
      var str;
      str =
        '<button class="js-filtering-category js-filtering-vintage db mr-m mr-l-l pv-s ph-m bdr-3 f7 tc-link pointer btn-link filter__selected-item" data-id="' +
        currentID +
        '">' +
        $(this).text() +
        '<span class="icon-close va-middle fw-800"></span></button>';
      $(".js-filtering-category-contatiner").append(str);
      ajax_filter_category();
      $(this).addClass("js-clicked tc-primary-text-50");

      if (is_desktop) {
        $(".js-filter-dropdown.active").removeClass("active");
        $(".js-filters-container").removeClass("active");
        $(".js-wine-filter").removeClass("active");
      }
    }
  });

  $(".js-size-category").click(function() {
    if ($(this).hasClass("js-clicked")) {
      return false;
    } else {
      var currentID = $(this).data("id");
      var str;
      str =
        '<button class="js-filtering-category js-filtering-size db mr-m mr-l-l pv-s ph-m bdr-3 f7 tc-link pointer btn-link filter__selected-item" data-id="' +
        currentID +
        '">' +
        $(this).text() +
        '<span class="icon-close va-middle fw-800"></span></button>';
      $(".js-filtering-category-contatiner").append(str);
      ajax_filter_category();
      $(this).addClass("js-clicked tc-primary-text-50");

      if (is_desktop) {
        $(".js-filter-dropdown.active").removeClass("active");
        $(".js-filters-container").removeClass("active");
        $(".js-wine-filter").removeClass("active");
      }
    }
  });

  $(".js-varietal-filter").click(function() {
    if ($(this).hasClass("js-clicked")) {
      return false;
    } else {
      $(".js-loading-animation").removeClass("hidden");
      var currentID = $(this).data("id");
      var currentKey = $(this).data("key");
      var str;
      str =
        '<button class="js-filtering-category js-filtering-varietal db mr-m mr-l-l pv-s ph-m bdr-3 f7 tc-link pointer btn-link filter__selected-item" data-id="' +
        currentID +
        '" data-key="' +
        currentKey +
        '">' +
        $(this).text() +
        '<span class="icon-close va-middle fw-800"></span></button>';
      $(".js-filtering-category-contatiner").append(str);
      ajax_filter_category();
      $(this).addClass("js-clicked tc-primary-text-50");

      if (is_desktop) {
        $(".js-filter-dropdown.active").removeClass("active");
        $(".js-filters-container").removeClass("active");
        $(".js-wine-filter").removeClass("active");
      }
    }
  });

  $(".js-type-filter").click(function() {
    if ($(this).hasClass("js-clicked")) {
      return false;
    } else {
      $(".js-loading-animation").removeClass("hidden");
      var currentID = $(this).data("id");
      var str;
      str =
        '<button class="js-filtering-category js-filtering-type db mr-m mr-l-l pv-s ph-m bdr-3 f7 tc-link pointer btn-link filter__selected-item" data-id="' +
        currentID +
        '">' +
        $(this).text() +
        '<span class="icon-close va-middle fw-800"></span></button>';
      $(".js-filtering-category-contatiner").append(str);
      ajax_filter_category();
      $(this).addClass("js-clicked tc-primary-text-50");

      if (!is_desktop) {
        closeFilterModal();
      }
      $(".js-filter-dropdown.active").removeClass("active");
      $(".js-filters-container").removeClass("active");
      $(".js-wine-filter").removeClass("active");
    }
  });

  $("#rangeSlider").slider({
    range: true,
    min: min,
    max: max,
    values: [min, max],
    slide: function(event, ui) {
      $("#amount").html("$" + ui.values[0] + " - $" + ui.values[1]);
    },
    stop: function(event, ui) {
      var str2;
      var isPrice = true;
      str2 =
        '<span class="js-filtering-category js-filtering-price db mr-m mr-l-l pv-s ph-m bdr-3 f7 tc-link pointer btn-link filter__selected-item" data-low="' +
        ui.values[0] +
        '" data-upper="' +
        ui.values[1] +
        '">$' +
        ui.values[0] +
        "- $" +
        ui.values[1] +
        '<span class="icon-close va-middle fw-800"></span></span>';
      $(".js-filtering-price").remove();
      $(".js-filtering-category-contatiner").append(str2);
      $(".js-filtering-price .icon-close").on("click", function() {
        $(this)
          .parent()
          .remove();
        ajax_filter_category(isPrice);
      });
      $(".js-price-filter").hide();
      ajax_filter_category(isPrice);
    }
  });

  $(".js-recipe-filter").click(function() {
    if ($(this).hasClass("js-clicked")) {
      return false;
    } else {
      var currentCat = $(this).data("id");
      var currentCatParent = $(this).data("parent");
      var filterArr = [currentCatParent, currentCat];
      var str;
      str =
        '<button class="js-filtering-category js-filtering-vintage db mr-m mr-l-l pv-s ph-m bdr-3 f7 tc-link pointer btn-link filter__selected-item" data-id="' +
        currentCat +
        '"data-parent="' +
        currentCatParent +
        '">' +
        $(this).text() +
        '<span class="icon-close va-middle fw-800"></span></button>';
      $(".js-filtering-category-contatiner").append(str);
      ajax_filter_recipe(filterArr);
      $(this).addClass("js-clicked tc-primary-text-50");

      if (is_desktop) {
        $(".js-filter-dropdown.active").removeClass("active");
        $(".js-filters-container").removeClass("active");
        $(".js-wine-filter").removeClass("active");
      }
    }
  });

  $(".js-clear-filter").click(function() {
    $(".js-filtering-category").each(function() {
      $(".js-clicked").removeClass("js-clicked tc-primary-text-50");
      $(this).remove();
    });
    $(".js-fitler-checkbox input").each(function() {
      $(this).prop("checked", false);
    });
    ajax_filter_category();
    ajax_filter_recipe([]);
  });

  $(document).on("click", ".js-filtering-category .icon-close", function() {
    var currentID = $(this)
      .parent()
      .data("id");
    var currentParent = $(this)
      .parent()
      .data("parent");
    $(this)
      .parent()
      .remove();
    $(".js-filters-container")
      .find("[data-id='" + currentID + "']")
      .removeClass("js-clicked tc-primary-text-50");
    $(".js-type-varietal-category").each(function() {
      if ($(this).data("id") === currentID) {
        $(this).removeClass("js-clicked tc-primary-text-50");
      }
    });
    ajax_filter_category();
    var filterArr = [currentParent, currentID];
    ajax_filter_recipe(filterArr, "remove");
  });

  // Mobile
  $(".js-open-modal").on("click", function() {
    $(".js-filters-container").addClass("opened");
    $(".js-bg-modal").addClass("opened");

    $(".js-individual-filter:first")
      .find(".js-wine-filter")
      .addClass("active");
    $(".js-individual-filter:first")
      .find(".js-filter-dropdown")
      .addClass("active");

    if ($(window).innerWidth() < 1025) {
      var containers = $(".js-filter-dropdown ul");
      var containerHeight = 0;
      $.each(containers, function(index, value) {
        var elHeight = $(value).height();
        containerHeight = containerHeight > elHeight ? containerHeight : elHeight;
      });

      $(".js-filters-container")
        .removeClass("o-0-nl")
        .animate(
          {
            height: containerHeight + 150
          },
          400
        );
    }
  });

  $(".js-bg-modal").on("click", function() {
    closeFilterModal();
  });
  $(".js-close-modal").on("click", function() {
    closeFilterModal();
  });

  function closeFilterModal() {
    if ($(window).innerWidth() < 1025) {
      $(".js-filters-container").animate(
        {
          height: 0
        },
        400,
        function() {
          $(".js-filters-container").removeClass("opened");
          $(".js-bg-modal").removeClass("opened");
          $(".js-filter-dropdown.active").removeClass("active");
          $(".js-wine-filter.active").removeClass("active");
          $(".js-filters-container.active").removeClass("active");
          $(".js-filters-container").addClass("o-0-nl");
        }
      );
    } else {
      $(".js-filters-container").removeClass("opened");
      $(".js-bg-modal").removeClass("opened");
      $(".js-filter-dropdown.active").removeClass("active");
      $(".js-wine-filter.active").removeClass("active");
      $(".js-filters-container.active").removeClass("active");
      $(".js-filters-container").addClass("o-0-nl");
    }
  }
  /************************
   * Filters - end
   ************************/

  $("#menu-btn").change(function() {
    if (this.checked) {
      setTimeout(function() {
        $("header")
          .siblings()
          .addClass("d-none");
      }, 500);
      setTimeout(function() {
        $("header")
          .parents()
          .siblings()
          .addClass("d-none");
      }, 500);
      $("body").css("overflow-x", "visible");
      $("html").css("overflow-x", "visible");
      $(".announcement_bar").removeClass("d-none");
      $(".main-menu").slideDown(500);
    } else {
      $(".main-menu").slideUp(500);
      $("header")
        .siblings()
        .removeClass("d-none");
      $("header")
        .parents()
        .siblings()
        .removeClass("d-none");
      $("body").css("overflow-x", "hidden");
      $("html").css("overflow-x", "hidden");
    }
  });

  $(document).on("keyup", ".quantity", function() {
    var $this = $(this);
    $this.val($this.val().replace(/[^\d]/g, ""));
    $(this)
      .closest(".js-product")
      .find(".v65-widgetProduct-addToCart form:first-child .v65-widgetProduct-addToCart-quantity input")
      .val($this.val());
  });

  $(document).on("keyup", ".per-alternate-quantity", function() {
    var $this = $(this);
    $this.val($this.val().replace(/[^\d]/g, ""));
    $(this)
      .closest(".js-product")
      .find(".v65-widgetProduct-addToCart form:nth-child(3) .v65-widgetProduct-addToCart-quantity input")
      .val($this.val());
  });

  $(document).on("click", ".add-to-cart-btn.per-bottle", function() {
    $(this)
      .closest(".js-product")
      .find(".v65-widgetProduct-addToCart form:first-child .add-to-cart")
      .click();
    $(".js-overlay").addClass("active");
    var str = $(".js-product-image").attr("data-img");
    $(".js-product-modal-img").css({
      "background-image": "url(" + str + ")",
      height: "300px",
      margin: "40px"
    });
  });

  $(".add-to-cart-btn.per-alternate").click(function() {
    $(".v65-widgetProduct-addToCart form:nth-child(3) .add-to-cart").click();
    $(".js-overlay").addClass("active");
    var str = $(".js-product-image").attr("data-img");
    $(".js-product-modal-img").css({
      "background-image": "url(" + str + ")",
      height: "300px",
      margin: "40px"
    });
  });

  $(document).on("click", ".js-card-sold-out > button", function() {
    $(".js-waitlist-modal").addClass("active");
  });

  $(document).on("click", ".js-shop-add-to-cart button", function() {
    if ($(this).hasClass("js-case")) {
      $(this)
        .closest(".js-product")
        .find(".v65-widgetAddToCartCase .add-to-cart")
        .click();
      var str = $(this)
        .closest(".js-product")
        .find(".js-product-image")
        .attr("data-img");
    } else {
      $(this)
        .closest(".js-product")
        .find(".v65-widgetProduct-addToCart form:first-child .add-to-cart")
        .click();
      var str = $(this)
        .closest(".js-product")
        .find(".js-product-image")
        .attr("data-img");
    }
    $(".js-overlay").addClass("active");
    $(".js-product-modal-img").css({
      "background-image": "url(" + str + ")",
      height: "300px",
      margin: "40px"
    });
  });

  $(document).on("click", ".js-card-sold-out > button", function() {
    $(this)
      .closest(".js-product")
      .find(".v65-widgetAddToCartCase .add-to-cart")
      .click();
    var str = $(this)
      .closest(".js-product")
      .find(".js-product-image")
      .attr("data-img");
    $(".js-waitlist-modal").addClass("active");
    $(".js-product-modal-img").css({
      "background-image": "url(" + str + ")",
      height: "300px",
      margin: "40px"
    });
  });

  $(document).on("click", ".js-shop-case-cart", function() {
    $(this)
      .closest(".js-product")
      .find(".v65-widgetAddToCartCase .add-to-cart")
      .click();
    var str = $(this)
      .closest(".js-product")
      .find(".js-product-image")
      .attr("data-img");
    $(".js-product-modal-img").css({
      "background-image": "url(" + str + ")",
      height: "300px",
      margin: "40px"
    });
    $(".js-overlay").addClass("active");
  });

  $(".add-to-cart-btn.per-case").click(function() {
    $(".v65-widgetAddToCartCase .add-to-cart").click();
    $(".js-overlay").addClass("active");
  });

  $(".js-case_check").on("click", function() {
    var context = $(this).parents(".js-product");
    $(context)
      .find(".js-shop-add-to-cart button")
      .addClass("js-case");
  });

  $(".js-prick_check").on("click", function() {
    var context = $(this).parents(".js-product");
    $(context)
      .find(".js-shop-add-to-cart button")
      .removeClass("js-case");
  });

  $(".js-price-option").on("click", function() {
    var price = $(this).attr("data-price");
    var type = $(this).attr("data-type");
    var priceNumber = parseFloat(price.replace("$", ""));
    if ($(this).hasClass("js-case_check")) {
      $(".js-wine-price").text("$" + priceNumber + " " + type);
    } else {
      $(".js-wine-price").text("$" + priceNumber + " / " + type);
    }
  });
  // $("#prick_check").click(function() {
  //   $(".add-to-cart-form").css({ display: "flex" });
  //   $(".per-case").css({ display: "none" });
  //   $(".add-to-cart-alternate").css({ display: "none" });
  // });
  // $("#case_check").click(function() {
  //   $(".add-to-cart-form").css({ display: "none" });
  //   $(".per-case").css({ display: "flex" });
  //   $(".add-to-cart-alternate").css({ display: "none" });
  // });
  // $("#alternate_check").click(function() {
  //   $(".add-to-cart-form").css({ display: "none" });
  //   $(".per-case").css({ display: "none" });
  //   $(".add-to-cart-alternate").css({ display: "flex" });
  // });
  $(".product-modal .icon-close").click(function() {
    $(".overlay").removeClass("active");
    $(".product-modal").addClass("dn");
  });
  $(".js-overlay").click(function(e) {
    if (!($(event.target).hasClass("product-modal") || $(e.target).parents(".product-modal").length)) {
      $(".product-modal .icon-close").click();
    }
  });

  $(".js-close-waitlist").on("click", function() {
    $(".overlay").removeClass("active");
  });

  $(".pagepopup .icon-close").click(function() {
    $(".pagepopup-container").removeClass("modal-show");
  });
  $(".pagepopup .no-thanks").click(function() {
    $(".pagepopup-container").removeClass("modal-show");
  });

  // Set Rating Stars Style
  $(".js-product").each(function(index, element) {
    var str = $(element)
      .find(".js-rating-score")
      .html();
    var el = $(element).find(".js-stars-container");
    if (str) {
      str = str.replace(/\s+/, "");
      var num = parseFloat(str);
      rateStyleJQ(num, el);
    }
  });
  ///////////////////////////

  if (!$(".profile").length) {
    $(".toProfile").hide();
  }
  if (!$(".blk__2columns").length) {
    $(".toNotes").hide();
  }
  if (!$(".recipe").length) {
    $(".toParing").hide();
  }
  $(".toSpec").click(function() {
    $("html,body").animate(
      {
        scrollTop: $(".wine-spec-container").offset().top
      },
      800 //speed
    );
  });
  $(".toProfile").click(function() {
    $("html,body").animate(
      {
        scrollTop: $(".profile").offset().top
      },
      800 //speed
    );
  });
  $(".toNotes").click(function() {
    $("html,body").animate(
      {
        scrollTop: $(".blk__2columns").offset().top
      },
      800 //speed
    );
  });
  $(".toParing").click(function() {
    $("html,body").animate(
      {
        scrollTop: $(".recipe").offset().top
      },
      800 //speed
    );
  });
});
$(document).on("scroll", function() {
  if ($(".product-anchor-nav").length) {
    if ($(this).scrollTop() >= $(".product-anchor-nav").position().top) {
      if (!$("#menu-btn").is(":checked")) {
        $(".sticky-product-container").addClass("sticky-scroll");
      }
    } else {
      $(".sticky-product-container").removeClass("sticky-scroll");
    }
  }
});

var filterObj = {};
function ajax_filter_recipe(filterArr, action) {
  if (filterArr.length > 0) {
    var parent = filterArr[0];
    var child = filterArr[1];
    var setFixedParentCategory = $(".js-individual-filter").attr("data-recipeParentCat");
    var setFixedChildCategory = $(".js-individual-filter").attr("data-recipeCat");

    if (action === "remove") {
      var index = filterObj[parent].indexOf(child);
      filterObj[parent].splice(index, 1);
    } else {
      if (filterObj[parent]) {
        filterObj[parent].push(child);
      } else {
        filterObj[parent] = [child];
      }

      if (setFixedParentCategory && setFixedChildCategory) {
        filterObj[setFixedParentCategory] = [setFixedChildCategory];
      }
    }
  } else {
    filterObj = {};
  }

  if ($(".js-filtering-category").length) {
    $(".js-clear-filter").removeClass("dn");
  } else {
    $(".js-clear-filter").addClass("dn");
  }

  jQuery.ajax({
    type: "POST",
    url: my_ajax_object.ajax_url,
    data: {
      action: "load_recipe_filter",
      recipe_cats: filterObj
    },
    success: function(response) {
      if (response === "show_all") {
        jQuery("#category-post-content").show();
        jQuery("#category-post-content-ajax").hide();
        return false;
      } else {
        jQuery("#category-post-content").hide();
        jQuery("#category-post-content-ajax").show();
        jQuery("#category-post-content-ajax").html(response);
        jQuery(".js-loading-animation").addClass("hidden");
        return false;
      }
    }
  });
}

function ajax_filter_category(isPrice) {
  type_ids = [];
  package_ids = [];
  varietal_ids = [];
  vintage_ids = [];
  size_ids = [];
  filtering_price = [];
  var cat = $(".js-filters-container").attr("data-cat");
  var package = $(".js-filters-container").attr("data-package");
  if (package !== "") {
    package_ids = package.split("-");
  }
  if (cat !== "") {
    type_ids = cat.split("-");
  }
  if ($(".js-filtering-price").length) {
    filtering_price.push($(".js-filtering-price").data("low"));
    filtering_price.push($(".js-filtering-price").data("upper"));
  }
  if ($(".js-filtering-category").length) {
    $(".js-clear-filter").removeClass("dn");
  } else {
    $(".js-clear-filter").addClass("dn");
  }
  $(".js-filtering-varietal").each(function() {
    varietal_ids.push({ key: $(this).data("key"), title: $(this).data("id") });
  });
  $(".js-filtering-type").each(function() {
    type_ids.push($(this).data("id"));
  });
  $(".js-filtering-vintage").each(function() {
    vintage_ids.push($(this).data("id"));
  });
  $(".js-filtering-size").each(function() {
    size_ids.push($(this).data("id"));
  });

  jQuery.ajax({
    type: "POST",
    url: my_ajax_object.ajax_url,
    data: {
      action: "load-filter2",
      type_ids: type_ids,
      varietal_ids: varietal_ids,
      vintage_ids: vintage_ids,
      size_ids: size_ids,
      price_range: filtering_price,
      packages: package_ids
    },
    success: function(response) {
      if (response === "show_all") {
        jQuery("#category-post-content").show();
        jQuery("#category-post-content-ajax").hide();
        console.log(isPrice);
        if (!isPrice) {
          var min = parseInt($(".js-initial-prices").attr("data-min"));
          var max = parseInt($(".js-initial-prices").attr("data-max"));
          $("#rangeSlider")[0].dataset.min = min;
          $("#rangeSlider")[0].dataset.max = max;
          $(".js-filter-dropdown")
            .find("#amount")
            .text("$" + min + " - $" + max);
          $("#rangeSlider").slider("destroy");
          $("#rangeSlider").slider({
            range: true,
            min: min,
            max: max,
            values: [min, max],
            slide: function(event, ui) {
              $("#amount").html("$" + ui.values[0] + " - $" + ui.values[1]);
            },
            stop: function(event, ui) {
              var str2;
              str2 =
                '<span class="js-filtering-category js-filtering-price db mr-m mr-l-l pv-s ph-m bdr-3 f7 tc-link pointer btn-link filter__selected-item" data-low="' +
                ui.values[0] +
                '" data-upper="' +
                ui.values[1] +
                '">$' +
                ui.values[0] +
                "- $" +
                ui.values[1] +
                '<span class="icon-close va-middle fw-800"></span></span>';
              $(".js-filtering-price").remove();
              $(".js-filtering-category-contatiner").append(str2);
              $(".js-filtering-price .icon-close").on("click", function() {
                $(this)
                  .parent()
                  .remove();
                ajax_filter_category();
              });
              $(".js-price-filter").hide();
              ajax_filter_category();
            }
          });
        }
        return false;
      } else {
        console.log(isPrice);
        if (!isPrice) {
          var newPrices = jQuery(".js-new-filter").html();
          jQuery(".js-price-filter").html(newPrices);
        }

        jQuery("#category-post-content").hide();
        jQuery("#category-post-content-ajax").show();
        jQuery("#category-post-content-ajax").html(response);
        vin65remote.product.addToCartForm($("#store_url").val());
        $(".js-stars-container").each(function(index, element) {
          var str = $(".js-rating-score").html();
          str = str.replace(/\s+/, "");
          var num = parseFloat(str);
          rateStyleJQ(num, element);
        });
        jQuery(".js-loading-animation").addClass("hidden");

        if (!isPrice) {
          var newMin = parseInt($(".js-new-prices").attr("data-min"));
          var newMax = parseInt($(".js-new-prices").attr("data-max"));
          $("#rangeSlider")[0].dataset.min = newMin;
          $("#rangeSlider")[0].dataset.max = newMax;
          $(".js-filter-dropdown")
            .find("#amount")
            .text("$" + newMin + " - $" + newMax);
          $("#rangeSlider").slider("destroy");
          $("#rangeSlider").slider({
            range: true,
            min: newMin,
            max: newMax,
            values: [newMin, newMax],
            slide: function(event, ui) {
              $("#amount").html("$" + ui.values[0] + " - $" + ui.values[1]);
            },
            stop: function(event, ui) {
              var str2;
              str2 =
                '<span class="js-filtering-category js-filtering-price db mr-m mr-l-l pv-s ph-m bdr-3 f7 tc-link pointer btn-link filter__selected-item" data-low="' +
                ui.values[0] +
                '" data-upper="' +
                ui.values[1] +
                '">$' +
                ui.values[0] +
                "- $" +
                ui.values[1] +
                '<span class="icon-close va-middle fw-800"></span></span>';
              $(".js-filtering-price").remove();
              $(".js-filtering-category-contatiner").append(str2);
              $(".js-filtering-price .icon-close").on("click", function() {
                $(this)
                  .parent()
                  .remove();
                ajax_filter_category();
              });
              $(".js-price-filter").hide();
              ajax_filter_category();
            }
          });
        }
        return false;
      }
    }
  });
}
function rateStyleJQ(num, divID) {
  var ratingRounded = Math.floor(num);
  $(divID)
    .find(".star-over")
    .slice(0, ratingRounded)
    .addClass("dib");
  var partialShade = Math.round((num - ratingRounded) * 100);
  if (partialShade != 0) {
    $(
      $(divID)
        .find(".star-over")
        .get(ratingRounded)
    )
      .addClass("dib")
      .css("width", partialShade + "%");
  }
}
jQuery(".remote-form").each(function() {
  if ("" !== $(this).attr("v65remotejs")) {
    vin65remote.form.form($(this).data("website"), $(this).data("formname"), $(this));
  }
});
