$(document).ready(function() {
  $(".js-wines-promo").slick({
    infinite: true,
    slidesToShow: 1,
    arrows: false,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          dots: true
        }
      }
    ]
  });
});
