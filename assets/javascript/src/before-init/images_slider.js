$(document).ready(function() {
  $(".js-images-slider").slick({
    infinite: true,
    slidesToShow: 1,
    arrows: true,
    vertical: false,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          arrows: false,
          dots: true
        }
      }
    ]
  });
});
