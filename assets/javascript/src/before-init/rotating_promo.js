$(document).ready(function() {
  $(".js-rotating-promo").slick({
    infinite: true,
    slidesToShow: 1,
    arrows: false,
    vertical: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          vertical: false,
          dots: true
        }
      }
    ]
  });
});
