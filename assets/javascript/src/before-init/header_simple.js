$(".js-toggle-menu").on("click", function(e) {
  e.preventDefault();
  $(this).toggleClass("close");

  if ($(window).innerWidth() > 690) {
    $(".js-mobile-nav").slideToggle(600);
  } else {
    $(".js-phone-nav").slideToggle(600);
  }
});

$(".js-nav-tab").on("click", function(e) {
  if ($(window).innerWidth() > 690 && $(window).innerWidth() < 1280) {
    e.preventDefault();
    $(".js-nav-tab.active").removeClass("active");
    $(".js-nav-submenu.active").removeClass("active");
    $(this).addClass("active");
    $(this)
      .siblings(".js-nav-submenu")
      .addClass("active");
  }
});

$(".js-account-nav").on("mouseenter", function() {
  if (!$(".js-login-submenu").hasClass("opened")) {
    $(".js-login-submenu")
      .slideDown(400)
      .addClass("opened");
  }
});

$(".js-account-nav").on("mouseleave", function() {
  if ($(".js-login-submenu:hover").length === 0) {
    $(".js-login-submenu")
      .slideUp(400)
      .removeClass("opened");
  }
});

$(".js-login-submenu").on("mouseleave", function() {
  if ($(".js-account-nav:hover").length === 0) {
    $(this)
      .slideUp(400)
      .removeClass("opened");
  }
});

$(".js-account-mobile").on("click", function() {
  $(".js-account-mobile-dropdown").slideToggle(400);
});
