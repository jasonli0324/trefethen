$(document).ready(function() {
  ////////////////////////////////////////
  //            WINE NAME               //
  ////////////////////////////////////////
  var yearListOpened = false;
  var nameListOpened = false;

  // Open menu on hover - Desk
  $(".js-portfolio-name-btn").on("mouseenter", function() {
    if ($(window).innerWidth() >= 1024) {
      if (!nameListOpened) {
        $(this)
          .siblings(".js-portfolio-name-list:not(.hidden)")
          .slideDown(400, function() {
            nameListOpened = true;
          });
      }
    }
  });

  $(".js-portfolio-name-list").on("mouseleave", function() {
    var context = $(this).parents(".js-filter-container");
    if ($(window).innerWidth() >= 1024) {
      if (nameListOpened) {
        if ($(context).find(".js-portfolio-name-btn:hover").length === 0) {
          $(this).slideUp(400, function() {
            nameListOpened = false;
          });
        }
      }
    }
  });

  $(".js-portfolio-name-btn").on("mouseleave", function() {
    var context = $(this).parents(".js-filter-container");
    if ($(window).innerWidth() >= 1024) {
      if (nameListOpened) {
        if ($(context).find(".js-portfolio-name-list:hover").length === 0) {
          $(this)
            .siblings(".js-portfolio-name-list")
            .slideUp(400, function() {
              nameListOpened = false;
            });
        }
      }
    }
  });

  // Open menu on click - Mobile
  $(".js-portfolio-name-btn").on("click", function() {
    if ($(window).innerWidth() < 1024) {
      $(this)
        .siblings(".js-portfolio-name-list:not(.hidden)")
        .slideToggle(400);
    }
  });

  // Add name to filter
  $("body").delegate(".js-name-filter", "click", function() {
    var context = $(this).parents(".js-filter-container");
    var hasYear =
      $(context)
        .find(".js-portfolio-filter")
        .attr("data-year") !== "";
    if (!$(this).hasClass("disabled")) {
      var name = $(this).attr("data-value");

      if (!hasYear) {
        filter_wine_name(name, context);
      }

      if (
        $(context)
          .find(".js-portfolio-name-btn")
          .hasClass("error")
      ) {
        $(context)
          .find(".js-portfolio-name-btn")
          .removeClass("error");
        $(context)
          .find(".js-name-error")
          .addClass("dn");
      }

      $(this).addClass("tc-link");
      $(context)
        .find(".js-name-filter")
        .not(this)
        .addClass("disabled");
      $(this)
        .siblings(".js-remove-name")
        .removeClass("dn");
      $(context)
        .find(".js-portfolio-name-list")
        .slideUp(400);
      $(context)
        .find(".js-name-placeholder")
        .removeClass("tc-primary-text-50")
        .text(name);
      $(context)
        .find(".js-portfolio-filter")
        .attr("data-name", name);
    }
  });

  // Remove name from filter
  $("body").delegate(".js-remove-name", "click", function() {
    var context = $(this).parents(".js-filter-container");
    var year = $(context)
      .find(".js-portfolio-filter")
      .attr("data-year");

    removeNameFilter(context);

    if (year !== "") {
      filter_wine_year(year, context);
    } else {
      filter_wine_year(false, context);
      $(context)
        .find(".js-initial-years-list")
        .removeClass("hidden");
      $(context)
        .find(".js-searched-years-list")
        .addClass("hidden");
    }
    $(".js-portfolio-name-list").slideUp(400);
  });

  ////////////////////////////////////////
  //            WINE YEAR               //
  ////////////////////////////////////////

  // Open menu on hover - Desk
  $(".js-portfolio-year-btn").on("mouseenter", function() {
    if ($(window).innerWidth() >= 1024) {
      if (!yearListOpened) {
        $(this)
          .siblings(".js-portfolio-year-list:not(.hidden)")
          .slideDown(400, function() {
            yearListOpened = true;
          });
      }
    }
  });

  $(".js-portfolio-year-list").on("mouseleave", function() {
    var context = $(this).parents(".js-filter-container");
    if ($(window).innerWidth() >= 1024) {
      if ($(context).find(".js-portfolio-year-btn:hover").length === 0) {
        $(this).slideUp(400, function() {
          yearListOpened = false;
        });
      }
    }
  });

  $(".js-portfolio-year-btn").on("mouseleave", function() {
    var context = $(this).parents(".js-filter-container");
    if ($(window).innerWidth() >= 1024) {
      if ($(context).find(".js-portfolio-year-list:hover").length === 0) {
        $(this)
          .siblings(".js-portfolio-year-list")
          .slideUp(400, function() {
            yearListOpened = false;
          });
      }
    }
  });

  // Open menu on click - Mobile
  $(".js-portfolio-year-btn").on("click", function() {
    if ($(window).innerWidth() < 1024) {
      $(this)
        .siblings(".js-portfolio-year-list:not(.hidden)")
        .slideToggle(400);
    }
  });

  // Add year to filter
  $("body").delegate(".js-year-filter", "click", function() {
    var context = $(this).parents(".js-filter-container");
    var hasName =
      $(context)
        .find(".js-portfolio-filter")
        .attr("data-name") !== "";

    if (!$(this).hasClass("disabled")) {
      var year = $(this).attr("data-value");

      if (!hasName) {
        filter_wine_year(year, context);
      }

      if (
        $(context)
          .find(".js-portfolio-year-btn")
          .hasClass("error")
      ) {
        $(context)
          .find(".js-portfolio-year-btn")
          .removeClass("error");
        $(context)
          .find(".js-year-error")
          .addClass("dn");
      }

      $(this).addClass("tc-link");
      $(context)
        .find(".js-year-filter")
        .not(this)
        .addClass("disabled");
      $(this)
        .siblings(".js-remove-year")
        .removeClass("dn");
      $(context)
        .find(".js-portfolio-year-list")
        .slideUp(400);
      $(context)
        .find(".js-year-placeholder")
        .removeClass("tc-primary-text-50")
        .text(year);
      $(context)
        .find(".js-portfolio-filter")
        .attr("data-year", year);
    }
  });

  // Remove year from filter
  $("body").delegate(".js-remove-year", "click", function() {
    var context = $(this).parents(".js-filter-container");
    var name = $(context)
      .find(".js-portfolio-filter")
      .attr("data-name");

    removeYearFilter(context);

    if (name !== "") {
      filter_wine_name(name, context);
    } else {
      filter_wine_name(false, context);
      $(context)
        .find(".js-initial-names-list")
        .removeClass("hidden");
      $(context)
        .find(".js-searched-names-list")
        .addClass("hidden");
    }
    $(context)
      .find(".js-portfolio-year-list")
      .slideUp(400);
  });

  ////////////////////////////////////////
  //            SEARCH WINE             //
  ////////////////////////////////////////
  $(".js-search-wine").on("click", function() {
    var context = $(this).parents(".js-filter-container");
    var wineName = $(context)
      .find(".js-portfolio-filter")
      .attr("data-name");
    var wineYear = $(context)
      .find(".js-portfolio-filter")
      .attr("data-year");
    var isWine = $(context)
      .find(".js-portfolio-filter")
      .hasClass(".js-is-wine");
    ajax_filter_search_wine(wineName, wineYear, context, isWine);
  });

  ////////////////////////////////////////
  //            CLEAR SEARCH            //
  ////////////////////////////////////////
  $("body").delegate(".js-clear-search", "click", function() {
    var context = $(this).parents(".js-filter-container");
    $(context)
      .find(".js-results-container")
      .html("");
    removeNameFilter(context);
    removeYearFilter(context);
  });

  function removeNameFilter(context) {
    var placeholder = $(context)
      .find(".js-name-placeholder")
      .attr("data-placeholder");
    $(context)
      .find(".js-portfolio-name-list")
      .removeClass("opened");
    $(context)
      .find(".js-remove-name:not(.dn)")
      .addClass("dn");
    $(context)
      .find(".js-name-filter.tc-link")
      .removeClass("tc-link");
    $(context)
      .find(".js-name-filter.disabled")
      .removeClass("disabled");
    $(context)
      .find(".js-name-placeholder")
      .addClass("tc-primary-text-50")
      .text(placeholder);
    $(context)
      .find(".js-portfolio-filter")
      .attr("data-name", "");
    $(context)
      .find(".js-initial-names-list")
      .removeClass("hidden");
    $(context)
      .find(".js-searched-names-list")
      .addClass("hidden");
  }

  function removeYearFilter(context) {
    var placeholder = $(context)
      .find(".js-year-placeholder")
      .attr("data-placeholder");
    $(context)
      .find(".js-portfolio-year-list")
      .removeClass("opened");
    $(context)
      .find(".js-year-filter.tc-link")
      .removeClass("tc-link");
    $(context)
      .find(".js-remove-year:not(.dn)")
      .addClass("dn");
    $(context)
      .find(".js-year-filter.disabled")
      .removeClass("disabled");
    $(context)
      .find(".js-year-placeholder")
      .addClass("tc-primary-text-50")
      .text(placeholder);
    $(context)
      .find(".js-portfolio-filter")
      .attr("data-year", "");
    $(context)
      .find(".js-initial-years-list")
      .removeClass("hidden");
    $(context)
      .find(".js-searched-years-list")
      .addClass("hidden");
  }

  ////////////////////////////////////////
  //            FILTER NAME             //
  ////////////////////////////////////////
  function filter_wine_name(name, context) {
    if (name) {
      var years = [];
      $(context)
        .find(".js-searched-years-list")
        .html("");
      $(context)
        .find(".js-portfolio-item")
        .each(function() {
          var thisName = $(this).attr("data-label");
          if (name === thisName) {
            var year = $(this).attr("data-year");
            if ($.inArray(year, years)) {
              var content =
                '<li class="flex justify-between">' +
                '<button class="js-year-filter f-secondary fw-300 ls-tinier h-tc-link td-40" data-value="' +
                year +
                '">' +
                year +
                "</button>" +
                '<button class="js-remove-year dn tc-link icon-close va-middle fw-800"></button>' +
                "</li>";
              $(context)
                .find(".js-searched-years-list")
                .append(content);
              years.push(year);
            }
          }
        });

      $(context)
        .find(".js-initial-years-list")
        .addClass("hidden");
      $(context)
        .find(".js-searched-years-list")
        .removeClass("hidden");
    } else {
      $(context)
        .find(".js-initial-years-list")
        .removeClass("hidden");
      $(context)
        .find(".js-searched-years-list")
        .addClass("hidden")
        .html("");
      yearListOpened = false;
    }
  }

  ////////////////////////////////////////
  //            FILTER YEAR             //
  ////////////////////////////////////////
  function filter_wine_year(year, context) {
    var labels = [];
    if (year) {
      $(context)
        .find(".js-searched-names-list")
        .html("");
      $(context)
        .find(".js-portfolio-item")
        .each(function() {
          var thisYear = $(this).attr("data-year");
          if (year === thisYear) {
            var label = $(this).attr("data-label");
            if ($.inArray(label, labels)) {
              var content =
                '<li class="flex justify-between">' +
                '<button class="js-name-filter f-secondary fw-300 ls-tinier h-tc-link td-40" data-value="' +
                label +
                '">' +
                label +
                "</button>" +
                '<button class="js-remove-name dn tc-link icon-close va-middle fw-800"></button>' +
                "</li>";
              $(".js-searched-names-list").append(content);
              labels.push(label);
            }
          }
        });

      $(context)
        .find(".js-initial-names-list")
        .addClass("hidden");
      $(context)
        .find(".js-searched-names-list")
        .removeClass("hidden");
    } else {
      removeYearFilter(context);
      $(context)
        .find(".js-initial-names-list")
        .removeClass("hidden");
      $(context)
        .find(".js-searched-names-list")
        .addClass("hidden")
        .html("");
      nameListOpened = false;
    }
  }

  ////////////////////////////////////////
  //            SEARCH WINE             //
  ////////////////////////////////////////
  function ajax_filter_search_wine(name, year, context, isWine) {
    jQuery.ajax({
      type: "POST",
      url: my_ajax_object.ajax_url,
      data: {
        action: "search-wines",
        name: name,
        year: year,
        isWine: isWine
      },
      success: function(response) {
        if (response === "missing name") {
          $(context)
            .find(".js-portfolio-name-btn")
            .addClass("error");
          $(context)
            .find(".js-name-error")
            .removeClass("dn");
          $(context)
            .find(".js-year-error")
            .addClass("dn");
        } else if (response === "missing year") {
          $(context)
            .find(".js-portfolio-year-btn")
            .addClass("error");
          $(context)
            .find(".js-name-error")
            .addClass("dn");
          $(context)
            .find(".js-year-error")
            .removeClass("dn");
        } else {
          $(context)
            .find(".js-name-error")
            .addClass("dn");
          $(context)
            .find(".js-year-error")
            .addClass("dn");
          $(context)
            .find(".js-results-container")
            .html(response);
        }
      }
    });
  }
});
