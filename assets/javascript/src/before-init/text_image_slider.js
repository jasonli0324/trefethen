$(document).ready(function() {
  $(".js-text-image-slider").slick({
    infinite: true,
    slidesToShow: 1,
    arrows: false,
    vertical: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          vertical: false,
          dots: true
        }
      }
    ]
  });
});
