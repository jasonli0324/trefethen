$(document).ready(function() {
  $(".js-slider-prev").on("click", function() {
    var parent = $(this).parent(".slider-nav");
    var total = parseInt(
      $(parent)
        .find(".js-total-slides")
        .text()
    );
    var i = parseInt(
      $(parent)
        .find(".js-current-slide")
        .text()
    );
    $(parent)
      .siblings(".slick-slider")
      .slick("slickPrev");
    i = i === 1 ? total : i - 1;
    $(parent)
      .find(".js-current-slide")
      .text(i);
  });

  $(".js-slider-next").on("click", function() {
    var parent = $(this).parent(".slider-nav");
    var i = parseInt(
      $(parent)
        .find(".js-current-slide")
        .text()
    );
    var total = parseInt(
      $(parent)
        .find(".js-total-slides")
        .text()
    );
    $(parent)
      .siblings(".slick-slider")
      .slick("slickNext");
    i = i === total ? 1 : i + 1;
    $(parent)
      .find(".js-current-slide")
      .text(i);
  });
});
