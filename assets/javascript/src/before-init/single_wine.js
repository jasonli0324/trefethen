$(document).ready(function() {
  var index = $(".js-downloads-list")
    .find(".active")
    .parent()
    .index();
  setMarkerPosition(index);

  $(".js-list-date").on("click", function() {
    var thisIndex = $(this)
      .parent()
      .index();
    $(".js-list-date.active").removeClass("active");
    $(this).addClass("active");
    setMarkerPosition(thisIndex);
  });

  function setMarkerPosition(index) {
    $(".js-downloads-list .marker").animate(
      {
        top: (index - 1) * 28
      },
      400,
      function() {}
    );
  }

  if ($(".js-sticky").length) {
    var stickyTop = $(".js-sticky").offset().top;

    $(window).scroll(function(event) {
      if ($(window).innerWidth() > 1024) {
        var stickyContainerHeight = $(".js-sticky-content").innerHeight();
        var stickyWidth = $(".js-sticky").outerWidth();
        var stickyHeight = $(".js-sticky").outerHeight();
        var scroll = $(window).scrollTop();
        var bottomPoint = stickyContainerHeight + 100 - stickyHeight;

        if (scroll >= bottomPoint) {
          $(".js-sticky")
            .removeClass("sticked")
            .addClass("bottom");
          $(".js-sticky").css({
            width: stickyWidth
          });
        } else if (scroll >= stickyTop - 100) {
          $(".js-sticky")
            .removeClass("bottom")
            .addClass("sticked");
          $(".js-sticky").css({
            width: stickyWidth
          });
        } else {
          $(".js-sticky")
            .removeClass("sticked")
            .removeClass("bottom");
          $(".js-sticky").css({
            width: "40%"
          });
        }
      }
    });
  }
});
