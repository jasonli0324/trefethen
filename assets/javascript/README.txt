
JAVSCRIPT SECTION GENERAL INFORMATION
--------------------------------------------------------------------
Created by: Highway 29 Creative



OVERVIEW
----------------------------------

Only one JavaScript file is loaded for global use in this theme (though WP core and plugins will load other files), that is the theme.min.js file.  It should be noted that certain content blocks are capable of loading additional JavaScript based on their needs, however unless those script resources of a content block are used excessively, it is best to keep the block js dependencies out of the global theme JavaScript file.

The theme.min.js file is a concatenation and minified version of all the JavaScript files located in the src directory. 

** IMPORTANT **
It is recommended that JavaScript files be written as stand alone as possible and contained within a closure as to prevent polluting the global namespace and creating dependency issues.  Because some of this is unavoidable there is a simplified concatenation order defined in this document.


The theme.min.js file has an associated .map file for proper development backtracking to the source js file.




DEPENDANCIES
----------------------------------

The src folder contains multiple JavaScript files that must be combined (in a general order) and then minified as the theme.min.js file.  A source map must also be created for the purpose of being able to backtrack errors and console messages to the proper src file.  There are a variety of command line and application based tools that can provide this functionality.  NPM, using uglify-js, is the built in means of concatenating, minifying, and creating a source map.  NPM will need to be installed and the package dev dependencies will need to be loaded as defined in the primary theme README file to use the built in functionality.

There are numerous alternatives for performing this task however other then just NPM.  Applications like Codekit (https://codekitapp.com/) can be used and there are numerous command line tools available.  To perform this task with the absolute minimum of tooling it is possible to use an online tool like https://jscompress.com/ by simply copying in the source JS from the various files (note order defined in next section) and then compressing it and pasting that code into the theme.min.js.  NOTE however this approach does not create a source map which is extremely useful in trouble shooting.




CONCATENATION ORDER
----------------------------------

Concatenation order is important as it is possible that certain JavaScript code will have dependencies on other files or variable declarations.

Though build tools exist that allow much greater control of the concatenation order and module dependencies, for sake of simplicity a more basic approach is taken here.  When the scripts are combined they are done in the following order.

1. JavaScript files, alphabetically, located in the 'assets/javascript/src/before-init' directory.
2. The main init file which is located at 'assets/javascript/src/init.js'.
3. JavaScript files, alphabetically, located in the 'assets/javascript/src/after-init' directory.

Fine tuning of concatenation order can be made by perpending a number on a file such as '1-your_file.js', '2-next_file.js', etc.

NOTE:  If it is desired NOT to use any of this concatenation then all JS code can simply be placed in the main init.js file in the order it is desired to operate, or addition script enqueue code can be added in the functions.php file.



