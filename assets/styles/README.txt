
STYLES SECTION GENERAL INFORMATION
--------------------------------------------------------------------
Created by: Highway 29 Creative



OVERVIEW
----------------------------------

There are four style files that are utilized from this directory for the site. A list and their functionality is below.

- style.css
This is the primary style sheet for the front end of the site.

- login-style.css
This css file contains modifications to the login page to improve it's appearance and brand identity

- editor-style.css
This css file contains the main typography styles and is included for use with the WP TinyMCE editor within WP admin.  The point of this stylesheet is to bring the same kind of styling to text within a WYSIWG editor as will be seen on the front end.

- acf-admin-style.css
This stylesheet adds minor style tweaks to some ACF elements within the WP admin interface.  The purpose of these styles is not for brand purposes but to make some elements and groupings more understandable to client administrators.


All CSS files have an associated .map file for proper development backtracking to the source scss file.

ALL four of the files are compiled from scss files of the same name within the 'scss' directory.




DEPENDANCIES
----------------------------------

The scss folder contains four primary scss files that need to be compiled into the css files seen in the root of the styles directory.  Each of the four main scss files also reference and import various files from within the directories in the scss folder.  There are a variety of command line and application based tools that can provide this functionality.  NPM, using node-sass, is the built in means of compilation and source map creation.  NPM will need to be installed and the package dev dependencies will need to be loaded as defined in the primary theme README file in order to use the built in functionality.

There are numerous alternatives for performing the SCSS compile aside from NPM.  Various application and command line tools are documented at http://sass-lang.com/install.




SCSS STRUCTURE
----------------------------------

Within the scss directory there are four scss files and multiple directories.  The top level style.scss only contains import command as all styles are held within the directories.  The other three each have some imports as needed and then some custom styling done right in that file.  The scss directory structure is seen below (order is based on typical alphabetic sort not import order or importance).

	*** DEPRECIATED   /blocks		*** -> style.scss imports block scss files directly from /inc/blocks directories
	/config
	/global
	/helpers
	*** DEPRECIATED   /parts		*** -> style.scss imports part scss files directly from /inc/parts directories
	/vendor
	/views

/blocks   *** DEPRECIATED - block scss files imported from their home in /inc/blocks ***
--> scss files copied from /inc/blocks are placed here and then an import statement is added to the scss root level style.scss file.  These files will contain content block specific styles.  These files are imported last in the style.scss file so that they can override any global styles.

/config
--> variable and other configuration files would be located here

/global
--> items of global reach are here, things such as forms, buttons, base container styles, and typography.

/helpers
--> This directory contains SASS functions and mixins

/parts   *** DEPRECIATED - part scss files imported from their home in /inc/parts ***
--> SCSS files copied from /inc/parts are placed here and then an import statement is added to the scss root level style.scss file. These files will contain styles for common parts used on the site such as headers, footers, etc.

/vendor
--> Styles from third party libraries are placed here.

/views
--> The views directory if for any files that only pertain to a single page template or post single

