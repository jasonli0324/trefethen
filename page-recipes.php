<?php 

	/**
	* Template Name: Recipes Index Layout
	*
	* This is the template used for Shop page
	*/

	get_header();

	$taxonomy = 'recipe_categories';
?>

<section>
  <div class="mxw-1952 mxw-1162-ds wrapper-small mh-auto mb-xxl ph-m">
    <?php custom_breadcrumbs(); ?>
  </div>
	<?php include( locate_template( 'inc/partials/page_header/page_header.php', false, false ) ); ?>
	<div class="mxw-1952 mxw-1162-ds wrapper-small mh-auto ph-m">
		<?php 
			$shop_filters = get_terms( array( 'taxonomy' => $taxonomy, 'parent' => 0 ) ); 
			$recipes_filter = true;
		?>
		<?php include( locate_template( 'inc/partials/shop_filters/shop_filters.php', false, false ) ); ?>
	</div>
  <?php 
    $post_args = array(
      'post_type' => 'recipe',
      'posts_per_page' => -1
		);

    $recipes = new WP_Query($post_args);
    if ($recipes->have_posts()) :
	?>
		<div class="mxw-1952 mxw-1162-ds wrapper-small mh-auto mb-xxl mb-xxxl-l pb-m-l ph-m">
			<ul id="category-post-content" class="flex flex-wrap recipes-list">
				<?php 
					while($recipes->have_posts()) : $recipes->the_post(); 
						$title = get_field('recipe_title');
						$url = get_permalink();
						$thumbnail = get_field('recipe_photos')['recipe_thumbnail_photo'];
						$categories = wp_get_post_terms($post->ID, $taxonomy);
						$categories_amount = count($categories);
						$j = 1;
					
						include( locate_template( 'inc/partials/recipe_card/recipe_card.php', false, false ) );
					endwhile; 
					wp_reset_postdata(); 
				?>
			</ul>
			<ul id="category-post-content-ajax" class="flex flex-wrap recipes-list"></ul>
		</div>
	<?php endif; ?>
	
  <?php FLEX::blocks(); ?>
</section>

<?php get_footer();?>