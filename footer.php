<?php
/**
 * Footer file
 */
?>

	<?php // uses footer part defined in 'inc/parts' ?>
	<?php global $disable_footer; 
	global $post;
	$site_primary = get_field('site_primary_accent', 'options');
	?>
	<?php if ( !isset($disable_footer) || (isset($disable_footer) && !$disable_footer)) FLEX::part('footer'); ?>

	<?php wp_footer(); ?>
	<script type="text/javascript">vin65remote.cart.init('<?php echo get_field("store_url", "option");?>',0);</script>
	<script type="text/javascript">vin65remote.product.addToCartForm('<?php echo get_field("store_url", "option");?>');</script>
	<script type="text/javascript">vin65remote.usertools.loginWidget('<?php echo get_field("store_url", "option");?>');</script>
	<script>
		(function(document, tag) { var script = document.createElement(tag); var element = document.getElementsByTagName('body')[0]; script.src = 'https://acsbap.com/apps/app/assets/js/acsb.js'; script.async = true; script.defer = true; (typeof element === 'undefined' ? document.getElementsByTagName('html')[0] : element).appendChild(script); script.onload = function() { acsbJS.init({ statementLink : '', feedbackLink : '', footerHtml : 'Powered by Highway 29 Creative', hideMobile : false, hideTrigger : false, language : 'en', position : 'left', leadColor : '<?php echo $site_primary['primary_accent_color'];?>', triggerColor : '#35856B', triggerRadius : '50%', triggerPositionX : 'left', triggerPositionY : 'bottom', triggerIcon : 'default', triggerSize : 'medium', triggerOffsetX : 20, triggerOffsetY : 20, mobile : { triggerSize : 'small', triggerPositionX : 'left', triggerPositionY : 'center', triggerOffsetX : 0, triggerOffsetY : 0, triggerRadius : '0' } }); };}(document, 'script'));
	</script>

	<?php
		$gtm = get_field('gtm', 'options');		
		$linkedin_id = get_field('linkedin', 'options');
		if ($gtm['use_google_tag_manager']): ?>
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?php echo $gtm['gtm_container_id'];?>"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
	<?php elseif(!empty($linkedin_id)): ?>
		<!-- LinkedIn Insight Tag -->
		<script type="text/javascript">
			_linkedin_partner_id = "<?php echo $linkedin_id?>";
			window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || [];
			window._linkedin_data_partner_ids.push(_linkedin_partner_id);
			</script><script type="text/javascript">
			(function(){var s = document.getElementsByTagName("script")[0];
			var b = document.createElement("script");
			b.type = "text/javascript";b.async = true;
			b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
			s.parentNode.insertBefore(b, s);})();
		</script>
		<noscript>
			<img height="1" width="1" style="display:none;" alt="" src="https://px.ads.linkedin.com/collect/?pid=<?php echo $linkedin_id?>&fmt=gif" />
		</noscript>
	<?php endif;?>

</body>
</html>


