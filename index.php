<?php
	/**
	 * The main template file
	 *
	 * This is the most generic template file in a WordPress theme
	 * and one of the two required files for a theme (the other being style.css).
	 * It is used to display a page when nothing more specific matches a query.
	 * E.g., it puts together the home page when no home.php file exists.
	 *
	 * @link https://codex.wordpress.org/Template_Hierarchy
	 *
	 * @package Highway29Creative
	 */
	// global $post;
	$page_id = get_option('page_for_posts', true);
  $title = get_field('page_header_title', $page_id);
  $intro = get_field('page_header_intro', $page_id);

	get_header(); 
?>

<section class="mb-xxxl-l pb-xxl pb-xxxl-l posts-list">
	<div class="mxw-1952 mxw-1162-ds wrapper-small mh-auto ph-m mb-xl">
		<?php custom_breadcrumbs(); ?>
	</div>
	<?php include( locate_template( 'inc/partials/page_header/page_header.php', false, false ) ); ?>
	<div class="mxw-1952 wrapper-small pt-l">
		<?php

			if  ( have_posts() ) :
		?>
			<?php while( have_posts() ) : the_post(); ?>
				<?php 
					$id = get_the_ID();
					$url = get_permalink();
          $photo = get_field('blog_post_photo')['url'];
					$title = get_the_title();
					$date = get_the_date();
					$cats = wp_get_post_categories( $id );
					$cat_count = count($cats);
					$flexible_content = get_field('additional_flexible_content')['content_blocks'];
					foreach($flexible_content as $content) {
						if ($content['acf_fc_layout'] === 'text_block' && $content['text_block_header_text']) {
							$intro = $content['text_block_header_text'];
							break;
						}
					}
					$k = 1;
				?>
				<article class="mb-xxl mb-xxxl-l pb-m-l">
					<div class="mxw-1952 mh-auto mb-m mb-xl-l ph-m">
						<?php if($photo) : ?>
							<?php if ($url) : ?>
								<a href="<?php echo $url; ?>">
							<?php endif; ?>
								<div class="bg-cover bg-center posts-list__image" style="background-image: url('<?php echo $photo; ?>')"></div>
							<?php if ($url) : ?>
								</a>
							<?php endif; ?>
						<?php else : ?>
							<div class="bdb-1 posts-list__border"></div>
						<?php endif; ?>
					</div>
					<div class="mxw-1952 mxw-1162-ds wrapper-small mh-auto ph-m">
						<div class="mxw-670">
							<?php if ($title) : ?>
								<h2 class="mb-m f-title h2"><?php echo $title; ?></h2>
							<?php endif; ?>
							<?php if($cat_count > 0) : ?>
								<ul class="f-secondary lh4 ls-tinier ">
									<?php foreach($cats as $cat) : ?>
										<?php $cat_name = get_category($cat)->name; ?>
										<li class="dib"><?php echo $cat_name; ?><?php if($k < $cat_count) echo ', '?></li>
									<?php $k++; endforeach; ?>
								</ul>
							<?php endif; ?>
							<?php if ($date) : ?>
								<p class="mb-m f-secondary f6-s lh4 fw-300 ls-tinier "><?php echo $date; ?></p>
							<?php endif; ?>
							<?php if ($intro) : ?>
								<div class="mb-m mb-xl-l tc-brown f3 f2-l"><?php echo $intro; ?></div>
							<?php endif; ?>
							<?php if ($url) : ?>
								<a href="<?php echo $url; ?>" class="relative dib f-secondary lh4 fw-300 h-tc-link-hover td-40 featured-link">Read<span class="pl-xs pl-s-ns tc-link icon-link-arrow"></span></a>
							<?php endif; ?>
						</div>
					</div>
				</article>
			<?php $intro = ''; endwhile; ?>
		<?php endif; ?>
	</div>
</section>
<?php get_footer();?>
