<?php 
	get_header();
	if( have_posts() ) :
		while( have_posts() ) :
		the_post(); 
		
		$wine_name = get_field('wine_name');
		$label_name = get_field('type_varietal')['label_name'];
		$dates = get_field('dates');
		$lab_data = get_field('lab_data');
		$type_varietals = get_field('type_varietal');
		$memberOnly = get_field('member_only');
		$article_price = get_field('price');
		$vivino_url = get_field('vivino_score');
		$vivino_rating = get_field('vivino_rating');
		$vivino_count = get_field('vivino_count');
		$alternate_name = get_field('alternate_format_name');
		$alternate_format_sku = get_field('alternate_alternate_format_sku');
		$alternate_price = get_field('alternate_alternate_price');
		$is_gift_card = (get_field('wine_card_type') === 'featured');
		$bottle_photos = get_field('bottle_photos');
		$gift_pack_photos = get_field('gift_pack_photos');
		$recipe_id = get_field('recipe');
		$downloads = get_field('downloads');
		$badge_message = get_field('badge_message');
		$is_card_image = false;
		$block_class_identifier = 'blk__video';
		$videoUrl = get_field('product_video');
		$teaser = get_field('teaser');
		$bottle_size = get_field('type_varietal')['size'];
		$product_type = get_field('type_varietal')['wine_type'];
		$is_featured = get_field('wine_card_type') === 'featured';

		if ($videoUrl !== "") {
			preg_match('/src="(.+?)"/', $videoUrl, $matches);
			$video_src = $matches[1];
			if (strpos($video_src, "youtube") !== false) :
				$type = "youtube";
			elseif (strpos($video_src, "vimeo") !== false) :
				$type = "vimeo";
			else :
				$type = "other";
			endif;
		}

		if ($is_gift_card) {
			$bottle_image = $gift_pack_photos['gift_pack_image'];
		} else {
			$bottle_image = $bottle_photos['secondary_photo'] ? $bottle_photos['secondary_photo'] : $bottle_photos['primary_photo'];
		}

		$brothers_args = array(
			'numberposts'	=> -1,
			'post_type'		=> 'wine',
			'meta_query' => array(
				array(
					'key' => 'type_varietal_label_name',
					'compare' => 'LIKE',
					'value' => $label_name
				)
			)
		);
		$downloads_arr = array();
		$brothers = new WP_Query($brothers_args);
		
		if ($brothers->have_posts()) :
			while($brothers->have_posts()) : $brothers->the_post(); 
				$year = get_field('dates') ? get_field('dates')['vintage'] : false;
				if($year) {
					$pdf_url = get_field('downloads')['tech_sheet'];
					$stack = array($year => $pdf_url);
					$year_int = (int)$year;
					$decate = (int)floor( $year_int * 0.1) * 10;
					if($pdf_url) {
						$downloads_arr[$decate][$year] = $pdf_url;
					}
				}
			endwhile;
		wp_reset_postdata(); 
		endif;
		krsort($downloads_arr);

		$description_intro = get_field('description_intro');
		$description = get_field('description');
		$suggested_recipe = get_field('recipe');
		$vintage = $dates ? $dates['vintage'] : false;
?>

<input type="hidden" class="is_sticky">
<article class="js-product mxw-1952 mh-auto ph-m pb-xxxl bg-white <?php echo $is_gift_card ? 'box-wine-page' : 'single-wine-page'; ?>">
	<div class="wrapper-small mb-xl"><?php custom_breadcrumbs(); ?></div>
	<section class="js-sticky-content relative mb-xxl-m mb-xxxl-l single-wine-page__content">
		<div class="js-product-image shrink-0 w-p-100 w-p-60-l mb-m <?php echo $bottle_photos['secondary_photo'] || $is_gift_card ? 'bg-cover' : 'bg-contain bg-no-repeat' ?> bg-center single-wine-page__image" data-img="<?php echo $bottle_image; ?>" style="background-image: url('<?php echo $bottle_image; ?>')"></div>
		
		<div class="js-sticky absolute-l z-1 w-p-100 w-p-40-l mb-xxl-nl pl-xxxl-l bg-white single-wine-page__sticky">
			<h1 class="mb-s mb-m-l quote tc-dark-grey"><?php if($vintage) { echo $vintage; } ?> <?php echo $wine_name; ?></h1>
			<?php 
				if ($product_type === 'Gift' || $product_type === 'Culinary') {
					$price_label = get_field('type_varietal')['item_denomination'] ? get_field('type_varietal')['item_denomination'] : 'Item';
				} elseif ($is_featured) {
					$price_label = 'Pack';
				} else {
					$price_label = $bottle_size;
				}
			?>
			<p class="js-wine-price mb-xl f-secondary fw-300 lh4 ls-tinier">$<?php echo floatval($article_price); ?> / <?php echo $price_label; ?></p>
			<?php if (!$is_gift_card) : ?>
				<div class="relative flex mb-l mb-xl-ns f-secondary tc-link single-wine-page__radio-btns">
					<!-- <p class="db pv-s ph-m bdr-3 ls-tinier" for="prick_check">Bottle</p> -->
					<input class="absolute" type="radio" name="payment_mode" id="prick_check" checked>
					<label class="js-price-option js-prick_check db pv-s ph-m bdr-3 ls-tinier pointer" data-type="<?php echo $bottle_size; ?>" for="prick_check"><?php echo $product_type === 'Dessert' || $product_type === 'Culinary' ? (get_field('type_varietal')['item_denomination'] ? get_field('type_varietal')['item_denomination'] : 'Item') : 'Bottle'; ?></label>
					<!-- <?php if ($alternate_name !== '') : ?>
						<input class="absolute" type="radio" name="payment_mode" id="alternate_check"> 
						<label class="js-price-option js-alternate_check db pv-s ph-m bdr-3 ls-tinier pointer" for="alternate_check"></label>
					<?php endif; ?> -->
					<?php if ($product_type !== 'Dessert' && $product_type !== 'Culinary') : ?>
						<input class="absolute" type="radio" name="payment_mode" id="case_check"> 
						<label class="js-price-option js-case_check db pv-s ph-m bdr-3 ls-tinier pointer" data-type="Case" for="case_check">Case</label>
					<?php endif; ?>
				</div>
			<?php endif; ?>
			<div v65remotejs="addToCartForm" productsku="<?php the_field('sku');?>" style="display:none;"></div>

			<div class="dn-s mb-l mb-xl-l pv-l-ns single-wine-page__teaser">
				<p class="f-title f-italic quote lh3 tc-brown"><?php echo $teaser; ?></p>
			</div>

			<div class="mb-l mb-xl-l pt-s pb-s-l">
				<div class="js-shop-add-to-cart js-shop-add-to-cart-container flex">
					<input class="quantity bdr-3 f6-s ta-center wine-card__input" type="number" value="1" />
					<button class="w-p-100 bdr-3 f-secondary f6-s tc-white bg-link h-bg-link td-40 single-wine-page__button">Add to Cart</button>
				</div>
				<p class="js-card-sold-out dn bdr-3 f-secondary ls-tinier tc-white bg-primary-text ta-center single-wine-page__sold-out">Sold Out</p>
			</div>
			
			<div class="dn-ns mb-l pv-l single-wine-page__teaser">
				<p class="f-title f-italic quote lh3 tc-brown"><?php echo $teaser; ?></p>
			</div>
			<?php if($is_gift_card) : ?>
				<p class="mb-xxl-s f7 ls-tinier f-secondary">Call <a href="tel:8668957696" class="tc-link">(866) 895-7696</a> for large orders</p>
			<?php else : ?>
				<div class="flex-l justify-between mb-xxl-s f-secondary">
					<?php if (have_rows('wine_scores')): ?>
						<div class="mb-l-nl">
							<p class="mb-s f7 ls-tinier uppercase">Scores</p>
							<ul class="flex single-wine-page__scores">
								<?php while (have_rows('wine_scores')) : the_row(); ?>
									<li class="f7 tc-brown lh7"><?php the_sub_field('score_code'); ?> <?php the_sub_field('score_number'); ?></li>
								<?php endwhile; ?>
							</ul>
						</div>
					<?php endif; ?>
					<?php if($vivino_rating && (int)$vivino_rating >= 4) : ?>
						<div>
							<div class="flex">
								<span class="f6 icon-vivino"></span>
								<div class="js-stars-container flex mb-xs mr-s">
									<?php for ($i = 0; $i < 5; $i++) : ?>
										<i class="relative f6 mr-xs star-under icon-star">
											<i class="absolute top-0 left-0 dn mr-xs f6 of-hidden star-over icon-star"></i>
										</i>
									<?php endfor; ?>
								</div>
							</div>
							<div class="flex">
								<p class="js-rating-score mr-xs f7 tc-brown"><?php echo $vivino_rating;?></p>
								<p class="js-rating-counts dn-s f7 tc-link"> (<span><?php echo $vivino_count;?></span> Reviews on <a target="_blank" rel="noopener noreferrer" href="<?php echo $vivino_url;?>">on Vivino.com</a>)</p>
							</div>
						</div>
					<?php endif; ?>
				</div>
			<?php endif; ?>
		</div>

		<div class="w-p-100 w-p-60-l mxw-900 single-wine-page__text">
			<?php if($is_gift_card) : ?>
				<?php if ($description_intro) : ?>
					<p class="mb-xxl f-title f-italic quote lh3 tc-brown"><?php echo $description_intro; ?></p>
				<?php endif; ?>
				<?php if ($description) : ?>
					<p class="mb-xxl f-secondary fw-300 lh4 ls-tinier"><?php echo $description; ?></p>
				<?php endif; ?>
			<?php else : ?>
				<?php if ($description) : ?>
					<p class="mb-xxl mb-xxxl-l pb-m-l f-title f-italic quote lh3 tc-brown"><?php echo $description; ?></p>
				<?php endif; ?>
			<?php endif; ?>
			
			<?php if($is_gift_card && have_rows('gift_items')) : ?>
				<div class="gift-items-container">
					<h3 class="mb-m f3 f2-l f-italic tc-brown">In This Collection</h3>
					<div class="flex justify-between flex-wrap">
						<?php
							if( have_rows('gift_items') ):
								while ( have_rows('gift_items') ) : the_row(); 
									$wineID = get_sub_field('product_in_pack');
									$bottle_photos = get_field('bottle_photos', $wineID);
									$item_image = $bottle_photos['primary_photo'];
									$vivino_rating = get_field('vivino_rating',$wineID);
									$vivino_count = get_field('vivino_count',$wineID);
									$price = get_field('price',$wineID);
									$vivino_url = get_field('vivino_score', $wineID);
									$wine_sku = get_field('sku', $wineID);
									$sub_product = true;
									
									if($wine_sku) {
										include( locate_template( 'inc/partials/wine_card/wine_card.php', false, false ) );
									}
								endwhile;
							endif;
						?>
					</div>
				</div>
			<?php elseif(!$is_gift_card) : ?>
				<?php 
					$vintage_notes = get_field('vintage_notes');
					$wine_notes = get_field('wine_notes');
					$food_affinities = get_field('food_affinities');
				?>
				<?php if($vintage_notes) : ?>
					<div class="mb-xl">
						<h3 class="mb-s f3 f2-l f-italic tc-brown">The Vintage</h3>
						<div class="f-secondary f6-nl fw-300 lh4"><?php echo $vintage_notes; ?></div>
					</div>
				<?php endif; ?>
				<?php if($wine_notes) : ?>
					<div class="mb-xl">
						<h3 class="mb-s f3 f2-l f-italic tc-brown">The Wine</h3>
						<div class="f-secondary f6-nl fw-300 lh4"><?php echo $wine_notes; ?></div>
					</div>
				<?php endif; ?>
				<?php if($food_affinities) : ?>
					<div class="mb-xl">
						<h3 class="mb-s f3 f2-l f-italic tc-brown">Food Affinities</h3>
						<div class="f-secondary f6-nl fw-300 lh4"><?php echo $food_affinities; ?></div>
					</div>
				<?php endif; ?>
				<?php if (have_rows('description_notes')): ?>
					<?php while (have_rows('description_notes')) : the_row(); ?>
						<div class="mb-xl">
							<h3 class="mb-s f3 f2-l f-italic tc-brown"><?php the_sub_field('description_notes_title'); ?></h3>
							<p class="f-secondary f6-nl fw-300 lh4"><?php the_sub_field('description_notes_copy'); ?></p>
						</div>
					<?php endwhile; ?>
				<?php endif; ?>
			<?php endif; ?>
			
			<?php 
				if(!$is_gift_card && $product_type !== 'Dessert' && $product_type !== 'Culinary' && count($brothers->posts) > 1) :
			?>
				<div class="mb-xxl mb-xxxl-l pb-m-l">
					<h3 class="mb-m f3 f2-l f-italic tc-brown">Download Tasting Notes</h3>
					<ul class="js-downloads-list relative f-secondary single-wine-page__downloads-list">
						<li class="absolute marker"></li>
						<?php
							$k = 0;
						?>
						<?php foreach($downloads_arr as $decade_val => $downloads_decade) : ?>
							<li>
								<button class="js-list-date relative lh4 ls-tinier tc-link <?php  if($k === 0) { echo 'active'; }  ?>"><?php echo $decade_val; ?>s</button>
								<ul class="absolute top-0 flex flex-wrap pt-s ph-m td-40 single-wine-page__downloads-sublist">
									<?php foreach($downloads_decade as $download_year => $download_url) : ?>
										<li class="mb-m ph-m f7">
											<a href="<?php echo $download_url; ?>" target="blank" class="ls-tinier td-40"><?php echo $download_year; ?></a>
										</li>
									<?php endforeach; ?>
								</ul>
							</li>
						<?php $k++; endforeach; ?>
					</ul>
				</div>
			<?php endif; ?>
		</div>
	
		<div class="w-p-100 w-p-60-l">
			<?php 
				if ($suggested_recipe) {
					$block_class_identifier = 'cross-promotion';
					$promotion_copy = get_field('recipe_title', $suggested_recipe);
					$promotion_image = get_field('recipe_photos', $suggested_recipe)['recipe_thumbnail_photo'];
					$promotion_link_url = get_permalink($suggested_recipe);
					$promotion_link_copy = 'See Recipe';
					$promotion_full_width = false;
					$is_single_wine = true;

					include( locate_template( 'inc/partials/cross_promotion/cross_promotion.php', false, false ) );
				}
			?>
		</div>

		<div class="w-p-100 w-p-60-l mxw-900 single-wine-page__text">
			<?php if(!$is_gift_card && get_field('accolades')) : ?>
				<h3 class="mb-s f3 f2-l f-italic tc-brown">Accolades</h3>
				<div class="f-secondary single-wine-page__accolades"><?php the_field('accolades'); ?></div>
			<?php endif; ?>
		</div>

	</section>
</article>

<?php FLEX::blocks(); ?>

<?php get_footer(); ?>
<?php endwhile; ?>
<?php endif; ?>