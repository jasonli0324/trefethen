<?php 
	get_header();

	if( have_posts() ) the_post(); 
	
	$prices = array();
	$filter_cats_arr = array();
	$filter_bottle_cats_arr = array();
	$category = $post->post_title;
	$post_args = array(
		'suppress_filters' => FALSE,
		'numberposts'	=> -1,
		'post_type'		=> 'wine',
		'meta_query'	=> array(
			'relation'		=> 'AND',
			array(
				'key' => 'wine_card_type',
				'value' => 'library',
				'compare' => 'NOT LIKE',
			)
		)
	);
	
	if($category === 'Whites & Rosé') {

		$post_white_subargs = array(
			'key' => 'type_varietal_wine_type',
			'value' => 'White',
			'compare' => 'LIKE',
		);
		$post_rose_subargs = array(
			'key' => 'type_varietal_wine_type',
			'value' => 'Rosé',
			'compare' => 'LIKE',
		);
		array_push($filter_cats_arr, $category);
		array_push($post_args['meta_query'], $post_white_subargs, $post_rose_subargs);

	} else {

		if($category === "Bottle" || $category === "Library") {

			$cat_name = $category === "Bottle" ? 'standard' :  $category;
			$post_subargs = array(
				'key' => 'wine_card_type',
				'compare' => 'LIKE',
				'value' => $cat_name
			);
			array_push($filter_bottle_cats_arr, $category);
			array_push($post_args['meta_query'], $post_subargs);

		} elseif ($category === "Culinary & Gifting") {
			
			$post_subargs_one = array(
				'key' => 'wine_card_type',
				'value' => 'featured',
				'compare' => 'LIKE'
			);
			$post_subargs_two = array(
				'key' => 'type_varietal_wine_type',
				'value' => array('Gift', 'Dessert', 'Culinary'),
				'compare' => 'IN',
			);
			$post_subargs_three = array(
				'key' => 'other_categories',
				'value' => 'Gifts',
				'compare' => 'LIKE',
			);
			array_push($filter_bottle_cats_arr, $category);
			array_push($post_args['meta_query'], $post_subargs_one, $post_subargs_two, $post_subargs_three);

		} else {
			$post_subargs = array(
				'key' => array('other_categories', 'type_varietal_wine_type', 'type_varietal_champagne_varietals', 'type_varietal_dessert_varietals', 'type_varietal_ice_varietals', 'type_varietal_red_varietals', 'type_varietal_rose_varietals', 'type_varietal_sparkling_varietals', 'type_varietal_white_varietals'),
				'value' => $category,
				'compare' => 'LIKE'
			);
			array_push($filter_cats_arr, $category);
			array_push($post_args['meta_query'], $post_subargs);
		}
	}

	$filter_cats = implode('-', $filter_cats_arr);
	$filter_bottle_cats = implode('-', $filter_bottle_cats_arr);
  $wines = new WP_Query($post_args);
  
  if($wines->have_posts()) {
    while($wines->have_posts()) {
			$wines->the_post();
			$is_gift = get_field('type_varietal')['wine_type'] === 'Gift';
			if(!$is_gift) {
				$price = get_field('price');
				if ($price !== 0) array_push($prices, $price);
			}
    } 
    wp_reset_postdata();
  }
	$prices = array_unique($prices);
	$prices = array_filter($prices);
	if(count($prices) > 0) {
		$max = max($prices);
		$min = min($prices);
	}
	$shop_filters = array('Price');
	$intro_text = get_field('intro_text');
	$index = 0;
?>

<div class="bg-white page-shop">
	<div class="js-loading-animation fixed top-0 left-0 w-p-100 vh-100 td-40 hidden loading-animation">
		<div class="flex justify-center items-center h-p-100">
			<div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
		</div>
	</div>
	<div class="mxw-1952 mxw-1162-ds wrapper-small mh-auto ph-m">
		<div class="mb-xl"><?php custom_breadcrumbs(); ?></div>
		<h1 class="mb-l-nl mb-s h1 f-title fw-300 tc-primary-text"><?php the_title(); ?></h1>
		<?php if($intro_text) : ?>
			<div class="w-p-100 mxw-670 mb-m mb-l-m mb-xl-l tc-brown f3 f2-l"><?php echo $intro_text; ?></div>
		<?php endif; ?>
		<?php 
			if ($wines->have_posts() && count($prices) > 1) :
				include( locate_template( 'inc/partials/shop_filters/shop_filters.php', false, false ) ); 
			endif;
		?>
	</div>
	<div id="category-post-content" class="page-shop__content">
		<section class="flex justify-between-nl flex-wrap mxw-1952 mxw-1162-ds wrapper-small mh-auto ph-m">
			<?php 
				if ($wines->have_posts()) :
          while ($wines->have_posts()) :
            $wines->the_post();
            global $post;
            $wineID = $post->ID;
						$is_gift = get_field('type_varietal')['wine_type'] === 'Gift';
            $vivino_rating = get_field('vivino_rating');
            $vivino_count = get_field('vivino_count');
            $price = get_field('price');
            $dates = get_field('dates');
            $bottle_photos = get_field('bottle_photos');
            $gift_pack_photos = get_field('gift_pack_photos');
            $badge_message = get_field('badge_message');
            $vivino_url = get_field('vivino_score');
						$wine_sku = get_field('sku');
						$is_cat_page = false; //set to true to prevent full width cards
						
						if ($category === "Pinot Noir" && get_field('type_varietal')['wine_type'] === "Rosé") {
							continue;
						} elseif($category !== "Culinary & Gifting" && ($wine_sku && !$is_gift)) {
							include( locate_template( 'inc/partials/wine_card/wine_card.php', false, false ) );
						} elseif ($category === "Culinary & Gifting" && $wine_sku) {
							include( locate_template( 'inc/partials/wine_card/wine_card.php', false, false ) );
						}
					
					$index++;
          endwhile;
          wp_reset_postdata();
				else :
					echo "<p class='h5'>Sorry! There aren't any " . $post->post_title . " wines here.</p>";
        endif; 
			?>
		</section>
	</div>
	<div id="category-post-content-ajax" class="flex justify-between-nl flex-wrap mxw-1952 mxw-1162-ds wrapper-small mh-auto ph-m page-shop__content"></div>
	
	<?php FLEX::blocks(); ?>
</div>
<?php get_footer();?>