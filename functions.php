<?php


/*************************************************************
Theme Configuration Variables

Primary setup logic contained in inc/core/functions.php and 
associated files

Implementation specific config variables are defined here.
*************************************************************/


// Error reporting, set to false for production
$error_reporting = true;


// Define theme text domain
$theme_text_domain = 'hwy29creative';


// Additional image thumbnail sizes
// Name, file prefix, width, height, crop
$custom_thumbnail_sizes = array(
	array('1200px wide', 'hwy29creative-1200px-wide', 1000, 9999, false),
	array('800px wide', 'hwy29creative-800px-wide', 800, 9999, false ),
	array('400px wide', 'hwy29creative-400px-wide', 400, 9999, false ),
	array('200px wide', 'hwy29creative-200px-wide', 200, 9999, false )
);


// Automatically create style guide page?
$create_style_guide_page = true;



// Create simplified custom post types
// NOTE: if CPT block specific then build in block -setup.php file
$custom_post_types = array(
	
	// Sample of config
	/*array(
		'name' => 'team_members',
		'singular' => 'Team Member',
		'plural' => 'Team Members',
		'desc' => 'list of team members',
		'public' => true,
		'show_ui' => true,
		'menu_position' => 5,
		'capability' => 'page',
		'icon' => 'dashicons-admin-users',
		'slug' => 'team_grid'
	)*/
);



// For full control on custom post types include build file
// Use below as a template
//require_once( 'inc/custom-post-types/cpt-build-template.php' );

require_once( 'inc/core/custom-post-types/wines.php');
require_once( 'inc/core/custom-post-types/clubs.php');
require_once( 'inc/core/custom-post-types/experiences.php');
require_once( 'inc/core/custom-post-types/events.php');
require_once( 'inc/core/custom-post-types/recipes.php');
require_once( 'inc/core/custom-post-types/wine_category.php');


require_once( 'inc/core/setup-parts/breadcrumbs.php');


/*************************************************************
Theme Custom Functions - Add them below here
*************************************************************/
// Enqueue WP Admin Dashboard stylesheet
function set_admin_styles() {
	wp_enqueue_style( 'admin-styles', get_stylesheet_directory_uri() . '/assets/styles/wp-dashboard.css' );
}
add_action( 'admin_enqueue_scripts', 'set_admin_styles');


/*************************************************************
Theme Core Functions - don't touch
*************************************************************/


// save text domain for global access
$GLOBALS[ 'theme_text_domain' ] = $theme_text_domain;


// require base script build script
require_once('inc/core/core.php');
require_once('inc/blocks/getVideoThumbnail.php');



/**
 * Load any client specific JS & CSS here
 */
function theme_scripts_and_styles() {

	// Any client specific global CSS or JS
  
	//Check if we are viewing a specific page template
	if(is_page()) { 
		global $wp_query;
 
		// Check for what page template is active
		$template_name = get_post_meta( $wp_query->post->ID, '_wp_page_template', true );

		// if specific page then add extra script(s) or css
		/*if ($template_name == 'page-your_template_name.php') {
			//load scripts & css
		}*/
   }
}
// enqueue base scripts and styles
add_action( 'wp_enqueue_scripts', 'theme_scripts_and_styles', 999 );


/*******************************
 * Reorder Dashboard Menu Items
 *******************************/

function wpse_custom_menu_order( $menu_ord ) {
	if ( !$menu_ord ) return true;

	return array(
		'index.php', // Dashboard
		'separator1', // First separator
		'edit.php?post_type=page', // Pages
		'upload.php', // Media
		'edit.php?post_type=wine', // Wines
		'edit.php?post_type=club', // Clubs
		'edit.php?post_type=experience', // Experiences
		'edit.php?post_type=event', // Events
		'edit.php', // Posts
		'edit.php?post_type=recipe', // Recipes
		'theme-general-settings',   // Theme Settings
	);
}
add_filter( 'custom_menu_order', 'wpse_custom_menu_order', 10, 1 );
add_filter( 'menu_order', 'wpse_custom_menu_order', 10, 1 );


/*************************
 * Remove Dashboard Menu
 *************************/

function remove_menus(){
	remove_menu_page( 'edit-comments.php' );          //Comments
}

add_action( 'admin_menu', 'remove_menus' );

/******************************
 * Rename Dashboard Menu Item
 ******************************/

 
function customize_post_admin_menu_labels() {
	global $menu;
	global $submenu;
	$menu[5][0] = 'Blog';
	$submenu['edit.php'][5][0] = 'Blog';
	$submenu['edit.php'][10][0] = 'Add Article';
	echo '';
}
add_action( 'admin_menu', 'customize_post_admin_menu_labels' );


/*=============================================
                BREADCRUMBS
=============================================*/
function the_breadcrumb($theme_location = 'main', $separator = ' &gt; ') {

	$items = wp_get_nav_menu_items($theme_location);
	var_dump($items);
	_wp_menu_item_classes_by_context( $items ); // Set up the class variables, including current-classes
	$crumbs = array();

	foreach($items as $item) {
		if ($item->current_item_ancestor) {
			$crumbs[] = "<a href=\"{$item->url}\" name=\"{$item->title}\" title=\"{$item->title}\">{$item->title}</a>";
		} else if ($item->current) {
			$crumbs[] = "<span name=\"{$item->title}\" title=\"{$item->title}\">{$item->title}</span>";
		}
	}
	echo implode($separator, $crumbs);
} // end the_breadcrumb()

/* Don't delete this closing tag. */



/*=============================================
      BLOCK RECIPES PARENT CATEGORIES EDIT
=============================================*/
function set_recipe_categories_walker( $args ) {
  if (
    ! empty( $args['taxonomy'] )
    && ( $args['taxonomy'] === 'recipe_categories' )
    && ( ! isset( $args['walker'] ) || ! $args['walker'] instanceof Walker )
  ) {
    $args['checked_ontop'] = FALSE;
    $args['walker'] = get_Walker_Category_No_Parent();
  }
  return $args;
}
add_filter( 'wp_terms_checklist_args', 'set_recipe_categories_walker' );

function get_Walker_Category_No_Parent() {
  class Walker_Category_No_Parent extends Walker_Category_Checklist {

   	function start_el( &$output, $category, $depth = 0, $args = array(), $id = 0 ) {
			// Hide wine categories
			// $wines_cat = get_term_by('slug', 'wines', 'recipe_categories');
		 	// if($category->term_id === $wines_cat->term_id || (int) $category->parent === $wines_cat->term_id) {
			// 	return;
			// }

      if ( (int) $category->parent === 0 ) {
				$this->doing_parent = esc_html( $category->name );
      } else {
        parent::start_el( $output, $category, $depth, $args, $id );
      }
    }

    function end_el( &$output, $category, $depth = 0, $args = array(), $id = 0 ) {
			// Hide wine categories
			// $wines_cat = get_term_by('slug', 'wines', 'recipe_categories');
		 	// if($category->parent === $wines_cat->term_id || (int) $category->parent === $wines_cat->term_id) {
			// 	return;
			// }

      if ( ! isset( $this->doing_parent ) || empty( $this->doing_parent ) ) {
        parent::end_el( $output, $category, $depth, $args, $id );
      }
    }

    function start_lvl( &$output, $depth = 0, $args = array() ) {
      if ( isset( $this->doing_parent ) && ! empty( $this->doing_parent ) ) {
        $output .= '<li><strong>' . $this->doing_parent . '</strong></li>';
        $this->doing_parent = FALSE;
      }
      parent::start_lvl( $output, $depth = 0, $args );
    }
  }
  return new Walker_Category_No_Parent;
}


/*=============================================
  	DELET UNUSED WINE CATEGORY ACFS
=============================================*/
function delete_unused_wine_cats( $post_id ) {
	// var_dump($_POST);
	$type = get_field('type_varietal', $post_id)['wine_type'];
	if($type === 'White') {
		if(get_field('type_varietal', $post_id)['red_varietals']) {
			delete_field('type_varietal_red_varietals', $post_id);
		}
		if(get_field('type_varietal', $post_id)['rose_varietals']) {
			delete_field('type_varietal_rose_varietals', $post_id);
		}
	}
	if($type === 'Rosé') {
		if(get_field('type_varietal', $post_id)['red_varietals']) {
			delete_field('type_varietal_red_varietals', $post_id);
		}
		if(get_field('type_varietal', $post_id)['white_varietals']) {
			delete_field('type_varietal_white_varietals', $post_id);
		}
	}
	if($type === 'Red') {	
		if(get_field('type_varietal', $post_id)['white_varietals']) {
			delete_field('type_varietal_white_varietals', $post_id);
		}
		if(get_field('type_varietal', $post_id)['rose_varietals']) {
			delete_field('type_varietal_rose_varietals', $post_id);
		}
	}
	if($type === 'Culinary' || $type === 'Dessert' || $type === 'Gift') {
		delete_field('dates', $post_id);
		
		if(get_field('type_varietal', $post_id)['size']) {
			delete_field('type_varietal_size', $post_id);
		}
		if(get_field('type_varietal', $post_id)['red_varietals']) {
			delete_field('type_varietal_red_varietals', $post_id);
		}
		if(get_field('type_varietal', $post_id)['rose_varietals']) {
			delete_field('type_varietal_rose_varietals', $post_id);
		}
		if(get_field('type_varietal', $post_id)['white_varietals']) {
			delete_field('type_varietal_white_varietals', $post_id);
		}
	}
}
add_action( 'save_post_wine', 'delete_unused_wine_cats' );

/*=============================================
      ADD ACF WINE CATEGORIES TO RECIPES
=============================================*/
function recipe_wine_categories( $post_id, $post, $update ) {
	$featured_wines = get_field('featured_wines', $post_id);
	
	// Create Wines parent category if it doesn't exist already
	if(!term_exists( 'wines', 'recipe_categories' )) {
		wp_insert_term(
			'Wines',
			'recipe_categories',
			array(
				'slug' => 'wines'
			)
		);
	}
	if($featured_wines) {
		foreach($featured_wines as $wine) {
			$type_varietal_name = get_field_object('type_varietal', $wine)['value']['wine_type'];
			$type_varietal_slug = strtolower($type_varietal_name);
			$type_varietal_slug = str_replace(' ', '_', $type_varietal_slug);
			$parent_term = term_exists( 'wines', 'recipe_categories' ); 
			$parent_term_id = (int)$parent_term['term_id'];
			
			// Create category if it doesn't exists already
			if(!term_exists( $type_varietal_slug, 'recipe_categories' )) {
				wp_insert_term(
					$type_varietal_name,
					'recipe_categories',
					array(
						'slug' => $type_varietal_slug,
						'parent' => $parent_term_id,
					)
				);
			}

			// Save category on post
			$cat_term_id = (int)term_exists( $type_varietal_slug, 'recipe_categories' );
			// var_dump($_POST);
			wp_set_object_terms( $post_id, $type_varietal_slug, 'recipe_categories', true );
		}
	}
}
add_action( 'save_post_recipe', 'recipe_wine_categories', 10, 3 );


/*=============================================
      AJAX CALLS
=============================================*/
add_action( 'wp_ajax_nopriv_search-wines', 'prefix_search_wine' );
add_action( 'wp_ajax_search-wines', 'prefix_search_wine' );
function prefix_search_wine() {
	$has_name = isset($_POST[ 'name' ]) && $_POST[ 'name' ] !== '';
	$has_year = isset($_POST[ 'year' ]) && $_POST[ 'year' ] !== '';
	$is_wine = isset($_POST[ 'isWine' ]) && $_POST[ 'isWine' ];
	
	if(!$is_wine && $has_name) {
		$wine_name = $_POST[ 'name' ];
		
		$args = array (
			'post_type' => 'wine',
			'posts_per_page' => -1,
			'meta_query' =>  array(
				'relation' => "AND",
				array(
					'key' => 'wine_name',
					'compare' => 'LIKE',
					'value' => $wine_name
				),
				array(
				'key' => 'wine_card_type',
				'value' => 'library',
				'compare' => 'NOT LIKE',
				)
			)
		);
	} elseif ($has_name && $has_year) {
		$wine_name = $_POST[ 'name' ];
		$wine_year = $_POST[ 'year' ];
		$args = array (
			'post_type' => 'wine',
			'posts_per_page' => -1,
			'meta_query' =>  array(
				'relation' => "AND",
				array(
					'key' => 'type_varietal_label_name',
					'compare' => 'LIKE',
					'value' => $wine_name
				),
				array(
					'key' => 'dates_vintage',
					'compare' => 'LIKE',
					'value' => $wine_year
				),
				array(
				'key' => 'wine_card_type',
				'value' => 'library',
				'compare' => 'NOT LIKE',
				)
			)
		);

	} elseif(!$has_name) {
		echo "missing name";
		die();
	} elseif($is_wine && !$has_year) {
		echo "missing year";
		die();
	}
	
	$wines = new WP_Query($args);
	ob_start (); 
	
	if ( $wines->have_posts() ) :
		while ( $wines->have_posts() ) : $wines->the_post();
		$bottle = get_field('bottle_photos')['primary_photo'];
		$downloads = get_field('downloads');
		$tech_sheet = $downloads['tech_sheet'];
		$label = $downloads['label'];
		$shelf_talker = $downloads['shelf_talker'];
		$permalink = get_permalink();
?>
		<div class="flex justify-between-nl items-start-nl pt-l pt-xl-l">
			<div class="mr-xl mr-xxl-l">
				<a href="<?php echo $permalink; ?>" class="db mb-s f3 f2-l tc-primary-text"><?php the_title(); ?></a>
				<div class="flex-l items-center">
					<?php if ($bottle) : ?>
						<a href="<?php echo $bottle; ?>" target="blank" class="db-nl mb-s-nl f-secondary lh4 ls-tinier fw-300 tc-link h-tc-link-hover td-40">Bottle Shot</a>
					<?php endif; ?>
					<?php if ($tech_sheet) : ?>
						<span class="dn-nl ph-m">|</span>
						<a href="<?php echo $tech_sheet; ?>" target="blank" class="db-nl mb-s-nl f-secondary lh4 ls-tinier fw-300 tc-link h-tc-link-hover td-40">Tech Sheet</a>
					<?php endif; ?>
					<?php if ($shelf_talker) : ?>
						<span class="dn-nl ph-m">|</span>
						<a href="<?php echo $shelf_talker; ?>" target="blank" class="db-nl mb-s-nl f-secondary lh4 ls-tinier fw-300 tc-link h-tc-link-hover td-40">Shelf Talker</a>
					<?php endif; ?>
					<?php if ($label) : ?>
						<span class="dn-nl ph-m">|</span>
						<a href="<?php echo $label; ?>" target="blank" class="db-nl mb-s-nl f-secondary lh4 ls-tinier fw-300 tc-link h-tc-link-hover td-40">Label</a>
					<?php endif; ?>
				</div>
			</div>
			<button class="js-clear-search tc-link icon-close va-middle fw-800"></button>
		</div>
<?php
		endwhile;
		wp_reset_postdata();

		$response = ob_get_contents();
		ob_end_clean();
		echo $response;
		die(1);
	else :
		echo "<p>Aww, shucks! No results</p>";
		die();
	endif;
}

add_action( 'wp_ajax_nopriv_load_recipe_filter', 'prefix_load_term_recipes' );
add_action( 'wp_ajax_load_recipe_filter', 'prefix_load_term_recipes' );
function prefix_load_term_recipes() {
	$is_filter = isset($_POST[ 'recipe_cats' ]);
	if ($is_filter) {
		$filtered_cats = $_POST[ 'recipe_cats' ];
		$taxonomy = 'recipe_categories';
		$recipe_args = array(
			'post_type' => 'recipe',
			'posts_per_page' => -1,
			'tax_query' => array(
				'relation' => "AND"
			)
		);

		foreach($filtered_cats as $group_cats) {
			$recipe_subargs = array(
				'tax_query' => array(
					'relation' => "OR",
				)
			);
			
			foreach($group_cats as $cat) {
				array_push(
					$recipe_subargs['tax_query'],
					array(
						'taxonomy' => $taxonomy,
						'field' => 'slug',
						'terms' => $cat
					)
				);
			}
			array_push($recipe_args['tax_query'], $recipe_subargs);
		}
	} else {
		echo "show_all";
		die();
	}
	global $post;
	$recipes = new WP_Query($recipe_args);
	ob_start (); 
	
	if ( $recipes->have_posts() ) :
		while ( $recipes->have_posts() ) : $recipes->the_post();
			$title = get_field('recipe_title');
			$url = get_permalink();
			$thumbnail = get_field('recipe_photos')['recipe_thumbnail_photo'];
			$categories = wp_get_post_terms($post->ID, $taxonomy);
			$categories_amount = count($categories);
			$j = 1;
		
			include( locate_template( 'inc/partials/recipe_card/recipe_card.php', false, false ) );

		endwhile;
		wp_reset_postdata();
	else :
?>
		<div class="no-result" style="background-image: url(<?php echo get_stylesheet_directory_uri().'/assets/images/no-results.png';?>)">
			<div>
				<h1>Aww, shucks! No results</h1>
				<p>Try removing some filters.</p>
			</div>
		</div>
<?php 
	endif;
		
	$response = ob_get_contents();
	ob_end_clean();
	echo $response;
	die(1);
}

add_action( 'wp_ajax_nopriv_load-filter2', 'prefix_load_term_posts' );
add_action( 'wp_ajax_load-filter2', 'prefix_load_term_posts' );
function prefix_load_term_posts () {
	$args = array (
		'post_type' => 'wine',
		'posts_per_page' => -1,
    'meta_query' => array(
      'relation' => "AND",
		array(
			'key' => 'wine_card_type',
			'value' => 'library',
			'compare' => 'NOT LIKE',
		)
    )
	);
	$is_filter = false;

	if(isset($_POST[ 'packages' ])) {
		$is_filter = true;
		$packages = $_POST[ 'packages' ];
		$array_packages ['relation'] = "OR";
		foreach($packages as $package){
			$pac_name = $package === "bottle" ? 'standard' : ($package === "gifting" ? 'featured' : $package);
			array_push($array_packages ,
				array(
					'key' => 'wine_card_type',
					'compare' => 'LIKE',
					'value' => $pac_name
				)
			);
		}
		array_push($args['meta_query'] , $array_packages);
	}

	if(isset($_POST[ 'size_ids' ])) {
		$is_filter = true;
		$sizes = $_POST[ 'size_ids' ];
		$array_sizes ['relation'] = "OR";
		foreach($sizes as $size) {
			array_push($array_sizes ,
				array(
					'key' => 'type_varietal_size',
					'compare' => 'LIKE',
					'value' => $size
				)
			);
		}
		array_push($args['meta_query'] , $array_sizes);
	}
	
	if(isset($_POST[ 'varietal_ids' ])) {
		$is_filter = true;
		$varietal_ids = $_POST[ 'varietal_ids' ];
		$array_varietal ['relation'] = "OR";
		foreach($varietal_ids as $varietal){
			array_push($array_varietal ,
				array(
					'key' => 'type_varietal_'.$varietal['key'],
					'value' => $varietal['title'],
					'compare' => '='
				)
			);
		}
		array_push($args['meta_query'] , $array_varietal);
	}

	if(isset($_POST[ 'price_range' ])) {
		$is_filter = true;
		$price_range = $_POST[ 'price_range' ];
		array_push($args['meta_query'],
			array(
				'key'       => 'price',
				'value' => $price_range,
				'compare' => 'BETWEEN',
				'type' => 'NUMERIC'
			)
		);
	}

	if(isset($_POST[ 'type_ids' ])) {
		$is_filter = true;
		$type_ids = $_POST[ 'type_ids' ];
		$array_varietal2 ['relation'] = "OR";
		foreach($type_ids as $type) {
			if($type === 'Whites & Rosé') {
				array_push($array_varietal2 ,
					array(
						'key' => 'type_varietal_wine_type',
						'value' => array('White', 'Rosé'),
						'compare' => 'IN'
					)
				);
			} else {
				array_push($array_varietal2 ,
					array(
						'key' => array('type_varietal_wine_type', 'type_varietal_red_varietals', 'type_varietal_rose_varietals', 'type_varietal_white_varietals'),
						'value' => $type,
						'compare' => '='
					)
				);
			}
		}
		array_push($args['meta_query'] , $array_varietal2);
	}

	if(isset($_POST[ 'vintage_ids' ])) {
		$is_filter = true;
		$vintage_ids = $_POST[ 'vintage_ids' ];
		$array_varietal3 ['relation'] = "OR";
		foreach($vintage_ids as $type){
			array_push($array_varietal3 ,
				array(
					'key' => 'dates_vintage',
					'value' => $type,
					'compare' => '='
				)
			);
		}
		array_push($args['meta_query'] , $array_varietal3);
	}
      
	global $post;
	
	if(!$is_filter){
		echo "show_all";
		die();
	}
	$wines = get_posts( $args );
	ob_start (); 

	if(count($wines) === 0) : 
?>
		<div class="no-result" style="background-image: url(<?php echo get_stylesheet_directory_uri().'/assets/images/no-results.png';?>)">
			<div>
				<h1>Aww, shucks! No results</h1>
				<p>Try removing some filters.</p>
			</div>
		</div>
<?php 
	else :
		$prices = array();
		foreach($wines as $index => $wine) :
			$wineID = $wine->ID;
			$vivino_rating = get_field('vivino_rating', $wineID);
			$vivino_count = get_field('vivino_count', $wineID);
			$price = get_field('price', $wineID);
			$dates = get_field('dates', $wineID);
			$bottle_photos = get_field('bottle_photos', $wineID);
			$gift_pack_photos = get_field('gift_pack_photos', $wineID);
			$badge_message = get_field('badge_message', $wineID);
			$vivino_url = get_field('vivino_score', $wineID);
			$wine_sku = get_field('sku', $wineID);
			
			$price = get_field('price', $wine->ID);
			if ($price !== 0) array_push($prices, $price);

			if (get_field('wine_card_type', $wineID) === 'featured' && !isset($_POST[ 'packages' ])) {
				continue;
			}

			$is_pinot = isset($_POST['type_ids']) && $_POST['type_ids'][0] === "Pinot Noir";
			$is_pinot_filter = isset($_POST['varietal_ids']) && $_POST['varietal_ids'][0]['title'] === "Pinot Noir";
			if (($is_pinot && get_field('type_varietal', $wineID)['wine_type'] === "Rosé") || ($is_pinot_filter && get_field('type_varietal', $wineID)['wine_type'] === "Rosé")) {
				continue;
			}
			
			if($wine_sku) {
				include( locate_template( 'inc/partials/wine_card/wine_card.php', false, false ) );
			}
			
		endforeach;
		$prices = array_unique($prices);
		$prices = array_filter($prices);
		if (count($prices) > 0) {
			$max = max($prices);
			$min = min($prices);
			
			echo '<div class="js-new-prices" data-min="'. $min .'" data-max="' . $max .'"></div>';
		}
	endif;
	
	wp_reset_postdata(); 
	$response = ob_get_contents();
	ob_end_clean();
	echo $response;
	die(1);
}

add_action('admin_head', 'my_custom_fonts');
function my_custom_fonts() {
  echo '<style>
    div[data-name=vivino_rating]{
        display:none;
    }
    div[data-name=vivino_count]{
        display:none;
    }
    div[data-name=price] input{
        pointer-events: none;
        background-color:#cecec3;
    }
    div[data-name=format_name] input{
        pointer-events: none;
        background-color:#cecec3;
    }
    div[data-name=alternate_format_sku] input{
        pointer-events: none;
        background-color:#cecec3;
    }
    div[data-name=alternate_price] input{
        pointer-events: none;
        background-color:#cecec3;
    }
  </style>';
}

add_filter('acf/load_value/type=textarea', 'remove_quote');
add_filter('acf/load_value/type=text', 'remove_quote');
function remove_quote( $value ) {
	return wptexturize( $value );
}

add_action( 'acf/save_post', 'my_save_post_function');
function my_save_post_function( $post_ID) {
  global $post; 

	$screen = get_current_screen();
  if($screen->base == 'toplevel_page_theme-general-settings'){
    $wine_args = array (
			'post_type' => 'wine',
			'posts_per_page' => -1,
			'post_status' => 'publish'
    );
    $wines = get_posts( $wine_args );
    $fpGoogle = fopen($_SERVER['DOCUMENT_ROOT'].'/datafeed/google.xml', 'w');
    $fpFaceBook = fopen($_SERVER['DOCUMENT_ROOT'].'/datafeed/facebook.xml', 'w');
    $xml = '';
    $xml = $xml.'<rss xmlns:g="http://base.google.com/ns/1.0" version="2.0">';
    $xml = $xml.'<channel><title>'.get_field('winery_name', 'option').'Products Feed</title><description>'.get_field('winery_name', 'option').'Shopping Feed</description>';
    $xml = $xml.'<link>'.home_url().'</link>';
    foreach($wines as $wine) {
			$bottle_photos = get_field('bottle_photos',$wine->ID);
			$gift_pack_photos = get_field('gift_pack_photos',$wine->ID);
			if(get_field('wine_card_type',$wine->ID) != 'featured'){
				$image_src= 'standard' == get_field('wine_card_type', $wine->ID) ? $bottle_photos['primary_photo'] : $gift_pack_photos['gift_pack_image']; 
				$xml = $xml.'<item><g:id>'.$wine->ID.'</g:id>';
				$xml = $xml.'<g:title>'.get_the_title($wine->ID).'</g:title>';
				$xml = $xml.'<g:description>'.get_field('description', $wine->ID).'</g:description>';
				$xml = $xml.'<g:link>'.get_permalink( $wine->ID ).'</g:link>';
				$xml = $xml.'<g:image_link>'. $image_src.'</g:image_link>';
				$xml = $xml.'<g:condition>new</g:condition>';
				$xml = $xml.'<g:availability>in stock</g:availability>';
				$xml = $xml.'<g:price>'.get_field('price', $wine->ID).' USD</g:price>';
				$xml = $xml.'<g:shipping><g:country>US</g:country><g:service>Standard</g:service><g:price>0.00 USD</g:price></g:shipping>';
				$xml = $xml.'<g:brand>'.get_field('winery_name', 'option').'</g:brand>';
				$xml = $xml.'<g:gtin/><g:mpn>'.get_field('sku',$wine->ID).'</g:mpn>';
				$xml = $xml.'<g:google_product_category>421</g:google_product_category></item>';
			}
    }
    $xml = $xml.'</channel></rss>';
    update_option('options_data_feeds_google_merchant_center', home_url().'/datafeed/google.xml');
    update_option('options_data_feeds_facebook_product_catalog', home_url().'/datafeed/facebook.xml');
    fwrite($fpGoogle, $xml);
    fwrite($fpFaceBook, $xml);
    fclose($fpGoogle);
    fclose($fpFaceBook);
	}
	
  if (isset($post) && $post->post_type !== 'wine'){
    return;
	}
	
  $wine_sku = get_field('sku',$post_ID);
  $vivino_url = get_field('vivino_score',$post_ID);
  $vivino_id_str_position = strpos($vivino_url,"/w/");
  $vivino_year_str_position = strpos($vivino_url,"year=");
  $vivino_sub_str = substr($vivino_url, $vivino_id_str_position + 3);
  $vivino_sub_year_str = substr($vivino_url, $vivino_year_str_position + 5);
  $vivino_id = substr($vivino_sub_str , 0, strpos($vivino_sub_str,"?"));
  $vivino_year = substr($vivino_sub_year_str , 0, strpos($vivino_sub_year_str,"&"));
  $vivino_api_url = "http://api.vivino.com/wines/".$vivino_id;
  $vivino_data = file_get_contents($vivino_api_url);
  $vivino_json = json_decode($vivino_data,true);
	$vivino_rating = array();
	
	if($vivino_json) {
		foreach($vivino_json['vintages'] as $vivino_vintage){
			if($vivino_vintage['year'] == $vivino_year){
				$vivino_rating = $vivino_vintage['statistics'];
			}
		}
	}
	
  $wine_url = get_field("store_url", "option").'/index.cfm?method=remote.addToCartForm&productSKU='.$wine_sku;
  $html = file_get_contents($wine_url);
  $doc = new DOMDocument();
  $doc->loadHTML($html);
  $classname="v65-widgetAddToCart";
  $finder = new DomXPath($doc);
  $nodes = $finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' $classname ')]");
  $k = 0;
  $price = '';
  $sku_name = '';
  $format_name='';
  $bottleprice = '';
  foreach ($nodes as $node) {
    $k++;
    
    if( $k >= 3 ) {
			$unitclassname="v65-widgetProduct-addToCart-unitDescription";
			$SKUclassname="v65-widgetProduct-addToCart-productSKU";
			$priceclassname="v65-widgetProduct-addToCart-price";
			$format_name = $finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' $unitclassname ')]",$node);
			$sku_name = $finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' $SKUclassname ')]",$node);
			$price = $finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' $priceclassname ')]",$node);
			$format_name = $format_name[1]->nodeValue;
			$format_name = substr($format_name , 1);
			$sku_name = $sku_name[1]->nodeValue;
			$sku_name = substr($sku_name , 5);
			$price = $price[2]->nodeValue;
			$dot_position = strpos($price, '.');
			$price = substr($price , 1 , $dot_position-1);
    } else {
			$bottlepriceclassname="v65-widgetProduct-addToCart-price";
			$bottleprice = $finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' $bottlepriceclassname ')]",$node);
			$bottleprice = $bottleprice[0]->nodeValue;
			$dot_position = strpos($bottleprice, '.');
			$bottleprice = substr($bottleprice , 1 , $dot_position-1);
    }
  }
  if ($vivino_rating) {
		update_post_meta($post_ID, 'vivino_rating', $vivino_rating['ratings_average']);
		update_post_meta($post_ID, 'vivino_count', $vivino_rating['ratings_count']);
	}
  update_post_meta($post_ID, 'price', $bottleprice);
  update_post_meta($post_ID, 'alternate_format_name', $format_name);
  update_post_meta($post_ID, 'alternate_alternate_format_sku', $sku_name);
  update_post_meta($post_ID, 'alternate_alternate_price', $price);
  $wine_categories_args = array(
    'post_type' => 'winecategory',
    'posts_per_page' => -1,
  );

  $wine_categories = get_posts( $wine_categories_args );
	
  $wine_args = array (
    'post_type' => 'wine',
    'posts_per_page' => -1,
  );
	$wines = get_posts( $wine_args );
  $wine_product_types = array();
  $wine_types = array();
  $wine_sizes = array();
  $wine_varietals = array();
  $wine_other_categories = array();
  $vintage = array();

  foreach($wines as $wine) { 
		$wine_card_type = get_field('wine_card_type', $wine->ID);
		$wine_varietal_type = get_field('type_varietal', $wine->ID)['wine_type'];
		$other_categories = get_field('other_categories', $wine->ID);
		if($wine_card_type === 'featured' || $wine_varietal_type === 'Culinary' || $wine_varietal_type === 'Gift' || $wine_varietal_type === 'Dessert' || $other_categories === 'Gifts') {
			array_push($wine_product_types, 'Culinary & Gifting');
			if($wine_varietal_type !== 'Gift' || $other_categories !== 'Gifts') {
				continue;
			}
		}
		
		$categories = get_field('other_categories', $wine->ID);
		if ($categories) {
			foreach($categories as $category) {
				array_push($wine_other_categories, $category['label']);
			}
		}

		$type_varietal = get_field('type_varietal', $wine->ID);
		$wine_size = get_field('type_varietal', $wine->ID)['size'];
		array_push($wine_sizes, $wine_size);
		
		$type_varietal_object = get_field_object('type_varietal', $wine->ID);
    for ($i = 1; $i < count($type_varietal_object['sub_fields']); $i++) {
			if ($type_varietal_object['sub_fields'][$i]['name'] !== 'size') {
				if ( $type_varietal['wine_type'] === $type_varietal_object['sub_fields'][$i]['conditional_logic'][0][0]['value']) {
					$varietal = array(
						'key' => $type_varietal_object['sub_fields'][$i]['name'],
						'title'=> $type_varietal[$type_varietal_object['sub_fields'][$i]['name']]
					);
					array_push($wine_varietals , $varietal);
				}
			}
    }
		$dates = get_field('dates',$wine->ID);
    array_push($vintage, $dates['vintage']);
    array_push($wine_types, $type_varietal['wine_type']);
	}
	
  $wine_product_types = array_unique($wine_product_types);
  $wine_varietals = array_unique($wine_varietals, SORT_REGULAR);
  $wine_types = array_unique($wine_types);
  $wine_other_categories = array_unique($wine_other_categories);
  $wine_sizes = array_unique($wine_sizes);
	$vintages = array_unique($vintage);
	
  if((count($wine_other_categories) + count($wine_sizes) + count($wine_product_types) + count($wine_varietals) + count($wine_types) + count($vintages)) !== count($wine_categories)){
    if(!empty($wine_categories)){
			foreach ( $wine_categories as $wine_category) {
				wp_delete_post( $wine_category->ID );
			}
    }
    foreach($wine_product_types as $wine_product_type) {
			if($wine_product_type !== '') {
				wp_insert_post(array (
					'post_type' => 'winecategory',
					'post_title' => $wine_product_type,
					'post_status' => 'publish'
				));
			}
    }
    foreach($wine_other_categories as $wine_other_category) {
			if($wine_category !== '') {
				wp_insert_post(array (
					'post_type' => 'winecategory',
					'post_title' => $wine_other_category,
					'post_status' => 'publish'
				));
			}
    }
    foreach($wine_sizes as $wine_size) {
			if($wine_size !== '') {
				wp_insert_post(array (
					'post_type' => 'winecategory',
					'post_title' => $wine_size,
					'post_status' => 'publish'
				));
			}
    }
    foreach($wine_types as $wine_type) {
			if($wine_type !== '') {
				wp_insert_post(array (
					'post_type' => 'winecategory',
					'post_title' => $wine_type,
					'post_status' => 'publish'
				));
			}
    }
    foreach($wine_varietals as $wine_varietal) {
			if($wine_varietal['title'] !== '') {
				$post_id = wp_insert_post(array (
					'post_type' => 'winecategory',
					'post_title' => $wine_varietal['title'],
					'post_status' => 'publish'
				));
			}
    }
    foreach($vintages as $vintage){
			if($vintage !== '') {
				wp_insert_post(array (
					'post_type' => 'winecategory',
					'post_title' => $vintage,
					'post_status' => 'publish'
				));
			}
		}
	}
	wp_insert_post(array (
		'post_type' => 'winecategory',
		'post_title' => 'Whites & Rosé',
		'post_status' => 'publish'
	));
}

add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
function special_nav_class ($classes, $item) {
  if (in_array('current-menu-item', $classes) ){
    $classes[] = 'active ';
  }
  return $classes;
}

add_filter('acf/load_field/name=pop_up_selection', 'my_acf_load_field');
function my_acf_load_field( $field ) {
	$field['choices'] = array(
		'option1' => '<img src="'.get_stylesheet_directory_uri().'/assets/images/option1.jpg"/>',
		'option2' => '<img src="'.get_stylesheet_directory_uri().'/assets/images/option2.jpg" />',
		'option3' => '<img src="'.get_stylesheet_directory_uri().'/assets/images/option3.jpg"/>'
	);
	return $field;
}

function my_load_field($field) {
	$field['readonly'] = 1;
	return $field;
}
add_filter("acf/load_field/key=field_5eda67a6b2542", "my_load_field");
add_filter("acf/load_field/key=field_5eda67c4b2543", "my_load_field");
add_filter("acf/load_field/key=field_5eda6807b2544", "my_load_field");
add_filter("acf/load_field/key=field_5eda6837b2545", "my_load_field");


//////////////////////////////////
// Remove Defaut Page Text Editor
//////////////////////////////////
add_action('init', 'init_remove_support',100);
function init_remove_support(){
	$post_type = 'page';
	remove_post_type_support( $post_type, 'editor');
}

add_filter('acf/format_value/type=textarea', 'do_shortcode', 10, 3);

?>