<?php

	if  ( have_posts() ) {
		while( have_posts() ) {
			the_post();
		}
	}

	$postType = get_post_type_object(get_post_type());
	if ($postType) {
		$postType = esc_html($postType->labels->singular_name);
	} else {
		$postType = '';
	}
	
	$title = get_field('event_title');
	$photos = get_field('event_photos');
	$photo = $photos['event_main_photo'];
	$description = get_field('event_description');
	
	$date = get_field('event_date_time');
	$dateTime = $date['event_date'];
	$date_start = $date['event_start_time'];
	$date_end = $date['event_end_time'];
	$days = array('Sunday', 'Monday', 'Tuesday', 'Wednesday','Thursday','Friday', 'Saturday');
	$dayofweek = $days[date('w', strtotime($dateTime))];
	$complete_date = $dayofweek . ', ' . $dateTime;
	
	$venue = get_field('venue_details');
	$venue_name = $venue['event_venue_name'];
	$venue_address_arr = $venue['event_venue_address'];
	$streetaddress = $venue_address_arr['streetaddress'];
	$addresslocality = $venue_address_arr['addresslocality'];
	$addressregion = $venue_address_arr['addressregion'];
	$postalcode = $venue_address_arr['postalcode'];
	$addresscountry = $venue_address_arr['addresscountry'];
	$venue_address = $streetaddress . ' ' . $addresslocality . ', ' . $addressregion . ' ' . $postalcode . ', ' . $addresscountry;
	$address_empty = empty($streetaddress) && empty($addresslocality) && empty($addressregion) && empty($postalcode) && empty($addresscountry);
	
	$ticket = get_field('event_tickets');
	$event_cost = $ticket['event_cost'];
	$regular_price = $event_cost['general_admission'];
	$promotion_price = $event_cost['promotion'];
	$event_how_purchase = $ticket['event_how_to_purchase'];
	$event_sku = $ticket['event_sku'];
	$event_link = $ticket['event_external_link'];

	get_header();
?>

<section class="mb-xxl mb-xxxl-l pb-m-l">
	<div class="mxw-1952 wrapper-small mh-auto ph-m mb-xxl mb-xxxl-l pb-m-l single-event-page">
	<div class="mb-xl">
		<?php custom_breadcrumbs(); ?>
	</div>
		<div class="flex-l justify-between">
			<div class="w-p-100 mxw-670-nxl mxw-900">
				<h1 class="mb-xl h2 f-title fw-300 tc-primary-text"><?php echo $title; ?></h1>
				<div class="mb-xxl mb-xxxl-l bg-cover bg-center single-event-page__image" style="background-image: url('<?php echo $photo; ?>')"></div>
				<div class="dn-nl mxw-670 tc-brown f3 f2-l intro-text"><?php echo $description; ?></div>
			</div>
			<div class="w-p-100 mb-l-nl pl-xl-l f-secondary single-event-page__sidebar">
				<div class="pb-l pb-xl-l bdb-1 f6-s">
					<p class="mb-s-l uppercase ls-tiny">Date</p>
					<p class="mb-m fw-300 lh4 ls-tinier"><?php echo $complete_date; ?></p>
					<p class="mb-s-l uppercase ls-tiny">Time</p>
					<p class="mb-m fw-300 lh4 ls-tinier"><?php echo str_replace(' ', '', $date_start); ?> to <?php echo str_replace(' ', '', $date_end); ?></p>
					<p class="mb-s-l uppercase ls-tiny">Location</p>
					<?php $is_home = strpos($venue_name, 'Trefethen'); ?>
					<?php if ($address_empty) : ?>
						<p class="f-secondary f6-s fw-300 lh4 ls-tinier tc-link"><span class="dib mr-s <?php echo is_numeric($is_home) ? 'icon-trefethen-location f3' : 'icon-location' ?> tc-primary-text"></span><?php echo $venue_name; ?></p>
					<?php else : ?>
						<a href="https://maps.google.com/?q=term<?php echo $venue_address; ?>" target="blank" class="f-secondary f6-s fw-300 lh4 ls-tinier tc-link"><span class="dib mr-s <?php echo is_numeric($is_home) ? 'icon-trefethen-location f3' : 'icon-location' ?> tc-primary-text"></span><?php echo $venue_name; ?></a>
					<?php endif; ?>
				</div>
				<div class="mb-l mb-xl-l pt-l pt-xl-l">
					<?php if ($event_how_purchase === 'winedirect'): ?>
						<button class="db pv-s ph-xxl bdr-3 tc-white ta-center bg-link h-bg-link td-40 single-event-page__btn" data-image="<?php echo $photos['event_thumbnail_photo'];?>">Register</button>
						<!-- <div class="wd-form" v65remotejs="addToCartForm" productsku="<?php echo $event_sku; ?>"></div> -->
					<?php elseif ($event_how_purchase === 'link'): ?>
						<a class="db pv-s ph-xxl bdr-3 tc-white ta-center bg-link h-bg-link td-40 single-event-page__btn" href="<?php echo $event_link; ?>" target="_blank">Register</a>
					<?php endif; ?>
				</div>
				<p class="f6-s lh4 ls-tinier"><?php echo $regular_price; ?></p>
				<?php if ($promotion_price) : ?>
					<p class="f6-s lh4 ls-tinier"><?php echo $promotion_price; ?></p>
				<?php endif; ?>
			</div>
		</div>
		<div class="dn-l mxw-670 tc-brown f3 f2-l intro-text"><?php echo $description; ?></div>
	</div>
	
	<?php 
		$featured_links_field = get_field('event_featured_links'); 
		$block_class_identifier = 'featured-links';
		$use_default = $featured_links_field['use_default'];
		if ($use_default) {
			$section_title = get_field('events_default_promo_title', 'options');
			$align_title = get_field('events_default_promo_align_title', 'options')['align_title'];
			$featured_links = get_field('events_default_promo_featured_links', 'options')['featured_link'];
			$optional_link = get_field('events_default_promo_optional_link', 'options')['optional_link'];
		} else {
			$featured_links_content = $featured_links_field['events_featured_links'];
			$section_title = $featured_links_content['events_featured_section_title'];
			$align_title = $featured_links_content['text_block_field'];
			$featured_links = $featured_links_content['featured_link'];
			$optional_link = $featured_links_content['optional_link'];
		}
		$amount = count($featured_links);
		$is_double = $amount < 3;
	?>
	<section class="content-block <?php echo $block_class_identifier; ?> <?php echo $is_double ? $block_class_identifier . '--double' : $block_class_identifier . '--triple';?>">
		<?php
			if ($section_title) {
				include( locate_template( 'inc/partials/section_title/section_title.php', false, false ) ); 
			}

			if ($featured_links) : 
		?>
				<div class="flex-l mxw-1952 mxw-1162-ds wrapper-small mh-auto  <?php if ($section_title) { echo 'mt-m'; }?> ph-m">
					<ul class="relative z-1 flex flex-wrap w-p-100 ph-m-s <?php echo $block_class_identifier; ?>__list">
			<?php
					foreach($featured_links as $link_item) {
						$image = $link_item['featured_link_image'];
						$title = $link_item['featured_link_tilte'];
						$description = $link_item['featured_link_description'];
						$link = $link_item['featured_link_url'];
						$link_title = $link ? $link['title'] : '';
						$link_url = $link ? $link['url'] : '#';
						
						include( locate_template( 'inc/partials/featured_link/featured_link.php', false, false ) ); 

					}
			?>
					</ul>
			<?php endif; ?>
			<?php if ($optional_link) : ?>
				<?php 
					$title = $optional_link['title']; 
					$url = $optional_link['url']; 
				?>
				<div class="relative dib-nl <?php echo $block_class_identifier; ?>__optional-link">
					<a href="<?php echo $url; ?>" class="absolute-l dib f-secondary f6-nl lh4 fw-300 h-tc-link-hover td-40 featured-link"><?php echo $title; ?><span class="pl-xs pl-s-ns tc-link icon-link-arrow"></span></a>
				</div>
			<?php endif; ?>
		</div>
	</section>
</section>

<?php get_footer(); ?>