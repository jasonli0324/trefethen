<?php
/**
 * The default template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Highway29Creative
 */


get_header();
if  ( have_posts() ) while( have_posts() ) the_post(); 

$is_page = true;

?>

<?php FLEX::style(); ?>


<div class="bg-white page-<?php echo $post->post_name; ?>">
	<?php if(!is_front_page()) : ?>
		<div class="mxw-1952 mxw-1162-ds wrapper-small mh-auto mb-xl ph-m">
			<?php custom_breadcrumbs(); ?>
		</div>
	<?php endif; ?>
	<?php FLEX::blocks(); ?>
</div>

<?php 
	$has_email_signup = get_field('has_email_signup');
	if ( $has_email_signup ) {
		FLEX::part('email_signup'); 
	}
?>

<?php FLEX::script(); ?>


<script>
	function page_init() {}
</script>

<?php
//get_sidebar();
get_footer();
?>