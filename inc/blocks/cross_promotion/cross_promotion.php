<?php
  $block_class_identifier = 'cross-promotion';

  /******** BLOCK SPECIFIC **********/
  /***********************************************************/

  // do amazing things!

  $promotion_copy = get_sub_field('cross_promotion_copy');
  $promotion_image = get_sub_field('cross_promotion_image');
  $promotion_full_width = get_sub_field('cross_promotion_full_width');
  $promotion_link = get_sub_field('cross_promotion_link');
  $promotion_link_copy = $promotion_link['title'];
  $promotion_link_url = $promotion_link['url'];
  $is_single_wine = true;

  /******** CONTENT RENDER **********/
  /***********************************************************/
?>

<?php if($promotion_full_width) : ?>
  </div>
<?php endif; ?>
  
  <div class="content-block <?php echo $block_class_identifier; ?>-container">
    <?php include( locate_template( 'inc/partials/cross_promotion/cross_promotion.php', false, false ) ); ?>
  </div>

<?php if($promotion_full_width) : ?>
  <div class="mxw-1952 mh-auto ph-m">
<?php endif; ?>