<?php

$block_class_identifier = 'rotating-promo';

/******** BLOCK SPECIFIC **********/
/***********************************************************/

// do amazing things!
$show_default = get_sub_field('show_default');

/******** CONTENT RENDER **********/
/***********************************************************/
?>

<section class="relative mxw-1952 mh-auto ph-m content-block <?php echo $block_class_identifier; ?>">
  <?php if($show_default) : ?>

    <?php if( have_rows('promo_slideshow', 'option') ) : ?>
      <ul class="js-rotating-promo <?php echo $block_class_identifier; ?>-content">
        <?php 
          while ( have_rows('promo_slideshow', 'option') ) : the_row(); 
            $image = get_sub_field('promo_slideshow_image', 'option');
            $title = get_sub_field('promo_slideshow_title', 'option');
            $link = get_sub_field('promo_slideshow_link', 'option');
            $link_title = $link ? $link['title'] : '';
            $link_url = $link ? $link['url'] : '#';
            $label = get_sub_field('promo_slideshow_label', 'option');
            $promo_slideshow_category = get_sub_field('promo_slideshow_category', 'option');

            include( locate_template( 'inc/partials/promo_slide/promo_slide.php', false, false ) );
          
          endwhile; 
        ?>
      </ul>
    <?php endif; ?>
    <?php $slides = count(get_field('promo_slideshow', 'option')); ?>

  <?php else : ?>

    <?php if( have_rows('promo_slideshow') ) : ?>
      <ul class="js-rotating-promo <?php echo $block_class_identifier; ?>-content">
        <?php 
          while ( have_rows('promo_slideshow') ) : the_row(); 
            $image = get_sub_field('promo_slideshow_image');
            $title = get_sub_field('promo_slideshow_title');
            $link = get_sub_field('promo_slideshow_link');
            $link_title = $link ? $link['title'] : '';
            $link_url = $link ? $link['url'] : '#';
            $label = get_sub_field('promo_slideshow_label');
            $promo_slideshow_category = get_sub_field('promo_slideshow_category');

            include( locate_template( 'inc/partials/promo_slide/promo_slide.php', false, false ) );
          
          endwhile; 
        ?>
      </ul>
    <?php endif; ?>
    <?php $slides = count(get_sub_field('promo_slideshow')); ?>

  <?php endif; ?>
  <?php
    $nav_horizontal = false;
    include( locate_template( 'inc/partials/slider_nav/slider_nav.php', false, false ) );
  ?>
</section>