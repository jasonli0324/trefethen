<?php

$block_class_identifier = 'video-banner';

/******** BLOCK SPECIFIC **********/
/***********************************************************/

// do amazing things!
$videoUrl = get_sub_field('video');

if($videoUrl) {
	preg_match('/src="(.+?)"/', $videoUrl, $matches);
	$src = $matches[1];
	
	if(strpos($src, "youtube") !== false):
		$type = "youtube";
		$youtubeId = extractYouTubeVideoID($videoUrl);
		$thumbnail =  getYouTubeThumbnailImage($youtubeId);
	elseif(strpos($src, "vimeo") !== false):
		$type = "vimeo";
		$vimeoId = getVimeoId($videoUrl);
		$thumbnail = getVimeoThumb($vimeoId);
	else:
		$type = "other";
		$thumbnail = get_sub_field('video_banner_cover_image');
	endif;
} else {
	$thumbnail = get_sub_field('video_banner_cover_image');
}
// https://img.youtube.com/vi/<insert-youtube-video-id-here>/hqdefault.jpg

$banner = get_sub_field('video_banner');
$banner_icon = $banner['video_banner_icon'];
$banner_text = $banner['video_banner_text'];
/******** CONTENT RENDER **********/
/***********************************************************/
?>


<section class="relative mxw-1952 mh-auto ph-m <?php echo $block_class_identifier; ?>">
  <div class="relative bg-cover bg-center of-hidden <?php echo $block_class_identifier; ?>__video-container" style="background-image: url(<?php echo $thumbnail; ?>)">
		<?php if($videoUrl) : ?>
			<?php if($type == 'youtube') : ?>
				<?php                    
					$youtubeId = extractYouTubeVideoID($videoUrl);
					$thumbnail =  getYouTubeThumbnailImage($youtubeId);
					// echo "<img src='$thumbnail' />";
				?>
				<iframe class="absolute center-elem-v w-p-100" src="<?php echo $src?>&modestbranding=1&autohide=1&controls=0&rel=0&autoplay=1&loop=1&enablejsapi=1&disablekb=1&playlist=<?php echo $youtubeId; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
			<?php elseif($type == 'vimeo'): ?>
				<?php                    
					$vimeoId = getVimeoId($videoUrl);
					$thumbnail = getVimeoThumb($vimeoId);
					// echo "<img class='thumbnail-background' src='$thumbnail' />";
				?>
				<iframe class="absolute center-elem-v w-p-100" src="<?php echo $src?>&modestbranding=1&autohide=1&controls=0&autoplay=1&showinfo=0&loop=1&enablejs=1&muted=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
			<?php else: ?>
				<iframe class="absolute center-elem-v w-p-100" src="<?php echo $src?>&modestbranding=1&autohide=1&controls=0&rel=0&autoplay=1&loop=1&muted=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
			<?php endif; ?>
		<?php endif; ?>
	</div>
	<?php 
		if (!empty($banner_text)):
	?>
		<div class="absolute-l bottom-0-l center-elem-h-l flex-l items-center pv-l pv-xl-l ph-xxl-l tc-primary-text-60 bg-white ta-center-nl <?php echo $block_class_identifier; ?>__banner">
			<img src="<?php echo $banner_icon;?>" class="mr-m-l mb-s-nl <?php echo $block_class_identifier; ?>__banner-icon">
			<div class="mh-auto-nl <?php echo $block_class_identifier; ?>__banner-text"><?php echo $banner_text;?></div>
		</div>
	<?php endif;?>
</section>

