<?php

	$block_class_identifier = 'shipment-schedule';

	/******** BLOCK SPECIFIC **********/
	/***********************************************************/

	// do amazing things!

	$clubs = get_sub_field('shipment_schedule_clubs');

	/******** CONTENT RENDER **********/
	/***********************************************************/
	if (count($clubs) > 0 ) :
		$dates_arr = array();
		$k = 0;
		foreach($clubs as $clubID) {
			$club_schedules = get_field('shipment_schedule', $clubID);
			$club_schedule = $club_schedules['shipment_schedule_dates'];
			$count = isset($club_schedule) && $club_schedule ? count($club_schedule) : 0;
			array_push($dates_arr, $count);
			$k = max($dates_arr);
		}
?>

	<section class="mxw-1952 mxw-1162-ds wrapper-small mh-auto ph-m content-block <?php echo $block_class_identifier; ?>">
		<table class="dn-s w-p-100 mb-xxl f-secondary f6-m">
			<thead>
				<tr>
					<?php 
					foreach($clubs as $clubID) :
						$clube_name = get_the_title($clubID);
					?>
						<td><p class="uppercase ls-medium"><?php echo $clube_name; ?></p></td>
					<?php endforeach; ?>
				</tr>
			</thead>
			<tbody>
				<?php for ($i = 0; $i < $k; $i++) : ?>
					<tr>
						<?php 
						foreach($clubs as $clubID) :
							$club_schedules = get_field('shipment_schedule', $clubID);
							$club_schedule = $club_schedules['shipment_schedule_dates'];
							$row_dates = $club_schedule[$i];
							$date_title = $row_dates['shipment_schedule_date'];
							$date_wines = $row_dates['shipment_schedule_wines'];
						?>
							<td valign="top" class="pr-l">
								<?php if(isset($date_title)) : ?>
									<p><?php echo $date_title; ?></p>
									<?php if($date_wines) : ?>
										<?php foreach($date_wines as $wine) : ?>
											<?php 
												$wine_sku = get_field('sku', $wine->ID);
												if($wine_sku) :
											?>
												<a class="db fw-300 tc-link" href="<?php  echo get_permalink($wine->ID); ?>"><?php echo $wine->post_title; ?></a>
											<?php else : ?>
												<span class="db fw-300 tc-link"><?php echo $wine->post_title; ?></span>
											<?php endif; ?>
										<?php endforeach; ?>
									<?php endif; ?>
								<?php endif; ?>
							</td>
						<?php endforeach; ?>
					</tr>
				<?php endfor; ?>
				<tr>
					<?php 
						foreach($clubs as $clubID) :
							$club_schedules = get_field('shipment_schedule', $clubID);
							$schedule_notes = $club_schedules['shipment_schedule_notes'];
					?>
						<td class="pr-l f7 lh3"><?php echo $schedule_notes; ?></td>
					<?php endforeach; ?>
				</tr>
			</tbody>
		</table>
		<section class="dn-ns w-p-100 mb-xxl f-secondary f6-m">
			<?php
				foreach($clubs as $clubID) :
					$clube_name = get_the_title($clubID);
					$club_schedules = get_field('shipment_schedule', $clubID);
					$club_schedule = $club_schedules['shipment_schedule_dates'];
					$schedule_notes = $club_schedules['shipment_schedule_notes'];
			?>
				<article class="mb-xl f6">
					<button class="js-schedule-btn uppercase ls-medium lh3 tc-primary-text"><span class="relative dib va-middle icon-plus"></span><?php echo $clube_name; ?></button>
					<ul class="js-schedule-list dn mt-m">
						<?php if(isset($club_schedule) && count($club_schedule) > 0 ) : ?>
							<?php foreach($club_schedule as $schedule) : ?>
								<?php	
									$date_title = $schedule['shipment_schedule_date'];
									$date_wines = $schedule['shipment_schedule_wines'];
								?>
								<li class="mb-m pl-m lh4">
									<p><?php echo $date_title; ?></p>
									<?php foreach($date_wines as $wine) : ?>
										<a class="db fw-300 tc-link" href="<?php echo get_permalink($wine->ID); ?>"><?php echo $wine->post_title; ?></a>
									<?php endforeach; ?>
								</li>
							<?php endforeach; ?>
						<?php endif; ?>
						<li><p class="pl-m f7 lh3"><?php echo $schedule_notes; ?></p></li>
					</ul>
				</article>
			<?php endforeach; ?>
		</section>
		<p class="f-secondary f7-s f6-m fw-300 lh4 ls-tinier">Wines are subject to change without notice. Club price excludes sales tax and shipping.</p>
	</section>

<?php endif;?>