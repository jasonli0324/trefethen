<?php

$block_class_identifier = 'people-section';

/******** BLOCK SPECIFIC **********/
/***********************************************************/

// do amazing things!

/******** CONTENT RENDER **********/
/***********************************************************/
  $title = get_sub_field('people_block_section_title');
?>

<section class="content-block <?php echo $block_class_identifier; ?>">
  <div class="mxw-1078-ds mxw-1392-dl mh-auto ph-m">
    <?php if($title) : ?>
      <h3 class="mb-xl f-title h2 fw-300"><?php echo $title; ?></h3>
    <?php endif; ?>
    <?php if( have_rows('person') ) : ?>
      <ul class="flex flex-wrap justify-between-s justify-around-m <?php echo $block_class_identifier; ?>__list">
        <?php while ( have_rows('person') ) : the_row(); ?>
          <?php
            $name = get_sub_field('people_block_name');
            $title = get_sub_field('people_block_title');
            $photo = get_sub_field('people_block_photo');
            $bio = get_sub_field('people_block_bio');
            $headshot = get_sub_field('people_block_headshot');
          ?>
          <li class="mb-xxl mb-xxxl-l <?php echo $block_class_identifier; ?>__list-item">
            <?php if($photo) : ?>
              <div class="mb-m mb-l-l bd-round bg-center bg-cover <?php echo $block_class_identifier; ?>__pic" style="background-image: url('<?php echo $photo; ?>')"></div>
            <?php endif; ?>
            <?php if($name) : ?>
              <p class="h5"><?php echo $name; ?></p>
            <?php endif; ?>
            <?php if($title) : ?>
              <p class="f-secondary f6-s fw-300 lh4"><?php echo $title; ?></p>
            <?php endif; ?>
            <div class="flex f6-s f-secondary lh4 tc-link">
              <?php if($bio) : ?>
                <a class="h-tc-link-hover td-40" href="<?php echo $bio; ?>" target="blank">Bio</a>
              <?php endif; ?>
              <?php if($headshot) : ?>
                <span class="ph-s-s ph-m">|</span>
                <a class="h-tc-link-hover td-40" href="<?php echo $headshot; ?>" target="blank">Headshot</a>
              <?php endif; ?>
            </div>
          </li>
        <?php endwhile; ?>
      </ul>
    <?php endif; ?>
  </div>
</section>