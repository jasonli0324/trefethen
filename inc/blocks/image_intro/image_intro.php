<?php

  $block_class_identifier = 'image-intro';

  /******** BLOCK SPECIFIC **********/
  /***********************************************************/

  // do amazing things!
  $image = get_sub_field('image_intro_cover');
	$imageUrl = $image['url'];
  $imageSubtext = get_sub_field('cover_image_subtext');
  $text = get_sub_field('image_intro_text');
  $image_height = get_sub_field('image_intro_height');

  /******** CONTENT RENDER **********/
  /***********************************************************/
?>

<section class="top-content-block <?php echo $block_class_identifier; ?>">
  <div class="mxw-1952 mh-auto <?php if($text) { echo 'mb-l mb-xl-l'; } ?> ph-m">
    <?php if($imageUrl) : ?>
      <?php if($image_height === 'full') : ?>
        <img src="<?php echo $imageUrl; ?>" class="w-p-100" alt="">
      <?php else : ?>
        <div class="h-p-100 <?php echo $imageSubtext ? 'mb-s-s mb-m' : '' ?> bg-center bg-cover <?php echo $image_height === 'tall' ? $block_class_identifier . '__cover-tall' : $block_class_identifier . '__cover-short'; ?>" style="background-image: url('<?php echo $imageUrl; ?>');"></div>
      <?php endif; ?>
    <?php endif; ?>
		<?php if($imageSubtext) : ?>
      <div class="mxw-1952 mxw-1162-ds wrapper-small mh-auto ph-m-l">
        <p class="pl-s-s f-secondary f7 tc-brown"><?php echo $imageSubtext; ?></p>
      </div>
    <?php endif; ?>
  </div>
  <?php if($text) : ?>
    <div class="mxw-1952 mh-auto ph-m">
      <div class="mxw-670 mh-auto f-secondary f6-s fw-300 lh4 ls-tinier <?php echo $block_class_identifier; ?>__text"><?php echo $text; ?></div>
    </div>
  <?php endif; ?>
</section>
