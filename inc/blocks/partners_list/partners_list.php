<?php

$block_class_identifier = 'partners-list';

/******** BLOCK SPECIFIC **********/
/***********************************************************/

// do amazing things!

/******** CONTENT RENDER **********/
/***********************************************************/
$k = 0;
?>

<section class="content-block <?php echo $block_class_identifier; ?>">
  <div class="mxw-1952 mxw-1162-ds wrapper-small mh-auto ph-m">
    <?php if( have_rows('partners') ) : ?>
      <ul class="<?php echo $block_class_identifier; ?>__list">
        <?php while ( have_rows('partners') ) : the_row(); ?>
          <?php
            $image = get_sub_field('partner_image');
            $details = get_sub_field('partner_details');
            $name = $details['partner_name'];
            $description = $details['partner_description'];
            $address = $details['partner_address'];
            $phone = $details['partner_phone_number'];
            $link = $details['partner_link'];
            $email = $details['partner_email'];
          ?>
          <li class="flex-ns <?php echo $k === 0 ? 'pb-l pb-xl-ns' : 'pv-l pv-xl-ns'; ?> bdb-1 <?php echo $block_class_identifier; ?>__item">
            <div class="shrink-0 mb-l-s bg-cover bg-center <?php echo $block_class_identifier; ?>__image" style="background-image: url('<?php echo $image; ?>')"></div>
            <div class="pl-l-ns">
              <h2 class="mb-m h5"><?php echo $name; ?></h2>
              <div class="flex mb-m">
                <?php if ($address) : ?>
                  <a href="https://maps.google.com/?q=term<?php echo $address; ?>" target="blank" class="db mr-m tc-link icon-location <?php echo $block_class_identifier; ?>__icon"></a>
                <?php endif; ?>
                <?php if ($phone) : ?>
                  <a href="tel:<?php echo $phone; ?>" class="db mr-m tc-link icon-call <?php echo $block_class_identifier; ?>__icon"></a>
                <?php endif; ?>
                <?php if ($email) : ?>
                  <a href="<?php echo $email; ?>" target="blank" class="db mr-m tc-link icon-email <?php echo $block_class_identifier; ?>__icon"></a>
                <?php endif; ?>
                <?php if ($link) : ?>
                  <a href="<?php echo $link; ?>" target="blank" class="db mr-m tc-link icon-link <?php echo $block_class_identifier; ?>__icon"></a>
                <?php endif; ?>
              </div>
              <div class="f-secondary f6-s fw-300 lh4 ls-tinier <?php echo $block_class_identifier; ?>__description"><?php echo $description; ?></div>
            </div>
          </li>
        <?php $k++; endwhile; ?>
      </ul>
    <?php endif; ?>
  </div>
</section>