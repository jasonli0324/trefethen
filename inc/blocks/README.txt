
BLOCKS GENERAL INFORMATION
--------------------------------------------------------------------
Created by: Highway 29 Creative




INTRODUCTION
----------------------------------

'Blocks' or 'Content Blocks' are various reusable components utilized in the process of building out a WP page.  Thinking of them as lego blocks that can be stacked together in a variety of ways is a pretty good illustration.

Blocks are intended to be extremely flexible! In addition to content field inputs, they should contain a number of setting options to vary the layout, colors, or other settings used for block render.  Some settings are standard and should be part of all content blocks.  These standard settings are already built in to the ACF Group template import file (more on that later).

By default, blocks can be added to a page only when using the 'Flexible Layout' page template through inclusion of the 'Flexible Page Layout' ACF group.  This group includes banner settings as well as the ability to add content blocks and custom javascript/css.  

The content type that can be used with content blocks can be expanded by associating the 'Flexible Page Layout' (with page banner settings) or the 'Flexible Content' (just blocks and custom javascript/css) ACF Group to other pages/post formats.  Note that if doing so, it is neccesary to update the various page template or single file to include the needed FLEX class method calls.  See the /page-flex.php file to see how the scripts, css, banner, and blocks are added.  




STRUCTURE
----------------------------------

A 'block' is structured as below, though variations are possible based on your needs.

/ block_name

	block_name.php
	block_name-setup.php
	_block_name.scss
	README.txt

	/ to_be_moved

		group_XXXXXXXXXXXXX.json
		block_name.js

	/ preview

	/ assets


block_name.php
--> This is the insertion script that utilizes needed ACF fields and generates the front end code.  This file uses a number of static methods defined in the BLOCK class for common needs.

block_name-setup.php
--> This file contains code that needs to be ran at setup, such as registering custom post types, etc.  The core functions.php file will include this file on setup.

_block_name.scss
--> This SASS file will contain the block specific styling.  It will be included in the main style.scss file as documented in the INSTALLATION section below.

README.txt
--> This document contains specific information about the purpose of the block and any needed unique installation or setup steps.

/ to_be_moved
--> This directory contains content that needs to be relocated on installation (more below) such as ACF groups and block specific javascript that needs ran globally.

/ preview
--> This directory contains a number of images used to demonstrate possible configurations of a particular block.  The images are automatically inserted into the 'Block General Design' field under the 'Block Description' tab that is accessable in the WP Admin after the block has been added to a page.  These images are useful for website administrators to have a better sense of the purpose and general look of a content block.  

/ assets
--> This directory contains any JS, images, or other files that will be referenced from the block build IN PLACE through script enqueueing or other means.



INSTALLATION
----------------------------------

When installing a new block you'll have to follow the below general steps, though certain blocks may contain additional steps as would be documented in the block specific README.txt file.

1.  Copy block directory into /inc/blocks/ do not change the name, it needs remain as defined.
2.  Update /assets/styles/scss/style.scss in the block import section of that file to import the new block scss file in place (example below)

	@import "../../../inc/blocks/standard_text/standard_text";

3.  Compile SASS
4. If there is globally loaded JavaScript move it to the pre or post init directories in /assets/javascript/src
5.  Copy group_XXXXXXXXXX.json from the 'to_be_moved' directory into /acf-theme-json.  NOTE that the block might have additional ACF group files if custom post types are also defined as part of the block.
6.  Edit ACF Group 'Flexible Content' and open field 'Content Blocks to Appear on Page'
7.  Add new Layout option and give it a label and name.  The 'label' is what the client will see in the admin interface, the 'name' NEEDS to be the same as the block directory name.
8.  Add a field in this new layout with type Clone and select the 'BLOCK - the_blocks_name' all fields option.  Label and name don't matter much here (just don't conflict).
9.  Save 'Flexible Content' ACF Form.

That's all folks!


