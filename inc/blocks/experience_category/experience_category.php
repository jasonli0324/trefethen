<?php

  /******** BLOCK SPECIFIC **********/
  /***********************************************************/

  $block_class_identifier = 'experience-category';

  // do amazing things!
  $category = get_sub_field('category_selected');
  $post_args = array(
    'post_type' => 'experience',
    'posts_per_page' => -1,
    'tax_query' => [
      [
        'taxonomy' => 'experience_categories',
        'terms' => $category,
      ],
    ],
  );
  $experiences = new WP_Query($post_args);

  /******** CONTENT RENDER **********/
  /***********************************************************/
?>

<section class="relative">
  <?php 
    while($experiences->have_posts()) : $experiences->the_post();
      $permalink = get_permalink();
      $photos = get_field('visit_experience_photos');
      $image = $photos['visit_experience_main_photo']['url'];
      $title = get_field('visit_experience_title');
      $description = get_field('visit_experience_short_description', false);
      $price = get_field('visit_experience_cost_summary');
      $price_details = get_field('visit_experience_payment_details');
      $winedirect_tock = get_field('visit_experience_use_winedirect_tock');
      $reservation_link = $winedirect_tock ? get_field('reservation_link', 'options') : get_field('visit_experience_reservation_link');
      $content_blocks = get_field('additional_content_blocks') && get_field('additional_content_blocks')['content_blocks'];
      $cross_promo = get_field('cross_promotion')['cross_promotion'];
      $block_class_identifier = 'experience-category';
  ?>
      <article class="f-secondary content-block <?php echo $block_class_identifier; ?>">
        <div class="mxw-1952 wrapper-small mh-auto mb-l mb-xl-l ph-m">
          <div style="background-image: url('<?php echo $image; ?>')" class="w-p-100-nl bg-cover bg-center <?php echo $block_class_identifier; ?>__image"></div>
        </div>
        <div class="mxw-1952 mxw-1162-ds wrapper-small mh-auto ph-m">
          <div class="mxw-670">
            <h2 class="mb-m mb-xl-l f-title h2"><?php echo $title; ?></h2>
            <div class="flex-nl flex-column-reverse-nl">
              <div class="flex-l mb-xl-l">
                <?php 
                  $reservation_link_type = $reservation_link['visit_experience_reservation_link_type'];
                  if ($reservation_link_type === 'email') {
                    $reservation_url = 'mailto:' . $reservation_link['experience_reservation_email'];
                  } elseif ($reservation_link_type === 'phone') {
                    $reservation_url = 'tel:' . $reservation_link['experience_reservation_phone'];
                  } elseif ($reservation_link_type === 'url') {
                    $reservation_url = $reservation_link['experience_reservation_url'];
                  } else {
                    $reservation_url = '#';
                  }
                ?>
                <a href="<?php echo $reservation_url; ?>" class="db w-p-100 mb-m-nl mr-xs-l pv-s ph-m bdr-3 tc-white ta-center bg-link h-bg-link td-40 <?php echo $block_class_identifier; ?>__btn">Reserve Now</a>
                <div class="<?php echo $cross_promo ? 'mb-xxl-nl' : ''; ?> pl-m-l">
                  <p class="f6-s lh4 ls-tinier"><?php echo $price; ?></p>
                  <p class="f7"><?php echo $price_details; ?></p>
                </div>
              </div>
              <div class="<?php echo $cross_promo ? 'mb-m mb-xxxl-l pb-m-l' : 'mb-m-nl'; ?> f6-s fw-300 lh4"><?php echo $description; ?></div>
            </div>
          </div>
        </div>

        <?php 
          if ($cross_promo['cross_promotion_copy']) {
            $block_class_identifier = 'cross-promotion';
            $promotion_copy = $cross_promo['cross_promotion_copy'];
            $promotion_image = $cross_promo['cross_promotion_image'];
            $link = $cross_promo['cross_promotion_link'];
            $promotion_link_url = $link ? $link['url'] : '';
            $promotion_link_copy = $link ? $link['title'] : '';
            $promotion_full_width = $cross_promo['cross_promotion_full_width'];	

            include( locate_template( 'inc/partials/cross_promotion/cross_promotion.php', false, false ) );
          }
        ?>
      </article>

  <?php endwhile; wp_reset_postdata();?>
</section>