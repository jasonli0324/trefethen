<?php

  $block_class_identifier = 'wine-category';

  /******** BLOCK SPECIFIC **********/
  /***********************************************************/

  // do amazing things!

  $categories = get_sub_field('wine_category');

  if($categories) :

    $prices = array();
    $filter_cats_arr = array();
    $filter_bottle_cats_arr = array();
    $post_args = array(
      'suppress_filters' => FALSE,
      'numberposts'	=> -1,
      'post_type'		=> 'wine',
      'meta_query' => array(
        'relation' => "OR",
      )
    );

    foreach($categories as $category) {
      if($category->post_title === "Bottle" || $category->post_title === "Gifting" || $category->post_title === "library") {
        $cat_name = $category->post_title === "Bottle" ? 'standard' : ($category->post_title === "Gifting" ? 'featured' : $category->post_title);
        $post_subargs = array(
          'key' => 'wine_card_type',
          'compare' => 'LIKE',
          'value' => $cat_name
        );
        array_push($filter_bottle_cats_arr, $category->post_name);
      } else {
        $post_subargs = array(
          'key' => array('type_varietal_wine_type', 'type_varietal_champagne_varietals', 'type_varietal_dessert_varietals', 'type_varietal_ice_varietals', 'type_varietal_red_varietals', 'type_varietal_rose_varietals', 'type_varietal_sparkling_varietals', 'type_varietal_white_varietals'),
          'compare' => 'LIKE',
          'value' => $category->post_title
        );
        array_push($filter_cats_arr, $category->post_title);
      }
      array_push($post_args['meta_query'], $post_subargs);
    }
    $filter_cats = implode('-', $filter_cats_arr);
    $filter_bottle_cats = implode('-', $filter_bottle_cats_arr);

    
    $wines = new WP_Query($post_args);
    
    if($wines->have_posts()) {
      while($wines->have_posts()) {
        $wines->the_post();
        $price = get_field('price');
        if ($price !== 0) array_push($prices, $price);
      } 
      wp_reset_postdata();
    }
    $prices = array_unique($prices);
    $max = max($prices);
    $min = min($prices) ? min($prices) : 0;
    $shop_filters = array('Price');
    $index = 0;
  
  /******** CONTENT RENDER **********/
  /***********************************************************/
?>

<section class="pt-l pt-xl-l">
  <div class="mxw-1952 mxw-1162-ds wrapper-small mh-auto ph-m">
    <?php 
      if ($wines->have_posts()) :
        include( locate_template( 'inc/partials/shop_filters/shop_filters.php', false, false ) ); 
      endif;
    ?>
  </div>

	<div id="category-post-content" class="page-shop__content">
		<section class="flex justify-between flex-wrap mxw-1952 mxw-1162-ds wrapper-small mh-auto ph-m">
			<?php 
				if ($wines->have_posts()) :
          while ($wines->have_posts()) :
            $wines->the_post();
            global $post;
            $wineID = $post->ID;
            $vivino_rating = get_field('vivino_rating');
            $vivino_count = get_field('vivino_count');
            $price = get_field('price');
            $dates = get_field('dates');
            $bottle_photos = get_field('bottle_photos');
            $gift_pack_photos = get_field('gift_pack_photos');
            $badge_message = get_field('badge_message');
            $vivino_url = get_field('vivino_score');
						$wine_sku = get_field('sku');
						
						if($wine_sku) {
              include( locate_template( 'inc/partials/wine_card/wine_card.php', false, false ) );
            }
            
					$index++;
          endwhile;
          wp_reset_postdata();
        else :
          echo "<p class='h5'>Sorry! There aren't any wines here.</p>";
        endif; 
			?>
		</section>
	</div>
	<div id="category-post-content-ajax" class="flex justify-between-nl flex-wrap mxw-1952 mxw-1162-ds wrapper-small mh-auto ph-m page-shop__content"></div>
	
</section>
<?php endif; ?>