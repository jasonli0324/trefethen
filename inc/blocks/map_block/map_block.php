<?php

$block_class_identifier = 'map-block';

/******** BLOCK SPECIFIC **********/
/***********************************************************/

// do amazing things!
$address = get_sub_field('map_address');

/******** CONTENT RENDER **********/
/***********************************************************/
?>


<iframe class="w-p-100 content-block <?php echo $block_class_identifier; ?>" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=<?php echo $address; ?>&key=AIzaSyCg6-rCMnCEAwyLLwiQoRQWTYFwjn2tn3k&zoom=12" allowfullscreen></iframe>
