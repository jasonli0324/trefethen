<?php

$block_class_identifier = 'text-image-slider';

/******** BLOCK SPECIFIC **********/
/***********************************************************/

// do amazing things!
$slider_title = get_sub_field('slider_title');

/******** CONTENT RENDER **********/
/***********************************************************/
?>
<section class="relative content-block <?php echo $block_class_identifier; ?>">
  <div class="relative mxw-1952 mxw-1162-ds wrapper-small mh-auto ph-m">
    <?php if( have_rows('text_image_slider_slides') ) : ?>
      <?php if($slider_title) : ?>
        <h2 class="absolute-ns z-1 mb-l bg-white top-0 f-title h2 ta-center-s <?php echo $block_class_identifier; ?>__title"><?php echo $slider_title; ?></h2>
      <?php endif; ?>
      <ul class="js-text-image-slider relative">
        <?php 
          while ( have_rows('text_image_slider_slides') ) : the_row(); 
            $slide_copy = get_sub_field('slide_copy');
            $slide_image = get_sub_field('slide_image');
            $slide_text = $slide_copy ? $slide_copy['slide_text'] : '';
            $slide_text_image = $slide_copy ? $slide_copy['slide_text_image'] : '';
            $slide_text_image_url = $slide_text_image ? $slide_copy['slide_text_image']['url'] : '';
            $slide_text_image_subtext = $slide_copy ? $slide_copy['slide_text_image_subtext'] : '';
        ?>
          <li>
            <div class="flex flex-column-reverse-s items-center">
              <div class="mr-xl-m mr-xxxl-l <?php echo $block_class_identifier; ?>__copy-content">
                <?php if($slide_text) : ?>
                  <div class="mb-l f6-nl f-secondary ta-center-s <?php echo $block_class_identifier; ?>__text"><?php echo $slide_text; ?></div>
                <?php endif; ?>
                <div class="ta-center-s">
                  <?php if($slide_text_image_url) : ?>
                    <img class="mh-auto-s mb-s <?php echo $block_class_identifier; ?>__text-image" src="<?php echo $slide_text_image_url; ?>" alt="<?php echo $slide_text_image_subtext; ?>">
                  <?php endif; ?>
                  <?php if($slide_text_image_subtext) : ?>
                    <p class="f-secondary f7 "><?php echo $slide_text_image_subtext; ?></p>
                  <?php endif; ?>
                </div>
              </div>
              <?php if($slide_image) : ?>
                <div class="mb-xxl-s bg-center bg-cover <?php echo $block_class_identifier; ?>__image" style="background-image: url('<?php echo $slide_image; ?>')"></div>
              <?php endif; ?>
            </div>
          </li>
        <?php endwhile; ?>
      </ul>
      <?php 
        $slides = count(get_sub_field('text_image_slider_slides'));
        $nav_horizontal = false;
        include( locate_template( 'inc/partials/slider_nav/slider_nav.php', false, false ) );
      ?>
    <?php endif; ?>

    <?php if( have_rows('text_image_slider_featured_links') ) : ?>
      <div class="flex justify-center justify-end-l mt-l-s pv-l">
        <ul class="w-p-100 flex justify-between <?php echo $block_class_identifier; ?>__links">
          <?php 
            while ( have_rows('text_image_slider_featured_links') ) : the_row(); 
            $icon = get_sub_field('link_icon');
            $link = get_sub_field('text_image_slider_featured_link');
            $link_title = $link['title'];
            $link_url = $link['url'];
          ?>
            <li><a href="<?php echo $link_url; ?>" class="f3 f2-l f-italic ta-center-s h-tc-link td-40"><span class="db-s mb-s-s mr-xs f2 va-middle icon-<?php echo $icon; ?>"></span><?php echo $link_title; ?></a></li>
          <?php endwhile; ?>
        </ul>
      </div>
    <?php endif; ?>
  </div>
</section>