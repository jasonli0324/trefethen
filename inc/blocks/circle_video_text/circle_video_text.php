<?php

  $block_class_identifier = 'circle-video-text';

  /******** BLOCK SPECIFIC **********/
  /***********************************************************/

  // do amazing things!
  $bg_image = get_sub_field('circle_video_text_bg_image');
  $field_content = get_sub_field('circle_video_text_content');
  $videoUrl = $field_content['circle_video_text_video'];
  $content = $field_content['text_content'];
  $text = $content['circle_video_content_text'];
  
  $image = $content ? $content['text_bottom_image'] : '';
  $image_url = $image ? $image['url'] : '';
  $image_subtext = $content ? $content['text_bottom_image_subtext'] : '';

  preg_match('/src="(.+?)"/', $videoUrl, $matches);
  $src = $matches[1];

  if(strpos($src, "youtube") !== false):
    $type = "youtube";
  elseif(strpos($src, "vimeo") !== false):
    $type = "vimeo";
  else:
    $type = "other";
  endif;

  /******** CONTENT RENDER **********/
  /***********************************************************/
?>

<section class="content-block <?php echo $block_class_identifier; ?>" style="background-image: url('<?php echo $bg_image; ?>')">
  <div class="flex flex-column-reverse-s justify-around-xl items-center mxw-1952 mxw-1162-ds wrapper-small mh-auto ph-m">
    <div class="f-secondary ta-center-s text-block <?php echo $block_class_identifier; ?>__copy">
      <div class="mb-l"><?php echo $text; ?></div>
      <div class="ta-center-s">
        <?php if($image_url) : ?>
          <img class="mh-auto-s mb-s" src="<?php echo $image_url; ?>" alt="<?php echo $image_subtext; ?>">
        <?php endif; ?>
        <?php if($image_subtext) : ?>
          <p class="f-secondary f7 "><?php echo $image_subtext; ?></p>
        <?php endif; ?>
      </div>
    </div>
    <div class="relative shrink-0 mb-l-s bd-round of-hidden <?php echo $block_class_identifier; ?>__video-container">
      <?php if($type == 'youtube') :?>
        <?php                    
          $youtubeId = extractYouTubeVideoID($videoUrl);
          $thumbnail =  getYouTubeThumbnailImage($youtubeId);
        ?>
        <iframe class="absolute center-elem-h h-p-100 <?php echo $block_class_identifier; ?>__video" src="<?php echo $src?>&modestbranding=1&autohide=1&controls=0&rel=0&autoplay=1&loop=1&enablejsapi=1&disablekb=1&playlist=<?php echo $youtubeId; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
      <?php elseif($type == 'vimeo'): ?>
        <?php                    
          $vimeoId = getVimeoId($videoUrl);
          $thumbnail = getVimeoThumb($vimeoId);
        ?>
        <iframe class="absolute center-elem-h h-p-100 <?php echo $block_class_identifier; ?>__video" src="<?php echo $src?>&modestbranding=1&autohide=1&controls=0&autoplay=1&showinfo=0&loop=1&enablejs=1&muted=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
      <?php else: ?>
        <iframe class="absolute center-elem-h h-p-100 <?php echo $block_class_identifier; ?>__video" src="<?php echo $src?>&modestbranding=1&autohide=1&controls=0&rel=0&autoplay=1&loop=1&muted=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
      <?php endif; ?>
    </div>
  </div>
</section>