<?php

$block_class_identifier = 'quote-block';

/******** BLOCK SPECIFIC **********/
/***********************************************************/

// do amazing things!
$quote = get_sub_field('quote');
$source = get_sub_field('source');

/******** CONTENT RENDER **********/
/***********************************************************/
?>

<section class="mxw-670 mh-auto ph-m-s ta-center content-block <?php echo $block_class_identifier; ?>">
  <p class="mb-s f-secondary f7"><?php echo $source; ?></p>
  <p class="f-title quote fw-300 f-italic lh3 tc-brown"><?php echo $quote; ?></p>
</section>