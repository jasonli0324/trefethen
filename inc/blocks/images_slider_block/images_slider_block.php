<?php

  $block_class_identifier = 'images-slider';

  /******** BLOCK SPECIFIC **********/
  /***********************************************************/

  // do amazing things!

  /******** CONTENT RENDER **********/
  /***********************************************************/
?>


<section class="mh-auto content-block <?php echo $block_class_identifier; ?>">

  <?php if( have_rows('images_slider_slider') ) : ?>
    <ul class="js-images-slider relative <?php echo $block_class_identifier; ?>__list">
      <?php while ( have_rows('images_slider_slider') ) : the_row(); ?>
        <?php
          $image = get_sub_field('images_slider_slider_image');
          $image_url = $image['url'];
          $imageSubtext = get_sub_field('images_slider_slider_image_subtext');
        ?>
        <li>
          <?php if($image_url) : ?>
            <div class="mb-s bg-cover bg-center <?php echo $block_class_identifier; ?>__image" style="background-image: url('<?php echo $image_url; ?>')"></div>
          <?php endif; ?>
          <?php if($imageSubtext) : ?>
            <p class="f-secondary f7 tc-brown"><?php echo $imageSubtext; ?></p>
          <?php endif; ?>
        </li>
      <?php endwhile; ?>
    </ul>
  <?php endif; ?>

</section>