<?php

  $block_class_identifier = 'dates-list';

  /******** BLOCK SPECIFIC **********/
  /***********************************************************/

  // do amazing things!
  $section_title = get_sub_field('section_title');
  $align_title = get_sub_field('align_title');

  /******** CONTENT RENDER **********/
  /***********************************************************/
?>


<section class="mxw-1242 mh-auto content-block <?php echo $block_class_identifier; ?>">
  <?php 
    if ($section_title) {
      include( locate_template( 'inc/partials/section_title/section_title.php', false, false ) ); 
    }
    $dates = get_sub_field('dates_list_date');
    $rows_desk = [
      1 => [],
      2 => [],
      3 => [],
      4 => [],
      5 => []
    ];
    $rows_tab = [
      1 => [],
      2 => [],
      3 => []
    ];
    $rows_mobile = [
      1 => [],
      2 => []
    ];
    $desk_counter = 1;
    $tab_counter = 1;
    $mobile_counter = 1;
    
    foreach($dates as $date) {
      array_push($rows_desk[$desk_counter], $date);
      $desk_counter = $desk_counter === 5 ? 1 : $desk_counter + 1;
      
      array_push($rows_tab[$tab_counter], $date);
      $tab_counter = $tab_counter === 3 ? 1 : $tab_counter + 1;

      array_push($rows_mobile[$mobile_counter], $date);
      $mobile_counter = $mobile_counter === 2 ? 1 : $mobile_counter + 1;
    }
  ?>

  <ul class="relative z-1 mt-m flex f-secondary <?php echo $block_class_identifier; ?>__list">
    <?php foreach($rows_desk as $row_item) : ?>
      <li class="dn-nl mb-xl">
        <?php foreach($row_item as $item) : ?>
          <div class="mb-xl">
            <p class="mb-s"><?php echo $item['dates_list_year']; ?></p>
            <?php 
              $sub_items = $item['dates_list_year_items'];
              foreach($sub_items as $sub_item) :
            ?>
              <p class="tc-brown f7 lh4"><?php echo $sub_item['dates_list_year_item']; ?></p>
            <?php endforeach; ?>
          </div>
        <?php endforeach; ?>
      </li>
    <?php endforeach; ?>

    <?php foreach($rows_tab as $row_item) : ?>
      <li class="dn-s dn-l mb-xl">
        <?php foreach($row_item as $item) : ?>
          <div class="mb-xl">
            <p class="mb-s"><?php echo $item['dates_list_year']; ?></p>
            <?php 
              $sub_items = $item['dates_list_year_items'];
              foreach($sub_items as $sub_item) :
            ?>
              <p class="tc-brown f7 lh4"><?php echo $sub_item['dates_list_year_item']; ?></p>
            <?php endforeach; ?>
          </div>
        <?php endforeach; ?>
      </li>
    <?php endforeach; ?>

    <?php foreach($rows_mobile as $row_item) : ?>
      <li class="dn-ns mb-xl">
        <?php foreach($row_item as $item) : ?>
          <div class="mb-xl">
            <p class="mb-s"><?php echo $item['dates_list_year']; ?></p>
            <?php 
              $sub_items = $item['dates_list_year_items'];
              foreach($sub_items as $sub_item) :
            ?>
              <p class="tc-brown f7 lh4"><?php echo $sub_item['dates_list_year_item']; ?></p>
            <?php endforeach; ?>
          </div>
        <?php endforeach; ?>
      </li>
    <?php endforeach; ?>
  </ul>

</section>