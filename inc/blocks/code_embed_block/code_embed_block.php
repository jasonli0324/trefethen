<?php

  /******** BLOCK SPECIFIC **********/
  /***********************************************************/

  $block_class_identifier = 'code-embed';
  // do amazing things!

  /******** CONTENT RENDER **********/
  /***********************************************************/
?>

<section class="relative mxw-1952 mh-auto ph-m content-block <?php echo $block_class_identifier; ?>">
  <?php the_sub_field('code_block'); ?>
</section>