<?php

$block_class_identifier = 'video-block';

/******** BLOCK SPECIFIC **********/
/***********************************************************/

// do amazing things!
$videoUrl = get_sub_field('video');
preg_match('/src="(.+?)"/', $videoUrl, $matches);

$src = $matches[1];

if(strpos($src, "youtube") !== false):
	$type = "youtube";
elseif(strpos($src, "vimeo") !== false):
	$type = "vimeo";
else:
	$type = "other";
endif;

/******** CONTENT RENDER **********/
/***********************************************************/
?>


<section class="content-block">
  <div class="relative mh-auto <?php echo $block_class_identifier; ?>">
    <?php if($type == 'youtube') :?>
      <?php                    
        $youtubeId = extractYouTubeVideoID($videoUrl);
        $thumbnail =  getYouTubeThumbnailImage($youtubeId);
        // echo "<img src='$thumbnail' />";
      ?>
      <iframe class="absolute-nl top-0 left-0 w-p-100 h-p-100 <?php echo $block_class_identifier; ?>__video" src="<?php echo $src?>&modestbranding=0&autohide=0&controls=1&rel=0&autoplay=0&loop=1&enablejsapi=1&disablekb=1&playlist=<?php echo $youtubeId; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
    <?php elseif($type == 'vimeo'): ?>
      <?php                    
        $vimeoId = getVimeoId($videoUrl);
        $thumbnail = getVimeoThumb($vimeoId);
        // echo "<img class='thumbnail-background' src='$thumbnail' />";
      ?>
      <iframe class="absolute-nl top-0 left-0 w-p-100 h-p-100 <?php echo $block_class_identifier; ?>__video" src="<?php echo $src?>&modestbranding=0&autohide=0&controls=1&autoplay=0&showinfo=0&loop=1&enablejs=1&muted=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
    <?php else: ?>
      <iframe class="absolute-nl top-0 left-0 w-p-100 h-p-100 <?php echo $block_class_identifier; ?>__video" src="<?php echo $src?>&modestbranding=0&autohide=0&controls=1&rel=0&autoplay=0&loop=1&muted=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
    <?php endif; ?>
  </div>
</section>

