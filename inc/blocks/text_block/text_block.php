<?php

$block_class_identifier = 'text-block';

/******** BLOCK SPECIFIC **********/
/***********************************************************/

// do amazing things!
$headline = get_sub_field('text_block_header_text');
$text = get_sub_field('text_block_text');
$link = get_sub_field('text_block_bottom_link');
$align = get_sub_field('text_block_field');

if ($align === 'right' ) {
  $justify = 'justify-end';
} elseif ($align === 'center' ) {
  $justify = 'justify-center';
} else {
  $justify = 'justify-start';
}

/******** CONTENT RENDER **********/
/***********************************************************/
?>

<section class="content-block <?php echo $block_class_identifier; ?>">
  <div class="flex <?php echo $justify; ?> mxw-1952 mxw-1162-ds wrapper-small mh-auto ph-m">
    <div class="mxw-670">
      <?php if ($headline) : ?>
        <div class="mb-xl f3 f2-l tc-brown intro-text"><?php echo $headline; ?></div>
      <?php endif; ?>
      <?php if ($text) : ?>
        <div class="<?php if($link) { echo 'mb-l'; } ?> f-secondary f6-s fw-300 lh4"><?php echo $text; ?></div>
      <?php endif; ?>
      <?php if($link) : ?>
        <?php
          $title = $link['title'];
          $url = $link['url'];
        ?>
        <a href="<?php echo $url; ?>" class="relative f-secondary f6-nl fw-300 ls-tinier featured-link"><?php echo $title; ?><span class="pl-s tc-link icon-link-arrow"></span></a>
      <?php endif; ?>
    </div>
  </div>
</section>