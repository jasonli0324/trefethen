<?php

$block_class_identifier = 'featured-links';

/******** BLOCK SPECIFIC **********/
/***********************************************************/

// do amazing things!
$section_title = get_sub_field('section_title');
$align_title = get_sub_field('align_title');
$amount = count(get_sub_field('featured_link'));
$optional_link = get_sub_field('optional_link');
$is_double = $amount < 3;

/******** CONTENT RENDER **********/
/***********************************************************/
?>

<section class="content-block <?php echo $block_class_identifier; ?> <?php echo $is_double ? $block_class_identifier . '--double' : $block_class_identifier . '--triple';?>">
  <?php 
    if ($section_title) {
      include( locate_template( 'inc/partials/section_title/section_title.php', false, false ) ); 
    }
  ?>
  <div class="flex-l mxw-1952 mxw-1162-ds wrapper-small mh-auto  <?php if ($section_title) { echo 'mt-m'; }?> ph-m">
    <?php if( have_rows('featured_link') ) : ?>
      <ul class="relative z-1 flex flex-wrap w-p-100 ph-m-s <?php echo $block_class_identifier; ?>__list">
        <?php 
          while ( have_rows('featured_link') ) : 
            the_row();
            $image = get_sub_field('featured_link_image');
            $title = get_sub_field('featured_link_tilte');
            $description = get_sub_field('featured_link_description');
            $link = get_sub_field('featured_link_url');
            $link_title = $link ? $link['title'] : '';
            $link_url = $link ? $link['url'] : '#';
            
            include( locate_template( 'inc/partials/featured_link/featured_link.php', false, false ) ); 
          
          endwhile; 
        ?>
      </ul>
      <?php if ($optional_link) : ?>
        <?php 
          $title = $optional_link['title']; 
          $url = $optional_link['url']; 
        ?>
        <div class="relative dib-nl <?php echo $block_class_identifier; ?>__optional-link">
          <a href="<?php echo $url; ?>" class="absolute-l dib f-secondary f6-nl lh4 fw-300 h-tc-link-hover td-40 featured-link"><?php echo $title; ?><span class="pl-xs pl-s-ns tc-link icon-link-arrow"></span></a>
        </div>
      <?php endif; ?>
    </div>
	<?php endif; ?>
</section>