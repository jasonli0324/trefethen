<?php

  $block_class_identifier = 'image-text';

  /******** BLOCK SPECIFIC **********/
  /***********************************************************/

  // do amazing things!
  $imageUrl = get_sub_field('image_text_block_image');
  $content = get_sub_field('image_text_block_content');
	$contentText = $content['text_field'];
	$contentImage = $content['text_bottom_image'];
	$contentImageUrl = $contentImage ? $contentImage['url'] : '';
  $contentImageSubtext = $content['text_bottom_image_subtitle'];

  /******** CONTENT RENDER **********/
  /***********************************************************/
?>


<section class="mxw-1952 mxw-1162-ds wrapper-small flex-ns items-center mh-auto ph-m content-block <?php echo $block_class_identifier; ?>">
  <?php if($imageUrl) : ?>
    <div class="w-p-50-ns mb-m-s bg-center bg-cover <?php echo $block_class_identifier; ?>__image" style="background-image: url('<?php echo $imageUrl; ?>')"></div>
  <?php endif; ?>
  <div class="w-p-50-ns pl-xl-m pl-xxl-l f-secondary ta-center-s text-block <?php echo $block_class_identifier; ?>__copy">
    <?php if($contentText) : ?>
      <div class="mb-l f6-s fw-300 lh4"><?php echo $contentText; ?></div>
    <?php endif; ?>
    <?php if($contentImageUrl) : ?>
      <div class="ta-center-s">
        <img class="mh-auto-s mb-s" src="<?php echo $contentImageUrl; ?>" alt="<?php echo $contentImageSubtext; ?>">
        <?php if($contentImageSubtext) : ?>
          <p class="f-secondary f7"><?php echo $contentImageSubtext; ?></p>
        <?php endif; ?>
      </div>
    <?php endif; ?>
  </div>
</section>