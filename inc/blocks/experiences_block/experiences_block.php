<?php

$block_class_identifier = 'experience-item';

/******** BLOCK SPECIFIC **********/
/***********************************************************/

// do amazing things!

$showAll = get_sub_field('show_by_cat');

/******** CONTENT RENDER **********/
/***********************************************************/
?>

<?php if($showAll) : 
	$cats = get_terms('experience_categories');
	foreach($cats as $cat) :
		$cat_title = $cat->ID;
		$args = array(
			'posts_per_page'   => -1,
			'post_type'        => 'experience',
			'tax_query' => [
				[
					'taxonomy' => $cat->taxonomy,
					'terms' => $cat->term_id,
				],
			],
		);
		$experiences = get_posts($args);
		$posts_amount = count($experiences);
		$k = 1;
?>
		<section class="content-block <?php echo $block_class_identifier; ?>-container">
			<?php 
				$section_title = $cat->name;
				include( locate_template( 'inc/partials/section_title/section_title.php', false, false ) );
			?>
			<div class="relative mxw-1952 mxw-1162-ds wrapper-small mt-m mh-auto mb-xxxl ph-m">
				<?php 
					foreach($experiences as $experience) {
						$itemID = $experience->ID;
						$has_mb = $posts_amount > $k;
						include( locate_template( 'inc/partials/experience_card/experience_card.php', false, false ) );
					}
				?>
			</div>
		</section>
	<?php $k++; endforeach; ?>
<?php else : ?>
	<?php if( have_rows('experiences_group') ) : ?>
		<?php while ( have_rows('experiences_group') ) : the_row(); ?>
			<?php 
				$section_title = get_sub_field('experiences_block_heading'); 
				$experiences = get_sub_field('selected_experiences');
				$posts_amount = count($experiences);
				$k = 1;
			?>
			<section class="content-block <?php echo $block_class_identifier; ?>-container">
				<?php 
					if ($section_title) {
						include( locate_template( 'inc/partials/section_title/section_title.php', false, false ) );
					}
				?>
				<div class="relative mxw-1952 mxw-1162-ds wrapper-small mt-m mh-auto mb-xxxl ph-m">
					<?php 
						foreach($experiences as $experience) {
  						$itemID = $experience;
							$has_mb = $posts_amount > $k;
							include( locate_template( 'inc/partials/experience_card/experience_card.php', false, false ) );
							$k++; 
						}
					?>
				</div>
			</section>
		<?php endwhile; ?>
	<?php endif; ?>
<?php endif; ?>

