<?php

	$block_class_identifier = 'wine-clubs';

	/******** BLOCK SPECIFIC **********/
	/***********************************************************/

	// do amazing things!

	$showAll = get_sub_field('show_all_wine_clubs');
	if ($showAll) {
		$clubs = get_posts([
			'posts_per_page' => -1,
			'post_type' => 'club'
		]);
	} else {
		$postIds = get_sub_field('wine_clubs_to_show');
		$clubs = get_posts([
			'posts_per_page' => -1,
			'post_type' => 'club',
			'post__in' => $postIds,
		]);
	}
	$k = 0;
	$numb_of_clubs = count($clubs);
	// To remove 
	/******** CONTENT RENDER **********/
	/***********************************************************/
?>


<section class='relative content-block  <?php echo $block_class_identifier; ?>'>
	<?php 
		$section_title = 'Clubs';
		include( locate_template( 'inc/partials/section_title/section_title.php', false, false ) );
	?>
	<div class="relative mxw-1952 mxw-1162-ds wrapper-small mh-auto mb-xxxl ph-m">
		<?php foreach($clubs as $club) : ?>
			<?php
				$name = get_field('club_club_name', $club->ID);
				$description = get_field('club_club_description_landing', $club->ID);
				$photo = get_field('club_club_photo', $club->ID);
				$sign_up = get_field('club_club_sign_up_link', $club->ID);
				$sign_up_type = get_field('club_sign_up_type', $club->ID);
				$sign_up_links = get_field('club_sign_up_links', $club->ID);
				$sign_up_contacts = get_field('club_sign_up_contacts', $club->ID);
				if($sign_up_contacts) {
					$sign_up_email = $sign_up_contacts['club_sign_up_email'] ? $sign_up_contacts['club_sign_up_email'] : '#';
					$sign_up_phone = $sign_up_contacts['club_sign_up_phone_number'] ? $sign_up_contacts['club_sign_up_phone_number'] : '#';
				}
			?>
			<article class="flex-ns mt-m <?php echo $numb_of_clubs === $k+1 ? '' : 'mb-xxl mb-xxxl-l pb-m-l'?> f-secondary <?php echo $block_class_identifier; ?>__item">
				<div class="db h-p-100 mr-m-ns mb-l-s shrink-0 bg-cover bg-center <?php echo $block_class_identifier; ?>__image" style="background-image: url('<?php echo $photo; ?>');"></div>
				<div class="mxw-490 pl-s-ns">
					<h2 class="mb-m f-title fw-300 lh2 <?php echo $block_class_identifier; ?>__title"><?php echo $name; ?></h2>
					<p class="mb-m f6-s fw-300 lh4 ls-tinier"><?php echo $description; ?></p>
					<?php if($sign_up_type === 'contacts') : ?>
						<p class="mb-s fw-300">Request Access</p>
						<ul class="flex">
							<li class="w-p-100 <?php echo $block_class_identifier; ?>__btn">
								<a href="mailto:<?php echo $sign_up_email; ?>" class="db w-p-100 pv-s ph-m bdr-3 ta-center tc-link btn-link td-40 <?php echo $block_class_identifier; ?>__btn-link">Email</a>
							</li>
							<li class="w-p-100 <?php echo $block_class_identifier; ?>__btn">
								<a href="tel:<?php echo $sign_up_phone; ?>" class="db w-p-100 pv-s ph-m bdr-3 ta-center tc-link btn-link td-40 <?php echo $block_class_identifier; ?>__btn-link">Call</a>
							</li>
						</ul>
					<?php else : ?>
						<?php if ($sign_up_links ) : ?>
							<p class="mb-s fw-300">Choose your bottle quantity:</p>
							<ul class="flex">
								<?php foreach($sign_up_links as $option ) : ?>
									<li class="w-p-100 <?php echo $block_class_identifier; ?>__btn <?php echo $block_class_identifier; ?>__btn-radio">
										<a href="<?php echo $option['sign_up_link']; ?>" target="blank" class="db w-p-100 pv-s ph-m bdr-3 ta-center tc-link btn-link <?php echo $block_class_identifier; ?>__btn-link"><?php echo $option['bottle_quantity'];?></a>
									</li>
								<?php endforeach; ?>
							</ul>
						<?php endif;?>
					<?php endif;?>
				</div>
			</article>
		<?php $k++; endforeach; ?>
	</div>
</section>
