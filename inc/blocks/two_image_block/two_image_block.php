<?php

  $block_class_identifier = 'two-image';

  /******** BLOCK SPECIFIC **********/
  /***********************************************************/

  // do amazing things!
  $section_title = get_sub_field('section_title');
  $align_title = get_sub_field('align_title');

  // Small Image
  $image_small = get_sub_field('two_image_small');
  $image_small_url = $image_small ? $image_small['url'] : '';
  $image_small_subtext = get_sub_field('two_image_small_subtitle');
  
  // Big Image
  $image_big = get_sub_field('two_image_big');
  $image_big_subtext = get_sub_field('two_image_big_subtitle');
	$image_big_url = $image_big ? $image_big['url'] : '';

  $align = get_sub_field('two_image_align');

  /******** CONTENT RENDER **********/
  /***********************************************************/
?>


<section class="mxw-1242 mh-auto content-block <?php echo $block_class_identifier; ?>">
  <?php 
    if ($section_title) {
      include( locate_template( 'inc/partials/section_title/section_title.php', false, false ) ); 
    }
  ?>
  <div class="relative z-1 flex-ns justify-between items-center <?php  if ($align === 'left') { echo 'flex-row-reverse'; }; if ($section_title) { echo 'mt-m'; } ?>">
    <div class="w-p-80 mt-m-s mh-auto-s mb-l-s <?php echo $block_class_identifier; ?>__image-small">
      <?php if($image_small_url) : ?>
        <div class="mb-s-s mb-m bg-cover bg-center <?php echo $block_class_identifier; ?>__image" style="background-image: url('<?php echo $image_small_url; ?>')"></div>
      <?php endif; ?>
      <?php if($image_small_subtext) : ?>
        <p class="pl-s-s pl-m f-secondary f7 tc-brown"><?php echo $image_small_subtext; ?></p>
      <?php endif; ?>
    </div>
    <div class="w-p-100 <?php echo $block_class_identifier; ?>__image-big">
      <?php if($image_big_url) : ?>
        <div class="mb-s-s mb-m bg-cover bg-center <?php echo $block_class_identifier; ?>__image" style="background-image: url('<?php echo $image_big_url; ?>')"></div>
      <?php endif; ?>
      <?php if($image_big_subtext) : ?>
        <p class="pl-s-s pl-m f-secondary f7 tc-brown"><?php echo $image_big_subtext; ?></p>
      <?php endif; ?>
    </div>
  </div>
</section>