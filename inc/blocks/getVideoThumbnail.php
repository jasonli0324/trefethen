<?php 
function extractYouTubeVideoID($url){
    $regExp = "/^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/";
    preg_match($regExp, $url, $video);
    return $video[7];
}

function getYouTubeThumbnailImage($video_id) {
    return "https://i3.ytimg.com/vi/$video_id/hqdefault.jpg"; //pass 0,1,2,3 for different sizes like 0.jpg, 1.jpg
}

function getVimeoId($url)
{
    if (preg_match('#(?:https?://)?(?:www.)?(?:player.)?vimeo.com/(?:[a-z]*/)*([0-9]{6,11})[?]?.*#', $url, $m)) {
        return $m[1];
    }
    return false;
}

function getVimeoThumb($id)
{
    $arr_vimeo = file_get_contents("https://vimeo.com/api/v2/video/$id.php");
    $arr_vimeo_array = unserialize($arr_vimeo);
    $thumb_url = $arr_vimeo_array[0]['thumbnail_large'];
    $thubm_url = str_replace("http:","https:",$thumb_url);
    // return $arr_vimeo[0]['thumbnail_small']; // returns small thumbnail
    // return $arr_vimeo[0]['thumbnail_medium']; // returns medium thumbnail
    return $thubm_url; // returns large thumbnail
}
?>