<?php
	$block_class_identifier = 'member-perks';

	/******** BLOCK SPECIFIC **********/
	/***********************************************************/

	// do amazing things!

	$title = get_sub_field('member_perks_title');
	$perks = get_sub_field('club_perks');

	/******** CONTENT RENDER **********/
	/***********************************************************/
?>

<section class='mxw-1952 mxw-1162-ds wrapper-small mh-auto ph-m content-block <?php echo $block_class_identifier; ?>'>
	<h2 class="mb-xl f-secondary uppercase ls-medium f4"><?php echo $title; ?></h2>
	<?php if($perks) : ?>
		<ul class="flex justify-between flex-wrap mxw-670">
			<?php foreach($perks as $perk): ?>
				<li class="w-p-50-s mb-l-s pr-m pr-l-ns <?php echo $block_class_identifier; ?>__item">
					<img src="<?php echo $perk['club_perk_image'];?>" class="mb-s <?php echo $block_class_identifier; ?>__icon">
					<p class="f6-s f-secondary fw-300 lh4 ls-tiny"><?php echo $perk['club_perk_title'];?></p>
				</li>
			<?php endforeach;?>
		</ul>
	<?php endif; ?>
</section>

