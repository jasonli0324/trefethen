<?php

  $block_class_identifier = 'assets-section';

  /******** BLOCK SPECIFIC **********/
  /***********************************************************/

  // do amazing things!
  $section_title = get_sub_field('section_title');
  $align_title = get_sub_field('align_title');
  $title_image = get_sub_field('title_image');

  /******** CONTENT RENDER **********/
  /***********************************************************/
?>

<section class="mxw-1952 mxw-1162-ds mh-auto content-block <?php echo $block_class_identifier; ?>">
  <?php 
    if ($section_title) {
      include( locate_template( 'inc/partials/section_title/section_title.php', false, false ) ); 
    }
  ?>
  <?php if($title_image) : ?>
    <div class="mxw-1162-ds wrapper-small mh-auto ph-m">
      <div class="relative z-1 mt-m mb-xxxl-nl bg-center bg-cover <?php echo $block_class_identifier; ?>__title-cover" style="background-image: url('<?php echo $title_image; ?>')"></div>
    </div>
  <?php endif; ?>
  <?php if( have_rows('assets') ) : ?>
    <?php while ( have_rows('assets') ) : the_row(); ?>
      <?php 
        $assets_field_title = get_sub_field('assets_field_title');
        $optional_link = get_sub_field('optional_link');
        $optional_link_label = $optional_link ? $optional_link['title'] : '';
        $optional_link_url = $optional_link ? $optional_link['url'] : '';
      ?>
      <div class="mxw-1078 mh-auto ph-m pb-m-l mb-xxl mb-xxxl-l">
        <h3 class="mb-m f-title h2 fw-300"><?php echo $assets_field_title; ?></h3>
        <?php if( have_rows('assets_list') ) : ?>
          <ul class="flex flex-wrap">
            <?php while ( have_rows('assets_list') ) : the_row(); ?>
              <?php
                $asset_image = get_sub_field('asset_image');
                $asset_file = get_sub_field('asset_file');
                $asset_label = get_sub_field('asset_label');
              ?>
              <li class="w-p-100 mb-m <?php echo $block_class_identifier; ?>__item">
                <a href="<?php echo $asset_file; ?>" target="blank" class="db mb-s bg-center bg-cover <?php echo $block_class_identifier; ?>__item-img" style="background-image: url('<?php echo $asset_image; ?>')"></a>
                <a href="<?php echo $asset_file; ?>" target="blank" class="db f-secondary tc-link h-tc-link-hover"><?php echo $asset_label; ?></a>
              </li>
            <?php endwhile; ?>
          </ul>
        <?php endif; ?>
          <?php if($optional_link_url) : ?>
            <a href="<?php echo $optional_link_url; ?>" class="relative dib f-secondary f6-nl lh4 fw-300 h-tc-link-hover td-40 featured-link"><?php echo $optional_link_label; ?><span class="pl-xs pl-s-ns tc-link icon-link-arrow"></span></a>
          <?php endif; ?>
      </div>
    <?php endwhile; ?>
  <?php endif; ?>
</section>