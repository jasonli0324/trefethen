<?php

	$block_class_identifier = 'heading-block';

	/******** BLOCK SPECIFIC **********/
	/***********************************************************/

	// do amazing things!

	$copy = get_sub_field('heading_block_copy');
	$title = get_sub_field('heading_block_headline') ? get_sub_field('heading_block_headline') : get_the_title();
	$sidebar = get_sub_field('heading_block_sidebar');
	$sidebar_title = $sidebar['heading_block_sidebar_title'];
	$sidebar_copy = $sidebar['heading_block_sidebar_copy'];
	$sidebar_links = $sidebar['heading_block_sidebar_links'];

	/******** CONTENT RENDER **********/
	/***********************************************************/
?>

<section class='mxw-1952 mxw-1162-ds wrapper-small mh-auto ph-m <?php echo $block_class_identifier; ?>'>
	<h1 class="<?php echo $copy ? 'mb-m' : ''; ?> h1 f-title fw-300 tc-primary-text"><?php echo $title; ?></h1>
	<div class="flex-ns justify-between">
		<?php if(!empty($copy)):?>
			<div class="mxw-740-nxl mxw-900 <?php if($sidebar_copy) {echo 'mb-xxl-s'; }?> tc-brown f3 f2-l intro-text featured-link-container"><?php echo $copy; ?></div>
		<?php endif; ?>
		<div class="f-secondary <?php echo $block_class_identifier; ?>__sidebar">
			<?php if($sidebar_title) : ?>
				<p class="mb-s f6-s uppercase ls-medium"><?php echo $sidebar_title; ?></p>
			<?php endif; ?>

			<?php if($sidebar_copy) : ?>
				<div class="mb-m mb-l-l f6-nl fw-300 lh4 tc-brown text-block"><?php echo $sidebar_copy; ?></div>
			<?php endif; ?>

			<?php if($sidebar_links) : ?>
				<div class="pt-m pt-l-ns <?php echo $block_class_identifier; ?>__sidebar-links">
					<?php foreach($sidebar_links as $link) : ?>
						<?php 
							$link_title = $link['heading_block_sidebar_link']['title']; 
							$link_url = $link['heading_block_sidebar_link']['url']; 
						?>
						<a href="<?php echo $link_url; ?>" class="relative dib mb-s f6-nl fw-300 lh4 featured-link"><?php echo $link_title; ?><span class="pl-s tc-link icon-link-arrow"></span></a>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
</section>

