<?php

  $block_class_identifier = 'image-quote';

  /******** BLOCK SPECIFIC **********/
  /***********************************************************/

  // do amazing things!
  $image = get_sub_field('image_quote_image');
	$imageUrl = $image ? $image['url'] : '';
  $imageSubtext = get_sub_field('image_quote_subtitle');
  $text = get_sub_field('image_quote_text');
  $align = get_sub_field('image_quote_align');

  /******** CONTENT RENDER **********/
  /***********************************************************/
?>


<section class="mxw-1952 mxw-1162-ds wrapper-small flex-ns justify-center items-center <?php echo $align === 'right' ? 'flex-row-reverse image-quote--right' : 'image-quote--left' ?> mh-auto ph-m-l content-block <?php echo $block_class_identifier; ?>">
  <div class="mb-l-s <?php echo $block_class_identifier; ?>__image">
    <?php if($imageUrl) : ?>
      <img class="w-p-100 mb-s" src="<?php echo $imageUrl; ?>" alt="<?php echo $imageSubtext; ?>" />
    <?php endif; ?>
    <?php if($imageSubtext) : ?>
      <p class="pl-s-s pl-m f-secondary f7 tc-brown"><?php echo $imageSubtext; ?></p>
    <?php endif; ?>
  </div>
  <?php if($text) : ?>
    <p class="ph-l-m f-title quote fw-300 f-italic lh3 tc-brown ta-center <?php echo $block_class_identifier; ?>__quote">
      <?php echo $text; ?>
    </p>
  <?php endif; ?>
</section>