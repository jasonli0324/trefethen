<?php

$block_class_identifier = 'wines-promo';

/******** BLOCK SPECIFIC **********/
/***********************************************************/

// do amazing things!
$section_title = get_sub_field('wines_rotating_promo_title');
$bg_image = get_sub_field('wines_rotating_promo_background_image');

/******** CONTENT RENDER **********/
/***********************************************************/
?>

<section class="relative content-block <?php echo $block_class_identifier; ?>" style="background-image: url('<?php echo $bg_image; ?>')">
  <?php 
    if ($section_title) {
      include( locate_template( 'inc/partials/section_title/section_title.php', false, false ) );
    }
  ?>
  <div class="relative z-1 mxw-1952 mxw-1162-ds wrapper-small mh-auto <?php if ($section_title) { echo 'mt-m'; }?> ph-m">
    <?php if( have_rows('wines_rotating_promo_slider') ) : ?>
      <ul class="js-wines-promo">
        <?php 
          while ( have_rows('wines_rotating_promo_slider') ) : the_row(); 
            $wineID = get_sub_field('wines_rotating_promo_slider_wine');
            $slider_image = get_sub_field('wines_rotating_promo_slider_image');
            $slider_copy = get_sub_field('wines_rotating_promo_slider_copy');
            $slider_link_copy = get_sub_field('wines_rotating_promo_slider_link_copy');
            $wine_name = get_the_title($wineID);
            $wine_url = get_permalink($wineID);
            $teaser = get_field('teaser', $wineID);
            $bottle_photo = get_field('bottle_photos', $wineID) ? get_field('bottle_photos', $wineID)['secondary_photo'] : '';
            $promo_image = $slider_image ? $slider_image : $bottle_photo;
            $promo_copy = $slider_copy ? $slider_copy : $teaser;
            $promo_link_copy = $slider_link_copy ? $slider_link_copy : 'Dicover ' . $wine_name;
        ?>
          <li class="<?php echo $block_class_identifier; ?>__item">
            <div class="flex-ns items-center">
              <div class="shrink-0 mh-auto-s mr-xxl-m mr-xxxl-l bg-cover bg-center <?php echo $block_class_identifier; ?>__image" style="background-image: url('<?php echo $promo_image; ?>')"></div>
              <div class="mt-xxl-s f-secondary ta-center-s">
                <?php if ($wine_name) : ?>
                  <h2 class="mb-l f6-s f4 ls-medium uppercase"><?php echo $wine_name; ?></h2>
                <?php endif; ?>
                <?php if ($promo_copy) : ?>
                  <p class="mb-l mb-xl-l f-title f-italic quote lh3 tc-brown"><?php echo $promo_copy; ?></p>
                <?php endif; ?>
                <?php if ($promo_link_copy) : ?>
                  <a href="<?php echo $wine_url; ?>" class="relative dib f6-s lh4 fw-300 h-tc-link-hover td-40 featured-link"><?php echo $promo_link_copy; ?><span class="pl-xs pl-s-ns tc-link icon-link-arrow"></span></a>
                <?php endif; ?>
              </div>
            </div>
          </li>
        <?php endwhile; ?>
      </ul>
      <?php 
        $slides = count(get_sub_field('wines_rotating_promo_slider'));
        $nav_horizontal = true;
        include( locate_template( 'inc/partials/slider_nav/slider_nav.php', false, false ) );
      ?>
    </div>
  <?php endif; ?>
</section>