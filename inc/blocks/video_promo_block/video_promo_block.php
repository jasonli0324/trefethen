<?php

$block_class_identifier = 'video-promo';

/******** BLOCK SPECIFIC **********/
/***********************************************************/

// do amazing things!
$section_title = get_sub_field('section_title');
$align_title = get_sub_field('align_title') ? get_sub_field('align_title') : 'left';
$copy = get_sub_field('video_promo_copy');
$videoUrl = get_sub_field('video_promo_video');
preg_match('/src="(.+?)"/', $videoUrl, $matches);

$src = $matches[1];

if(strpos($src, "youtube") !== false):
	$type = "youtube";
elseif(strpos($src, "vimeo") !== false):
	$type = "vimeo";
else:
	$type = "other";
endif;

/******** CONTENT RENDER **********/
/***********************************************************/
?>

<section class="content-block <?php echo $block_class_identifier; ?>">
  <?php include( locate_template( 'inc/partials/section_title/section_title.php', false, false ) ); ?>
  <div class="relative z-1 flex-ns items-center mxw-1952 mt-l mh-auto ph-m">
    <div class="w-p-100 w-p-60-ns w-p-70-l <?php echo $block_class_identifier; ?>__video-container">
      <?php if($type == 'youtube') :?>
        <?php                    
          $youtubeId = extractYouTubeVideoID($videoUrl);
          $thumbnail =  getYouTubeThumbnailImage($youtubeId);
        ?>
        <iframe class="w-p-100 mb-l-s va-middle <?php echo $block_class_identifier; ?>__video" src="<?php echo $src?>&modestbranding=1&autohide=1&controls=1&rel=0&autoplay=0&loop=1&enablejsapi=1&disablekb=1&playlist=<?php echo $youtubeId; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
      <?php elseif($type == 'vimeo'): ?>
        <?php                    
          $vimeoId = getVimeoId($videoUrl);
          $thumbnail = getVimeoThumb($vimeoId);
        ?>
        <iframe class="w-p-100 mb-l-s va-middle <?php echo $block_class_identifier; ?>__video" src="<?php echo $src?>&modestbranding=1&autohide=1&controls=1&autoplay=0&showinfo=0&loop=1&enablejs=1&muted=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
      <?php else: ?>
        <iframe class="w-p-100 mb-l-s va-middle <?php echo $block_class_identifier; ?>__video" src="<?php echo $src?>&modestbranding=1&autohide=1&controls=1&rel=0&autoplay=0&loop=1&muted=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
      <?php endif; ?>
    </div>
    <div class="w-p-100 w-p-40-ns w-p-30-l pl-m-m pl-l-l f-secondary <?php echo $block_class_identifier; ?>__content">
      <p class="mb-s-s mb-l fw-300 f6-nl lh4 ls-tinier"><?php echo $copy; ?></p>
      <?php if( have_rows('video_promo_links') ) : ?>
        <ul>
          <?php while ( have_rows('video_promo_links') ) : the_row(); ?>
            <?php 
              $link = get_sub_field('video_promo_link');
              $link_title = $link ? $link['title'] : '';
              $link_url = $link ? $link['url'] : '#';
            ?>
            <li>
              <a href="<?php echo $link_url; ?>" class="relative dib mb-s f6-nl lh4 fw-300 h-tc-link-hover td-40 featured-link"><?php echo $link_title; ?><span class="pl-xs pl-s-ns tc-link icon-link-arrow"></span></a>
            </li>
          <?php endwhile; ?>
        </ul>
      <?php endif; ?>
    </div>
  </div>
</section>