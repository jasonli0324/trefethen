<?php
  $block_class_identifier = 'journal-promotion';

  /******** BLOCK SPECIFIC **********/
  /***********************************************************/

  // do amazing things!
  $section_title = get_sub_field('section_title');
  $align_title = get_sub_field('align_title');

  $featured_image = get_sub_field('journal_promotion_image');
  $featured_posts = get_sub_field('journal_promotion_posts');

  /******** CONTENT RENDER **********/
  /***********************************************************/
?>

<section class="content-block <?php echo $block_class_identifier; ?>">
  <?php 
    if ($section_title) {
      include( locate_template( 'inc/partials/section_title/section_title.php', false, false ) ); 
    }
  ?>
  <div class="relative z-1 mxw-1952 mxw-1162-ds wrapper-small mh-auto ph-m <?php if ($section_title) {echo 'mt-m'; } ?> <?php echo $block_class_identifier; ?>__content">
    <div class="relative flex-ns">
      <div class="dn-s bg-cover bg-center <?php echo $block_class_identifier; ?>__image" style="background-image: url('<?php echo $featured_image; ?>')"></div>
      <?php if($featured_posts) : ?>
        <div class="w-p-100 mh-auto-s pl-m-m pl-l-l <?php echo $block_class_identifier; ?>__posts-container">
          <ul class="mb-m">
            <?php foreach($featured_posts as $post) : ?>
              <?php 
                $ID = $post->ID;
                $date = get_the_date('', $ID);
                $title = get_the_title($ID);
                $url = get_permalink($ID);
                $post_image = get_field('blog_post_photo', $ID)['url'];
              ?>
              <li class="pv-m bdb-1 <?php echo $block_class_identifier; ?>__post">
                <a href="<?php echo $url; ?>">
                  <p class="mb-s f-secondary f7"><?php echo $date; ?></p>
                  <h4 class="mb-s tc-link f3 f2-l"><?php echo $title; ?></h4>
                  <div class="absolute top-0 left-0 dn-s bg-cover bg-center td-60 <?php echo $block_class_identifier; ?>__image post-image" style="background-image: url('<?php echo $post_image; ?>')"></div>
                </a>
              </li>
            <?php endforeach; ?>
          </ul>
          <?php $post_type_archive = get_post_type_archive_link('post'); ?>
          <a href="<?php echo $post_type_archive; ?>" class="relative dib lh4 f-secondary fw-300 h-tc-link-hover td-40 featured-link">See All Posts <span class="pl-xs pl-s-ns tc-link icon-link-arrow"></span></a>
        </div>
      <?php endif; ?>
    </div>
  </div>
</section>