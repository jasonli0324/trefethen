<?php

  $block_class_identifier = 'wine-portfolio';

  /******** BLOCK SPECIFIC **********/
  /***********************************************************/

  // do amazing things!
  $wines = get_sub_field('wine_portfolio');
  $section_title = get_sub_field('section_title');
  $align_title = get_sub_field('align_title');
  $years = array();
  $labels = array();
  $is_wine = true;
  
  foreach($wines as $wineID) {
    if(get_field('type_varietal', $wineID)['wine_type'] === 'Gift' || get_field('type_varietal', $wineID)['wine_type'] === 'Culinary' || get_field('type_varietal', $wineID)['wine_type'] === 'Dessert') {
      $is_wine = false;
    } else {
      $year = get_field('dates', $wineID)['vintage'];
      array_push($years, $year);
    }
    $label = $is_wine ? get_field('type_varietal', $wineID)['label_name'] : get_field('wine_name', $wineID);
    array_push($labels, $label);
  }
  if($is_wine) {
    $years = array_unique($years);
  }
  $labels = array_unique($labels);

  /******** CONTENT RENDER **********/
  /***********************************************************/
?>


<section class="relative mxw-1952 mh-auto ph-m content-block <?php echo $block_class_identifier; ?>">
  <?php 
    if ($section_title) {
      include( locate_template( 'inc/partials/section_title/section_title.php', false, false ) ); 
    }
  ?>
  <div class="js-filter-container relative z-1 mxw-1952 mxw-1162-ds wrapper-small <?php if ($section_title) {echo 'mt-m';} ?> mh-auto ph-m <?php echo $block_class_identifier; ?>">
    <?php if($wines) : ?>
      <ul class="flex flex-wrap <?php echo $block_class_identifier; ?>__list">
        <?php foreach($wines as $wineID) : ?>
          <?php 
            $photos = get_field('bottle_photos', $wineID);
            $primaryPhoto = $photos ? $photos['primary_photo'] : '';
            $secondaryPhoto = $photos ? $photos['secondary_photo'] : '';
            $wineImage = $primaryPhoto ? $primaryPhoto : $secondaryPhoto;
            $wineName = get_the_title($wineID);
            $wineLabelName = get_field('type_varietal', $wineID)['label_name'];
            $wineSingleName = get_field('wine_name', $wineID);
            $wineYear = $is_wine ? get_field('dates', $wineID)['vintage'] : false;
            $bottle = get_field('bottle_photos', $wineID)['primary_photo'];
            $downloads = get_field('downloads', $wineID);
            $tech_sheet = $downloads['tech_sheet'];
            $label = $downloads['label'];
            $shelf_talker = $downloads['shelf_talker'];
          ?>
          <li class="js-portfolio-item flex w-p-33-l mb-xxl pb-s" data-year="<?php echo $wineYear; ?>" data-name="<?php echo $wineSingleName; ?>" data-label="<?php echo $wineLabelName; ?>">
          <div class="shrink-0 bg-cover bg-center <?php echo $block_class_identifier; ?>__bottle" style="background-image: url('<?php echo $wineImage; ?>')"></div>
          <div class="<?php echo $block_class_identifier; ?>__content">
              <?php if ($wineName) : ?>
                <p class="mb-m pb-m bdb-1 f3 f2-l <?php echo $block_class_identifier; ?>__name"><?php echo $wineName; ?></p>
              <?php endif; ?>
              <?php if ($bottle) : ?>
                <a href="<?php echo $bottle; ?>" target="blank" class="db f-secondary lh4 fw-300 ls-tinier tc-link">Bottle Shot</a>
              <?php endif; ?>
              <?php if ($tech_sheet) : ?>
                <a href="<?php echo $tech_sheet; ?>" target="blank" class="db f-secondary lh4 fw-300 ls-tinier tc-link">Tech Sheet</a>
              <?php endif; ?>
              <?php if ($shelf_talker) : ?>
                <a href="<?php echo $shelf_talker; ?>" target="blank" class="db f-secondary lh4 fw-300 ls-tinier tc-link">Shelf Talker</a>
              <?php endif; ?>
              <?php if ($label) : ?>
                <a href="<?php echo $label; ?>" target="blank" class="db f-secondary lh4 fw-300 ls-tinier tc-link">Label</a>
              <?php endif; ?>
            </div>
          </li>
        <?php endforeach; ?>
      </ul>
      <div class="mxw-1078 mh-auto ph-m-l">
        <div class="js-portfolio-filter flex flex-wrap items-center <?php if($is_wine) { echo 'js-is-wine'; } ?>" data-name="" data-year="">
          <p class="w-p-100-nl mr-m mb-l-nl  f-secondary uppercase ls-medium"><span class="icon-search"></span> Find a vintage:</p>
          <div class="relative z-2 w-p-100-s mb-l-nl mr-xl-ns <?php echo $block_class_identifier; ?>__search-name">
            <button class="js-portfolio-name-btn relative flex justify-between items-center w-p-100 h5 lh4 tc-primary-text <?php echo $block_class_identifier; ?>__search-input" data-selected="">
              <span class="js-name-placeholder tc-primary-text-50" data-placeholder="Select a wine">Select a wine</span>
              <span class="tc-link f5 icon-caret"></span>
            </button>
            <ul class="js-portfolio-name-list js-initial-names-list absolute w-p-100 pa-m dn bg-white <?php echo $block_class_identifier; ?>__search-list">
              <?php foreach($labels as $label) : ?>
                <li class="flex justify-between">
                  <button class="js-name-filter f-secondary fw-300 ls-tinier h-tc-link td-40" data-value="<?php echo $label; ?>"><?php echo $label;?></button>
                  <button class="js-remove-name tc-link icon-close va-middle fw-800 dn"></button>
                </li>
              <?php endforeach; ?>
            </ul>
            <ul class="js-portfolio-name-list js-searched-names-list absolute dn w-p-100 pa-m hidden bg-white <?php echo $block_class_identifier; ?>__search-list"></ul>
          </div>
          <?php if($is_wine) : ?>
            <div class="relative w-p-100-s mb-l-nl mr-xl-ns <?php echo $block_class_identifier; ?>__search-year">
              <button class="js-portfolio-year-btn relative flex justify-between items-center w-p-100 h5 lh4 tc-primary-text <?php echo $block_class_identifier; ?>__search-input" data-selected="">
                <span class="js-year-placeholder tc-primary-text-50" data-placeholder="Year">Year</span>
                <span class="tc-link f5 icon-caret"></span>
              </button>
              <ul class="js-portfolio-year-list js-initial-years-list absolute w-p-100 pa-m dn bg-white <?php echo $block_class_identifier; ?>__search-list">
                <?php foreach($years as $year) : ?>
                  <li class="flex justify-between">
                    <button class="js-year-filter f-secondary fw-300 ls-tinier h-tc-link td-40" data-value="<?php echo $year; ?>"><?php echo $year; ?></button>
                    <button class="js-remove-year tc-link icon-close va-middle fw-800 dn"></button>
                  </li>
                <?php endforeach; ?>
              </ul>
              <ul class="js-portfolio-year-list js-searched-years-list absolute dn w-p-100 pa-m hidden bg-white <?php echo $block_class_identifier; ?>__search-list"></ul>
            </div>
          <?php endif; ?>
          <div class="w-p-100-nl">
            <button class="js-search-wine db bdr-3 f-secondary tc-white ta-center bg-link h-bg-link td-40 <?php echo $block_class_identifier; ?>__search-btn">Search</button>
          </div>
        </div>
        <p class="js-name-error dn tc-link f-secondary">Please select a wine.</p>
        <p class="js-year-error dn tc-link f-secondary">Please select a year.</p>
        <div class="js-results-container"></div>
      </div>
    <?php endif; ?>
  </div>

</section>