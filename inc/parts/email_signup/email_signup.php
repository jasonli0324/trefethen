<?php 
  $images = get_field('email_signup_image', 'option');
  $copy = get_field('email_signup_copy', 'option');
  $label = get_field('email_signup_label', 'option');
  $thankyou = get_field('email_signup_thankyou', 'option');
?>
<section class="mv-xxl ph-m email-signup">
  <div class="pl-xl-l pv-m pb-xxl-s bg-taupe-bright">
    <div class="flex-ns items-center mxw-432-s mxw-1952 mh-auto ph-m ph-l-m">
      <img class="mb-xl-s email-signup__image" src="<?php echo $images; ?>" alt="email signup">
      <div class="mb-xl-s ml-m-ns ph-l-l f6-s f-title f-italic fw-300 email-signup__copy"><?php echo $copy; ?></div>
      <div class="w-p-100 pl-m-l email-signup__input">
        <!-- <p class="mb-xl uppercase f-secondary email-signup__label"><?php echo $label; ?></p> -->
        <div v65remotejs="form" class="remote-form" data-website="<?php echo get_field("store_url", "option");?>" data-formname="<?php echo get_field('pre_footer_winedirect_form_name','option');?>" >&nbsp;</div>
      </div>
    </div>
  </div>
</section>