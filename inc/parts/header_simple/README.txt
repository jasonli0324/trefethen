
HEADER_SIMPLE PART
--------------------------------------------------------------------
Created by: Highway 29 Creative




INTRODUCTION
----------------------------------

This is a basic page header and navigation.  Structure includes a primary header bar with logo and main menu.  Below 1024px it will collapse and use a mobile menu system.

Menu 'main-nav' is created as part of global theme init and is used in this 'part'.

The header has a number of setup options that are found in the WP admin at Theme Options->Header (after installation)



ACF GROUP VARIABLES
----------------------------------

Header ACF values are retrieved from the 'options' page 

	get_field('field_name', 'options')

Value structure is below

	sticky_header
	header_images
		header_logo
	header_colors
		header_background_color
		header_text_color
		menu_item_hightlight (primary, secondary, custom)
		custom_accent_color




INSTALLATION
----------------------------------

- import _header_simple.scss in place from main style.scss file.

	@import "../../../inc/parts/header_simple/header_simple";

- compile SASS
- copy to_be_moved/header_simple.js to /assets/javascript/src/before-init
- compile + minify JS
- copy to_be_moved/group_5a3e8f0c22b93.json to /acf-theme-json
- clone in 'PART - header_simple' ACF group (json in step above) into the theme options ACF group in the header tab
- update /header.php to be sure this part is referenced

	<?php FLEX::part('header_simple'); ?>