
PARTS GENERAL INFORMATION
--------------------------------------------------------------------
Created by: Highway 29 Creative




INTRODUCTION
----------------------------------

'Parts' are used for common, non block reusable sections. An example could be common social icons that are used in different places or by different blocks, header, footer, page banners, etc.

Often what can define something as a 'part' as opposed to a 'block' is if it has config options that should live in the general theme options page.  Also if something is used globally then it should be a 'part' and not a 'block'.

Items that are ALWAYS 'parts' include:

- page header (not referring the HTML head, but the page <header> and main navigation)
- page footer (not including closing tags for main content container div and body)
- page banner (with proper configuration it is possible to offer multiple page banner options)




STRUCTURE
----------------------------------

A 'part' is structured as below, though variations are possible based on your needs.

/ part_name

	part_name.php
	part_name-setup.php
	_part_name.scss
	README.txt

	/ to_be_moved

		group_XXXXXXXXXXXXX.json
		part_name.js


part_name.php
--> This is the insertion script that utilizes needed ACF fields and generates the front end code.

part_name-setup.php
--> This file contains code that needs to be ran at setup, such as registering the part if it's a page banner (more below), declaration of helper functions, etc.  The core functions.php file will include this file on setup.

_part_name.scss
--> This SASS file will contain the part specific styling.  It will be included in the main style.scss file as documented in the INSTALLATION section below.

README.txt
--> This document contains specific information about the purpose of the part and any needed installation or setup steps.

/ to_be_moved
--> this directory contains content that needs to be relocated on installation (more below) such as ACF groups and part specific javascript




INSTALLATION
----------------------------------

When installing a new part you'll have to follow the below general steps, though certain parts may contain additional steps as would be documented in the parts specific README.txt file.

1. Copy part directory in 'inc/parts/'.
2. Be sure the part has a unique name.
3. Copy any ACF JSON files to /acf-theme-json
4. Clone the ACF group into any place needed (such as the theme options).  Depending on the purpose you might have to create a new tab in the theme options page ACF group.
5. update /assets/styles/scss/style.scss and add an @import line for the part specific scss file.  Reference it from that file to it's actually location in the parts directory such as  
	
	@import "../../../inc/parts/page_banner_image/page_banner_image";

6. Compile SASS
7. If there is globally loaded JavaScript move it to the pre or post init directories in /assets/javascript/src
8. Compile JS  (using NPM script or replacement as explained in the /assets/javascript/README.txt file)


If the part is a replacement header or footer then you'll need to update the root /header.php or /footer.php file that is referencing the part.

If adding a new page banner be sure to remove any other page banner parts OR comment out their 'register_page_banner()' call in their setup file.  Be sure the new page banner calls that register function.




USE
----------------------------------

There is a global class called 'FLEX'.  Parts can be included in a PHP file by utilizing one of that classes methods.

<?php FLEX::part('part_name'); ?>

The header and footer parts are requested by the root header.php and footer.php files with the above code.  If multiple header and footer parts are loaded in the /inc/parts directory simply updating the part name requested in header.php and footer.php to allow you to switch and pull in the desired part.  At this point the header and footer are global, there is eventual potential to allow for page specific headers and footers.

The page banner is handled a bit differently so as to allow the eventual possibility of multiple user defined page banners.  The call for a page banner is handled with the below method.

<?php FLEX::banner(); ?>

The method code then handles inserting the registered page banner.  The page banner must be registered though with the following code in the page banner 'parts' setup file.

<?php register_page_banner('page_banner_part_name'); ?>

Eventually this will allow the registration of multiple page banners, at current only the last registered page banner is used.