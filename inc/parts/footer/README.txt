
FOOTER PART
--------------------------------------------------------------------
Created by: Highway 29 Creative




INTRODUCTION
----------------------------------

This is a basic page footer.  Structure is two rows, with top row containing general contact information text and recent news posts.  The bottom row contains a copyright notice and then links to additional pages (like privacy policy).

This part has a number of setup options that are found in the WP admin at Theme Options->Footer (after installation)



ACF GROUP VARIABLES
----------------------------------

Footer ACF values are retrieved from the 'options' page 

	get_field('field_name', 'options')

Value structure is below

	contact_information_section
		title
		content
	news_section
		title
		news_source
		news_items_to_display
		timestamp_color  (primary, secondary)
		link_to_news_item_page
	copyright_text_notice
	footer_links
		link_title
		link_type (page, post, external)
		page_link
		post_link
		external_link




INSTALLATION
----------------------------------

- import _footer.scss in place from main style.scss file.

	@import "../../../inc/parts/footer/footer";

- compile SASS
- copy to_be_moved/group_5a42fcdd9495a.json to /acf-theme-json
- clone in 'PART - footer' ACF group (json in step above) into the theme options ACF group in the footer tab
- update /footer.php to be sure this part is referenced

	<?php FLEX::part('footer'); ?>