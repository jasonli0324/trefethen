<?php
	$footer_style = get_field('footer_style', 'option');
	$bg_color = $footer_style['footer_bg_color'];
	$bg_image = $footer_style['footer_bg_image'];
?>
<footer class="ph-m footer-wrapper">
	<div class="pv-xl footer" style="background-color: <?php echo $bg_color; ?>; background-image: url('<?php echo $bg_image; ?>');">
		<div class="flex justify-end justify-between-l mh-auto pr-xxxl-m ph-m footer__wrapper">
			<nav class="dn-nl">
				<?php if( have_rows('footer_menu', 'option') ) : ?>
					<ul class="flex justify-between">
						<?php while ( have_rows('footer_menu', 'option') ) : the_row(); ?>
							<?php
								$main_link = get_sub_field('main_link');
								$main_link_title = $main_link ? $main_link['title'] : '';
								$main_link_url = $main_link ? $main_link['url'] : '';
							?>
							<li class="ta-right">
								<a class="relative dib mb-xl uppercase f-secondary ls-tinier h-tc-link-hover td-40 nav__main-link footer__main-link" href="<?php echo $main_link_url; ?>"><?php echo $main_link_title; ?></a>
								<?php if( have_rows('sub_menu', 'option') ) : ?>
									<ul>
										<?php while ( have_rows('sub_menu', 'option') ) : the_row(); ?>
											<?php
												$sub_menu_link = get_sub_field('sub_menu_link'); 
												$sub_menu_link_title = $sub_menu_link ? $sub_menu_link['title'] : '';
												$sub_menu_link_url = $sub_menu_link['url'];
											?>
											<li>
												<a class="db mb-s pb-xs-nl f3 f2-l f-italic h-tc-link-hover td-40 footer__sublink" href="<?php echo $sub_menu_link_url; ?>"><?php echo $sub_menu_link_title; ?></a>
												<?php if( have_rows('sub_menu_menu', 'option') ) : ?>
													<ul class="mb-xl">
														<?php while ( have_rows('sub_menu_menu', 'option') ) : the_row(); ?>
															<?php
																$sub_menu_menu_link = get_sub_field('sub_menu_menu_link'); 
																$sub_menu_menu_link_title = $sub_menu_menu_link ? $sub_menu_menu_link['title'] : '';
																$sub_menu_menu_link_url = $sub_menu_menu_link ? $sub_menu_menu_link['url'] : '';
															?>
															<li class="mb-s pb-xs-nl f7"><a class="f-secondary h-underline td-40" href="<?php echo $sub_menu_menu_link_url; ?>"><?php echo $sub_menu_menu_link_title; ?></a></li>
														<?php endwhile; ?>
													</ul>
												<?php endif; ?>
											</li>
										<?php endwhile; ?>
									</ul>
								<?php endif; ?>
							</li>
						<?php endwhile; ?>
					</ul>
				<?php endif; ?>
			</nav>
			<div class="flex flex-column items-end f6-s f-secondary ta-right">
				<?php
					$footer_contacts = get_field('footer_contacts', 'option');
					$footer_address = $footer_contacts['footer_address'];
					$footer_email = $footer_contacts['footer_email'];
					$footer_phone_number = $footer_contacts['footer_phone_number'];
					$footer_logo = get_field('footer_logo', 'option');
					$site_title = get_bloginfo('name');
					$copyright_notice = get_field('copyright_notice', 'option');
				?>
				<a href="<?php echo home_url('/');?>">
					<img src="<?php echo $footer_logo; ?>" class="mb-l mb-xl-ns footer__logo" alt="<?php echo $site_title; ?>">
				</a>
				<p class="mb-m mb-l-ns"><?php echo $site_title; ?></p>
				<div class="footer__addresses">
					<p class="mb-m mb-l-ns fw-300"><?php echo $footer_address; ?></p>
					<a class="db mb-m mb-l-ns fw-300" href="mailto:<?php echo $footer_email?>"><?php echo $footer_email?></a>
					<a class="db mb-l mb-xl-ns fw-300" href="tel:<?php echo $footer_phone_number?>"><?php echo $footer_phone_number?></a>
				</div>
				<?php if( have_rows('featured_links', 'option') ) : ?>
					<ul class="mb-m">
						<?php while ( have_rows('featured_links', 'option') ) : the_row(); ?>
							<?php
								$featured_link = get_sub_field('featured_link'); 
								$featured_link_title = $featured_link ? $featured_link['title'] : '';
								$featured_link_url = $featured_link['url'];
							?>
							<li class="mb-m"><a class="relative dib lh4 fw-300 h-tc-link-hover td-40 featured-link" href="<?php echo $featured_link_url; ?>"><?php echo $featured_link_title; ?><span class="pl-xs pl-s-ns tc-link icon-link-arrow"></span></a></li>
						<?php endwhile; ?>
					</ul>
				<?php endif; ?>
				<?php if( have_rows('locial_links', 'option') ) : ?>
					<ul class="flex mb-l mb-xl-ns">
						<?php while ( have_rows('locial_links', 'option') ) : the_row(); ?>
							<?php
								$social_network = get_sub_field('social_network'); 
								$social_link = get_sub_field('social_link'); 
							?>
							<li class="ml-m"><a href="<?php echo $social_link; ?>" class="f2 icon-<?php echo $social_network; ?>"></a></li>
						<?php endwhile; ?>
					</ul>
				<?php endif; ?>
				<div class="tc-primary-text-60 footer__institutional">
					<?php if( have_rows('institutional_links', 'option') ) : ?>
						<ul class="mb-m mb-l-ns f6-s f7">
							<?php while ( have_rows('institutional_links', 'option') ) : the_row(); ?>
								<?php
									$page_link = get_sub_field('page_link'); 
									$page_link_title = $page_link ? $page_link['title'] : '';
									$page_link_url = $page_link['url'];
								?>
								<li class="mb-xs"><a class="db lh4" href="<?php echo $page_link_url; ?>"><?php echo $page_link_title; ?></a></li>
							<?php endwhile; ?>
						</ul>
					<?php endif; ?>
					<p class="f6-s f7 lh4"><span class="js-year"></span> © <?php echo $copyright_notice; ?></p>
				</div>
			</div>
		</div>
	</div>
</footer>