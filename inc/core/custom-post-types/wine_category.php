<?php
/**
 * Wine Category class
 *
 */

class WineCategory {

	public function __construct() {	
		$this->create_post_type();
	}

	private function create_post_type() {
		/**
		 * Post Type: Wines
		 */
		$args = array(
			'labels' =>array(
				'name' => 'Wine Category',
				'singular_name' => 'Wine Category',
			),
			'description'         => '',
			'public'              => true,
			'publicly_queryable'  => true,
			'show_ui'             => true,
			'show_in_rest'        => false,
			'rest_base'           => '',
			'has_archive'         => false,
			'show_in_menu'        => true,
			'exclude_from_search' => false,
			'capability_type'     => 'post',
			'map_meta_cap'        => true,
			'hierarchical'        => false,
			'rewrite'             => [
				// 'slug'       => 'wines',
				'with_front' => false,
			],
      'query_var'           => true,
			'menu_icon' => 'dashicons-calendar-alt',
			'supports' =>array('title', 'editor', 'thumbnail','custom-fields')
		);
		register_post_type( 'winecategory', $args );
	}
}

new WineCategory;
