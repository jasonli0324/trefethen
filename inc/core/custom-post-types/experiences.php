<?php
/**
 * Experiences class
 *
 */

class Experiences {

	public function __construct() {
		$this->create_post_type();
	}

	private function create_post_type() {
		/**
		 * Post Type: Experiences
		 */
		$args = [
			'labels' => [
				'name'          => __( 'Experiences', '' ),
				'singular_name' => __( 'Experience', '' ),
			],
			'description'         => '',
			'public'              => true,
			'publicly_queryable'  => true,
			'show_ui'             => true,
			'show_in_rest'        => false,
			'rest_base'           => '',
			'has_archive'         => false,
			'show_in_menu'        => true,
			'exclude_from_search' => false,
			'capability_type'     => 'post',
			'map_meta_cap'        => true,
			'hierarchical'        => false,
      'taxonomies'  => array( 'experience_categories' ),
			'rewrite'             => [
				'slug'       => 'experiences',
				'with_front' => false,
			],
			'query_var'           => true,
			'menu_icon'           => 'dashicons-awards',
			'supports'            => ['title', 'custom-fields'],
		];

		register_post_type( 'experience', $args );
	}
}

new Experiences;


/**********************************************************
// Create categories for experiences custom post type
/**********************************************************/
function experience_categories_taxonomy() {
  $experience_cats = array(
    'name' => _x( 'Experience Categories', 'taxonomy general name' ),
    'singular_name' => _x( 'Experience Category', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Experience Categories' ),
    'all_items' => __( 'All Experience Categories' ),
    'parent_item' => __( 'Parent Experience Category' ),
    'parent_item_colon' => __( 'Parent Experience Category:' ),
    'edit_item' => __( 'Edit Experience Category' ), 
    'update_item' => __( 'Update Experience Category' ),
    'add_new_item' => __( 'Add New Experience Category' ),
    'new_item_name' => __( 'New Experience Category' ),
    'menu_name' => __( 'Experience Categories' ),
  );    
  
// Register the taxonomy. Replace the parameter experiences withing the array by the name of your custom post type.
  register_taxonomy('experience_categories',array('experience'), array(
    'hierarchical' => true,
    'labels' => $experience_cats,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
  ));
  
}
add_action( 'init', 'experience_categories_taxonomy', 0 );
