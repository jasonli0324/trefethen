<?php
/**
 * Events class
 *
 */

class Events {

	public function __construct() {
		$this->create_post_type();
	}

	private function create_post_type() {
		/**
		 * Post Type: Events
		 */
		$args = [
			'labels' => [
				'name'          => __( 'Events', '' ),
				'singular_name' => __( 'Event', '' ),
			],
			'description'         => '',
			'public'              => true,
			'publicly_queryable'  => true,
			'show_ui'             => true,
			'show_in_rest'        => false,
			'rest_base'           => '',
			'has_archive'         => false,
			'show_in_menu'        => true,
			'exclude_from_search' => false,
			'capability_type'     => 'post',
			'map_meta_cap'        => true,
			'hierarchical'        => false,
			'rewrite'             => [
				'slug'       => 'events',
				'with_front' => false,
			],
			'query_var'           => true,
			'menu_icon'           => 'dashicons-awards',
			'supports'            => ['title', 'custom-fields'],
		];

		register_post_type( 'event', $args );
	}
}

new Events;
