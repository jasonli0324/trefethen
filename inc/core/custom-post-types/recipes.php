<?php
/**
 * Recipes class
 *
 */

class Recipes {

	public function __construct() {
        $this->create_post_type();
	}

	private function create_post_type() {
		/**
		 * Post Type: Recipes
		 */
		$args = [
			'labels'              => [
				'name'          => __( 'Recipes', '' ),
				'singular_name' => __( 'Recipe', '' ),
			],
			'description'         => '',
			'public'              => true,
			'publicly_queryable'  => true,
			'show_ui'             => true,
			'show_in_rest'        => false,
			'rest_base'           => '',
			'has_archive'         => false,
			'show_in_menu'        => true,
			'exclude_from_search' => false,
			'capability_type'     => 'post',
			'map_meta_cap'        => true,
			'hierarchical'        => false,
      'taxonomies' 	 				=> array( 'recipe_categories' ),
			'rewrite'             => [
				'slug'       => 'recipes',
				'with_front' => false,
			],
			'query_var'           => true,
			'menu_icon'           => 'dashicons-awards',
			'supports'            => ['title', 'custom-fields'],
		];

		register_post_type( 'recipe', $args );
    }
}

new Recipes;

/**********************************************************
// Create categories for recipes custom post type
/**********************************************************/
function recipe_categories_taxonomy() {
  $recipe_cats = array(
    'name' => _x( 'Recipe Categories', 'taxonomy general name' ),
    'singular_name' => _x( 'Recipe Category', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Recipe Categories' ),
    'all_items' => __( 'All Recipe Categories' ),
    'parent_item' => __( 'Parent Recipe Category' ),
    'parent_item_colon' => __( 'Parent Recipe Category:' ),
    'edit_item' => __( 'Edit Recipe Category' ), 
    'update_item' => __( 'Update Recipe Category' ),
    'add_new_item' => __( 'Add New Recipe Category' ),
    'new_item_name' => __( 'New Recipe Category' ),
    'menu_name' => __( 'Recipe Categories' ),
  );    
  
// Register the taxonomy. Replace the parameter recipes withing the array by the name of your custom post type.
  register_taxonomy('recipe_categories',array('recipe'), array(
    'hierarchical' => true,
    'labels' => $recipe_cats,
    'show_ui' => true,
    'show_admin_column' => true,
		'query_var' => true,
		'rewrite'             => [
			'slug'       => 'pairings-recipes',
			'with_front' => true,
		],
  ));
  
}
add_action( 'init', 'recipe_categories_taxonomy', 0 );