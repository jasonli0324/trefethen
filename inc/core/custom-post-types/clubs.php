<?php
/**
 * Clubs class
 *
 */

class Clubs {

	public function __construct() {
		$this->create_post_type();
	}

	private function create_post_type() {
		/**
		 * Post Type: Clubs
		 */
		$args = [
			'labels' => [
				'name'          => __( 'Clubs', '' ),
				'singular_name' => __( 'Club', '' ),
			],
			'description'         => '',
			'public'              => true,
			'publicly_queryable'  => true,
			'show_ui'             => true,
			'show_in_rest'        => false,
			'rest_base'           => '',
			'has_archive'         => false,
			'show_in_menu'        => true,
			'exclude_from_search' => false,
			'capability_type'     => 'post',
			'map_meta_cap'        => true,
			'hierarchical'        => false,
			'rewrite'             => [
				'slug'       => 'clubs',
				'with_front' => true,
			],
			'query_var'           => true,
			'menu_icon'           => 'dashicons-awards',
			'supports'            => ['title', 'custom-fields'],
		];

		register_post_type( 'club', $args );
	}
}

new Clubs;
