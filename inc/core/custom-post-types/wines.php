<?php
/**
 * Wines class
 *
 */

class Wines {

	public function __construct() {
		$this->create_post_type();
	}

	private function create_post_type() {
		/**
		 * Post Type: Wines
		 */
		$args = array(
			'labels' =>array(
				'name' => 'Wines',
				'singular_name' => 'Wines',
			),
			'public' => true,
			'has_archive' => false,
			'menu_icon' => 'dashicons-calendar-alt',
			'rewrite' => [
				'slug'       => 'wine',
				'with_front' => false,
			],
			'supports' =>array('title', 'editor', 'thumbnail','custom-fields')
		);
		register_post_type( 'wine', $args );
	}
}

new Wines;
