
CORE SETUP GENERAL INFORMATION
--------------------------------------------------------------------
Created by: Highway 29 Creative




OVERVIEW
----------------------------------

The contents of this directory are loaded by the primary root functions.php file through the inclusion of the functions.php file located here.  All primary theme setup logic is executed through the code here.

NOTE: This theme utilizes the concept of content blocks, located in the /inc/blocks directory.  Some content blocks may need to have setup operations performed for them to operate correctly (or to create a custom post type that they use). To aid in modularity of these blocks, they are able to have block specific setup logic located in a block_name-setup.php located within a given blocks directory.  Code within the setup files here will look for any block specific setup code by looking for an appropriately named file within all the block directories and then include and execute it.




STRUCTURE
----------------------------------

Directory structure listed below.

	functions.php
	/custom-post-types
	/setup-parts
	/setup-plugins


functions.php
-->  primary holder of base theme setup logic, also references setup-parts and setup-plugins directories.


/setup-parts
-->  Directory contains larger code segments that have specific focus which are imported into core level functions.php


/setup-plugins
-->  Directory contains plugin specific setup logic for required plugins (mainly just ACF)


/custom-post-types
-->  Directory contains separate files for CPT creation if full built method needed for complete flexibility.