<?php


/*************************************************************
Load Block Setup Files

Some blocks contain block specific setup logic that needs to
be executed on init.  This might include thumbnail creation,
custom post type creation, etc.

To aid in modularity this funcitonality is allowed

This section checks for a file in each block that has
'-setup' appened to the end of the file name.  Such as
'your_block_name-setup.php'.
*************************************************************/

$block_directory = get_stylesheet_directory() . '/inc/blocks';
$directories = glob($block_directory . '/*' , GLOB_ONLYDIR);
foreach ($directories as $dir) {
	$file = glob($dir . '/*-setup.php');
	if (count($file) > 0) require_once($file[0]);
}




/*************************************************************
Block Preview Image Generation

Called by the enchanced message block in the block ACF groups
that will embed images of some block configurations.
*************************************************************/
function block_preview_images( $block ) {
	if ( $block != 'your_block_name' ) {
		echo "Some possible representations of this content block<br/><br/>";
		$dir = get_stylesheet_directory() . '/inc/blocks/' . $block . '/previews/';
		chdir($dir);
		foreach( glob("*.{jpg,png}", GLOB_BRACE) as $filename) {
		   $image = get_stylesheet_directory_uri() . '/inc/blocks/' . $block . '/previews/' . $filename;
		   echo '<image src="' . $image . '" style="width: 100%; height: auto; max-width: 500px">';
		}
	}
	else {
		echo '<b>Block folder name not defined in ACF Block General Design Field for auto image insertion!</b>';
	}
}




/*************************************************************
Block Support Helper Functions

These are helper functions which are commonly used in the 
blocks.  It helps reduce the block line count by utilizing 
a DRY methodology.
*************************************************************/



class BLOCK {


	/**
	 * processes custom block id if one defined and replaces any spaces
	 * with underscores
	 * @param  string $id custom id value for block
	 * @return string     processed id value
	 */
	static function set_id( $id ) {
		return str_replace(' ', '_', $id);
	}



	/**
	 * sets standardized block classes
	 * @param number $i iterator value that comes initially from FLEX class
	 * @return  string space seperated classes
	 */
	static function set_standard_classes( $i ) {
		return 'content_block block_' . $i;
	}



	/**
	 * checks if custom classes set
	 * @param string $classes list of classes form content_block_callses block identity
	 * @return  string class list or blank
	 */
	static function set_custom_classes( $classes ) {
		if (isset($classes)) return  ' ' . $classes;
		else return '';
	}



	/**
	 * function checks if a non custom padding collapse value is defined and
	 * then returns that, if custom is selected then empty return
	 * @param  string   $collapse   value from block_padding_adjustment block settings
	 * @return string   class name or empty
	 */
	static function set_padding_class( $collapse ) {
		if ($collapse != 'custom') return $collapse;
		else return '';
	}



	/**
	 * function checks if padding collapse set to custom and then generates
	 * inline styles based on custom top and bottom values defined
	 * @param  string   $collapse   value from block_padding_adjustment block settings
	 * @param  number   $top   custom top padding field
	 * @param  number   $bottom    custom bottom padding field
	 * @return string   inline style if custom defined and blank if not custom
	 */
	static function set_padding_style( $collapse, $top, $bottom) {
		if ($collapse == 'custom') {
			$top = (isset($top)) ? $top : 0;
			$bottom = (isset($bottom)) ? $bottom : 0;
			return ' padding-top:' . $top . 'px; padding-bottom:' . $bottom . 'px;';
		}
		else return '';
	}



	/**
	 * function checks if custom color style selected and then outputs needed
	 * inline styles
	 * @param string $style value from color_theme block setting
	 * @param string $text  custom text color hex
	 * @param string $bg    custom background color hex
	 */
	static function set_colors( $style, $text, $bg ) {
		if ($style == 'custom') {
			return ' color:' . $text . '; background:' . $bg . ';';
		}
		else return '';
	}



	/**
	 * set needed inner container width
	 * @param string $width value from content_width block setting
	 * @return string needed class or empty string
	 */
	static function set_width_class( $width ) {
		if ($width == 'full') return ' inner_full_width';
		else return '';
	}



	/**
	 * sets inline style if custom max width defined
	 * @param string $width value from content_width block setting
	 * @param number $max   value from custom_maximum_content_width block setting
	 * @return string  inline styles if custom defined or empty 
	 */
	static function set_width_style( $width, $max ) {
		$max = (isset($max)) ? $max : 1200;

		if ($width == 'custom') return ' max-width:' . $max . 'px;';
		else return '';
	}



	/**
	 * determines which link value to use and returns
	 * @param  array $cta 
	 * @return string      link address
	 */
	static function get_cta_link( $cta ) {
		$cta_link = array(
			'href' => '',
			'target' => '_blank'
		);

		if  ( $cta['link_type'] == 'external' ) $cta_link['href'] = $cta['external_link'];
		elseif ( $cta['link_type'] == 'file' ) {

			// check if id or url
			if ( is_numeric($cta['file_link']) ) $cta_link['href'] = wp_get_attachment_url( $cta['file_link'] );
			else $cta_link['href'] = $cta['file_link'];
		}
		elseif ( $cta['link_type'] == 'internal' ) {
			$cta_link['href'] = $cta['internal_link'];
			$cta_link['target'] = '_self';
		}
		elseif ( $cta['link_type'] == 'scrollto' ) {
			$cta_link['href'] = $cta['link_scroll'];
			$cta_link['target'] = '';
		}
		elseif ( $cta['link_type'] == 'scroll' ) {
			$cta_link['href'] = '#' . $cta['scroll_link'];
			$cta_link['target'] = '';
		}
		elseif ($cta['link_type'] == 'modal') {
			$cta_link['href'] = '#modal_' . $cta['modal_link'];
			$cta_link['target'] = '';
		}
		
		return $cta_link;
	}


	
	/**
	 * function associates proper cta style classes or inserts needed style tag
	 * @param  array $cta      cta values from repeater
	 * @param  string $selector used to directly CSS target the custom styles for cta (assuming style=custom)
	 * @return string           cta style classes
	 */
	static function get_cta_style( $cta, $selector ) {
		$button_classes = '';

		if ( $cta['style']  == 'primary') $button_classes = ' theme_accent_bg';
		elseif ( $cta['style']  == 'secondary') $button_classes = ' theme_accent2_bg';
		elseif ( $cta['style']  == 'primary-ghost') $button_classes = ' button--ghost theme_accent_bg';
		elseif ( $cta['style']  == 'secondary-ghost') $button_classes = ' button--ghost theme_accent2_bg';
		elseif ( $cta['style']  == 'white') $button_classes = ' button--white';						// here for legacy support
		elseif ( $cta['style']  == 'black') $button_classes = ' button--black';						// here for legacy support
		elseif ( $cta['style']  == 'white-ghost') $button_classes = ' button--white button--ghost';	// here for legacy support
		elseif ( $cta['style']  == 'black-ghost') $button_classes = ' button--black button--ghost';	// here for legacy support
		elseif ( $cta['style']  == 'custom') {
			?>
			<style>
			<?php echo $selector; ?> { background: <?php echo $cta['button_color']; ?>; color: <?php echo $cta['button_text_color']; ?>; }
			<?php echo $selector; ?>:hover { background: <?php echo adjustBrightness( $cta['button_color'], -40); ?>; }
			</style>
			<?php
		}
		elseif ( $cta['style']  == 'custom-ghost') {
			$button_classes = ' button--ghost';
			?>
			<style>
			<?php echo $selector; ?> { background: none; border-color: <?php echo $cta['button_outline_color']; ?>; color: <?php echo $cta['button_outline_color']; ?>; }
			<?php echo $selector; ?>:hover { background: <?php echo $cta['button_outline_color']; ?>; color: <?php echo $cta['button_text_hover_color']; ?>; }
			</style>
			<?php
		}

		return $button_classes;
	}
}




/**
 * Update block id value to replace spaces with underscores
 * @param  [type] $id [description]
 * @return [type]     [description]
 */
function get_block_id( $id ) {
	return str_replace(' ', '_', $id);
}


/**
 * check if top padding removed and then apply appropriate class
 * @param  [type] $collapse [description]
 * @return [type]           [description]
 */
function check_top_padding( $collapse ) {
	if ($collapse) return ' collapse-top-padding';
	else return '';
}




/**
 * returns a proper style if custom background selected
 * @param [type] $bg [description]
 * @return [type]     [description]
 */
function set_block_background( $bg ) {
	if ($bg) return ' background:' . $bg . ';';
	else return '';
}


/**
 * returns an appropriate class if full width selected
 * @param [type] $width [description]
 * @return [type]     [description]
 */
function set_block_width_class( $width ) {
	if ($width == 'full') return ' inner_full_width';
	else return '';
}


/**
 * returns proper style if custom max width defined
 * @param [type] $width [description]
 * @param [type] $max   [description]
 * @return [type]     [description]
 */
function set_block_width_style( $width, $max ) {
	if ($width == 'custom') return ' max-width:' . $max . 'px;';
	else return '';
}


/**
 * apply appropriate theme accent background class if either is selected
 * @param [type] $accent [description]
 * @return [type]     [description]
 */
function set_accent_background_class( $accent ) {
	if ( $accent == 'primary' ) return ' theme_accent_bg';
	elseif ( $accent == 'secondary' ) return ' theme_accent2_bg';
	else return '';
}


/**
 * if theme accent isn't used and custom bg defined return a proper background style
 * @param [type] $accent [description]
 * @param [type] $bg     [description]
 * @return [type]     [description]
 */
function set_accent_background_style( $accent, $bg ) {
	if ( $accent == 'custom' ) return ' background:' . $bg . ';';
	else return '';
}


/**
 * apply appropriate theme accent color class if either is selected
 * @param [type] $accent [description]
 * @return [type]     [description]
 */
function set_accent_color_class( $accent ) {
	if ( $accent == 'primary' ) return ' theme_accent_color';
	elseif ( $accent == 'secondary' ) return ' theme_accent2_color';
	else return '';
}


/**
 * if theme accent color isn't used and custom color defined return a proper color style
 * @param [type] $accent [description]
 * @param [type] $color  [description]
 * @return [type]     [description]
 */
function set_accent_color_style( $accent, $color ) {
	if ( $accent == 'custom' ) return ' color:' . $color . ';';
	else return '';
}


/**
 * apply appropriate theme font color class if either is selected
 * @param [type] $accent [description]
 * @return [type]     [description]
 */
function set_accent_font_class( $accent ) {
	if ( $accent == 'primary' ) return ' theme_accent_font_color';
	elseif ( $accent == 'secondary' ) return ' theme_accent2_font_color';
	else return '';
}


/**
 * if theme accent font color isn't used and custom font color defined return a proper color style
 * @param [type] $accent [description]
 * @param [type] $color  [description]
 * @return [type]     [description]
 */
function set_accent_font_style( $accent, $color ) {
	if ( $accent == 'custom' ) return ' color:' . $color . ';';
	else return '';
}


/**
 * retrieve appropriate link href based on link type
 * @param  [type] $cta [description]
 * @return [type]      [description]
 */
function get_cta_link( $cta ) {
	$cta_link = '';

	if ( $cta['link_type'] == 'page') $cta_link = $cta['page_link'];
	elseif ( $cta['link_type'] == 'post') $cta_link = get_permalink( $cta['post_link'] );
	elseif ( $cta['link_type'] == 'external') $cta_link = $cta['external_link'];
	elseif ( $cta['link_type'] == 'file') $cta_link = $cta['file_link'];

	return $cta_link;
}



/**
 * apply appropriate button classes or if custom selected add a small style tag section for specific button style targeting
 * @param  [type] $array [description]
 * @return [type]        [description]
 */
function get_cta_style( $array ) {
	$button_class = '';

	if ( $array['style']  == 'primary') $button_class = ' theme_accent_bg';
	elseif ( $array['style']  == 'secondary') $button_class = ' theme_accent2_bg';
	elseif ( $array['style']  == 'white') $button_class = ' button--white';
	elseif ( $array['style']  == 'black') $button_class = ' button--black';
	elseif ( $array['style']  == 'white-ghost') $button_class = ' button--white button--ghost';
	elseif ( $array['style']  == 'black-ghost') $button_class = ' button--black button--ghost';
	elseif ( $array['style']  == 'custom') {
		?>
		<style>
		<?php echo $array['css_selector']; ?> { background: <?php echo $array['button_color']; ?>; color: <?php echo $array['button_text_color']; ?>; }
		<?php echo $array['css_selector']; ?>:hover { background: <?php echo adjustBrightness( $array['button_color'], -40); ?>; }
		</style>
		<?php
	}

	return $button_class;
}

?>