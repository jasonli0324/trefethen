<?php

/*************************************************************
 Convert hexdec color string to rgb(a) string - helper
 *************************************************************/
 
function hex2rgba($color, $opacity = false) {
 
	$default = 'rgb(0,0,0)';
 
	//Return default if no color provided
	if(empty($color))
          return $default; 
 
	//Sanitize $color if "#" is provided 
        if ($color[0] == '#' ) {
        	$color = substr( $color, 1 );
        }
 
        //Check if color has 6 or 3 characters and get values
        if (strlen($color) == 6) {
                $hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
        } elseif ( strlen( $color ) == 3 ) {
                $hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
        } else {
                return $default;
        }
 
        //Convert hexadec to rgb
        $rgb =  array_map('hexdec', $hex);
 
        //Check if opacity is set(rgba or rgb)
        if($opacity){
        	if(abs($opacity) > 1)
        		$opacity = 1.0;
        	$output = 'rgba('.implode(",",$rgb).','.$opacity.')';
        } else {
        	$output = 'rgb('.implode(",",$rgb).')';
        }
 
        //Return rgb(a) color string
        return $output;
}





/*************************************************************
 Adjust color - helper
 *************************************************************/

function adjustBrightness($hex, $steps) {
    // Steps should be between -255 and 255. Negative = darker, positive = lighter
    $steps = max(-255, min(255, $steps));

    // Normalize into a six character long hex string
    $hex = str_replace('#', '', $hex);
    if (strlen($hex) == 3) {
        $hex = str_repeat(substr($hex,0,1), 2).str_repeat(substr($hex,1,1), 2).str_repeat(substr($hex,2,1), 2);
    }

    // Split into three parts: R, G and B
    $color_parts = str_split($hex, 2);
    $return = '#';

    foreach ($color_parts as $color) {
        $color   = hexdec($color); // Convert to decimal
        $color   = max(0,min(255,$color + $steps)); // Adjust color
        $return .= str_pad(dechex($color), 2, '0', STR_PAD_LEFT); // Make two char hex code
    }

    return $return;
}





/*************************************************************
 Custom Content and Excerpt Word Length
 *************************************************************/

function excerpt($limit, $end = '…') {
    $excerpt = explode(' ', get_the_excerpt(), $limit);
    $excerpt_full = explode(' ', get_the_excerpt());
    
    if (count($excerpt_full)>=$limit) {
        array_pop($excerpt);
        $excerpt = implode(" ",$excerpt) . $end;
    } 
    else {
        $excerpt = implode(" ",$excerpt);
    } 
    
    $excerpt = preg_replace('`[[^]]*]`','',$excerpt);
    return $excerpt;
}
 
function content($limit, $end = '…') {
  $content = explode(' ', get_the_content(), $limit);
  $content_full = explode(' ', get_the_content());
  
  if (count($content_full)>=$limit) {
    array_pop($content);
    $content = implode(" ",$content) . $end;
  } 
  else {
    $content = implode(" ",$content);
  } 
  
  $content = preg_replace('/[.+]/','', $content);
  $content = apply_filters('the_content', $content); 
  $content = str_replace(']]>', ']]&gt;', $content);
  return $content;
}




/*************************************************************
 function to generate random string used for unique id's
 *************************************************************/
function generateRandomString($length = 15) {
    return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
}



?>