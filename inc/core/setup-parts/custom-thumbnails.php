<?php


/*************************************************************
Custom Thumbnail Setup
*************************************************************/


// var used for thumbnail names
$custom_thumbnail_names = array();


/**
 * Update thumbnail name array
 * @param	array $sizes - list of thumbail prefixes and associated names
 * @return  array - updated list
 */
function pshadow_custom_image_sizes( $sizes ) {

	// access name array
	global $custom_thumbnail_names;

	// return merge of old sizes and new ones
	return array_merge( $sizes, $custom_thumbnail_names);

}


// Check if any custom thumbnails defined
if ( count($custom_thumbnail_sizes) >= 1 ) {

	// loop through thumbnail array
	foreach ($custom_thumbnail_sizes as $thumbnail) {

		// Add thumbnail size
		add_image_size( $thumbnail[1], $thumbnail[2], $thumbnail[3], $thumbnail[4] );

		// Add thumbnail name to array
		$custom_thumbnail_names[$thumbnail[1]] = __($thumbnail[0], $theme_text_domain);
	}

	// call for name set
	add_filter( 'image_size_names_choose', 'pshadow_custom_image_sizes' );
}



?>