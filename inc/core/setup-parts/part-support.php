<?php

/*************************************************************
Load Parts Setup Files

Some parts in the 'inc/parts/ directory contain setup logic 
that needs to be executed on init.  This might include menu 
association, thumbnail creation, custom post type creation, etc.

To aid in modularity this funcitonality is allowed

This section checks for a file in each part that has
'-setup' appened to the end of the file name.  Such as
'your_part_name-setup.php'.
*************************************************************/

$block_directory = get_stylesheet_directory() . '/inc/parts';
$directories = glob($block_directory . '/*' , GLOB_ONLYDIR);
foreach ($directories as $dir) {
	$file = glob($dir . '/*-setup.php');
	if (count($file) > 0) require_once($file[0]);
}


?>