<?php

/*************************************************************
Flex Content Class

This class is used for the insert of the various content
blocks, parts, and associate elements.
*************************************************************/

// block flex class for injecting page specific styles, scripts, and content blocks
class FLEX {


	// returns page specific custom styles fron the 'Flexible Content' ACF Group in style tags
	static function style() { 
		if (get_field('custom_css')) echo '<style>' . get_field('custom_css') . '</style>'; 
	}


	// returns page specific custom javascript fron the Flexible Content ACF Group in script tags
	static function script() { 
		if (get_field('custom_js')) echo '<script>' . get_field('custom_js') . '</script>'; 
	}


	// returns selected page banner
	static function banner($options = array()) {

		// grab globally registered page banner
		global $page_banner_part;

		// include page banner part
		$file = 'inc/parts/' . $page_banner_part . '/' . $page_banner_part . '.php';
		if (file_exists(get_stylesheet_directory() . '/' . $file)) include( locate_template( $file, false, false) );
		else echo '<h5><span style="color:red;">BANNER ERROR</span>: Banner <span style="color:white;background:#666666;">' . $page_banner_part . '</span> not found.  Might be part of a different git repository.';
	}



	// returns requested 'part' from /inc/parts/
	static function part($part_name) {
		$file = 'inc/parts/' . $part_name . '/' . $part_name . '.php';
		if (file_exists(get_stylesheet_directory() . '/' . $file)) include( locate_template( $file, false, false) );
		else echo '<h5><span style="color:red;">PART ERROR</span>: Part <span style="color:white;background:#666666;">' . $part_name . '</span> not found.  Might be part of a different git repository.';
	}



	// Loops through associated content blocks as defined in the flex content field in the 'Flexible Content' ACF group
	static function blocks() {
		// check if the flexible content field has rows of data
		if( have_rows('content_blocks') ) :
			// init block order variable
			$i = 0;

			// loop through content blocks
		    while ( have_rows('content_blocks') ) : the_row();

		    	// increment block order counter (used in the content block)
		    	$i++;

		    	// find and include content block
				$file = 'inc/blocks/' . get_row_layout() . '/' . get_row_layout() . '.php';
		    	if (file_exists(get_stylesheet_directory() . '/' . $file)) include( locate_template( $file, false, false ) ); 
		    	else echo '';

			endwhile;
		else:
			echo '';
		endif;
	}
}


?>