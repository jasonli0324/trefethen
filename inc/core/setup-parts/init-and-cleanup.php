<?php


/************************************************************
Misc Cleanup
************************************************************/


function pshadow_head_cleanup() {
	// EditURI link
	remove_action( 'wp_head', 'rsd_link' );
	// windows live writer
	remove_action( 'wp_head', 'wlwmanifest_link' );
	// previous link
	remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
	// start link
	remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
	// links for adjacent posts
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
	// WP version
	remove_action( 'wp_head', 'wp_generator' );
}

// remove WP version from RSS
function pshadow_rss_version() { return ''; }

// remove WP version from scripts
function pshadow_remove_wp_ver_css_js( $src ) {
	if ( strpos( $src, 'ver=' ) )
		$src = remove_query_arg( 'ver', $src );
	return $src;
}

// remove injected CSS for recent comments widget
function pshadow_remove_wp_widget_recent_comments_style() {
	if ( has_filter( 'wp_head', 'wp_widget_recent_comments_style' ) ) {
		remove_filter( 'wp_head', 'wp_widget_recent_comments_style' );
	}
}

// remove injected CSS from recent comments widget
function pshadow_remove_recent_comments_style() {
	global $wp_widget_factory;
	if (isset($wp_widget_factory->widgets['WP_Widget_Recent_Comments'])) {
		remove_action( 'wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style') );
	}
}

// remove injected CSS from gallery
function pshadow_gallery_style($css) {
	return preg_replace( "!<style type='text/css'>(.*?)</style>!s", '', $css );
}

// remove the p from around imgs (http://css-tricks.com/snippets/wordpress/remove-paragraph-tags-from-around-images/)
function pshadow_filter_ptags_on_images($content){
	return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}

// This removes the annoying […] to a Read More link
function pshadow_excerpt_more($more) {
	global $post;
	global $theme_text_domain;
	
	// edit here if you like
	return '...  <a class="excerpt-read-more" href="'. get_permalink( $post->ID ) . '" title="'. __( 'Read ', $theme_text_domain ) . esc_attr( get_the_title( $post->ID ) ).'">'. __( 'Read more &raquo;', $theme_text_domain ) .'</a>';
}




/*************************************************************
Theme Initilization
*************************************************************/


/**
 * Theme init function
 */
function pshadow_init() {
	
	
	//Allow editor style.
	add_editor_style( get_stylesheet_directory_uri() . '/assets/styles/editor-style.css' );


	// launching operation cleanup
	add_action( 'init', 'pshadow_head_cleanup' );
	// A better title
	add_filter( 'wp_title', 'rw_title', 10, 3 );
	// remove WP version from RSS
	add_filter( 'the_generator', 'pshadow_rss_version' );
	// remove pesky injected css for recent comments widget
	add_filter( 'wp_head', 'pshadow_remove_wp_widget_recent_comments_style', 1 );
	// clean up comment styles in the head
	add_action( 'wp_head', 'pshadow_remove_recent_comments_style', 1 );
	// clean up gallery output in wp
	add_filter( 'gallery_style', 'pshadow_gallery_style' );
	
	// cleaning up random code around images
	add_filter( 'the_content', 'pshadow_filter_ptags_on_images' );
	// cleaning up excerpt
	add_filter( 'excerpt_more', 'pshadow_excerpt_more' );

	
	// Check if the menu exists
	$menu_name = 'Main Menu';
	$menu_exists = wp_get_nav_menu_object( $menu_name );

	// If it doesn't exist, let's create it.
	if( !$menu_exists){
	    $menu_id = wp_create_nav_menu($menu_name);
	}

	// Create main menu
	global $theme_text_domain;
	register_nav_menus(
		array(
			'main-nav' => __( 'Main Menu', $theme_text_domain ),   // main nav in header
			'top-nav' => __( 'Top Menu', $theme_text_domain ),   // Top nav in header
			'footer-nav' => __( 'Footer Menu', $theme_text_domain )  // Footer nav in header
		)
	);

	// Enable support for HTML5 markup.
	add_theme_support( 'html5', array(
		'comment-list',
		'search-form',
		'comment-form'
	) );


	// If configured to do so, create a style guide page, other wise do it manually
	global $create_style_guide_page;
	if (($create_style_guide_page) && ( get_page_by_title( 'Style Guide' ) == NULL )) {
		$postarr = array(
			'post_type' => 'page',
			'post_title' => 'Style Guide',
			'post_status' => 'publish'
		);
		$style_guide_id = wp_insert_post( $postarr );
		update_post_meta( $style_guide_id, '_wp_page_template', 'page-style_guide.php' );
	}


	// Enable Featured Image
	add_theme_support( 'post-thumbnails' );

}

// let's get this party started
add_action( 'after_setup_theme', 'pshadow_init' );


?>