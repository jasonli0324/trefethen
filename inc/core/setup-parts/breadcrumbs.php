<?php 
  // Breadcrumbs
  function custom_breadcrumbs($separator = '/', $home_title  = 'Home') {
      
    // If you have any custom post types with custom taxonomies, put the taxonomy name below (e.g. product_cat)
    $custom_taxonomy = 'product_cat';
       
    // Get the query & post information
    global $post,$wp_query;
       
    // Do not display on the homepage
    if ( !is_front_page() ) {
       
      // Build the breadcrums
      echo '<ul class="flex f-secondary f7 breadcrumbs">';
          
      // Home page
      // echo '<li class="tc-link"><a href="' . get_home_url() . '" title="' . $home_title . '">' . $home_title . '</a></li>';
      // echo '<li> ' . $separator . ' </li>';
          
      if ( is_home() && !is_tax() && !is_category() && !is_tag() ) {
        // If post is a custom post type
        $post_type = get_post_type();
        // If it is a custom post type display name and link
        $post_type_archive = get_post_type_archive_link($post_type);
        $post_type_archive_name = ucfirst(basename($post_type_archive));
        $page = get_page_by_title($post_type_archive_name);
          
        if( $page->post_parent ) {
          // If child page, get parents 
          $anc = get_post_ancestors( $page->ID );
          
          // Get parents in the right order
          $anc = array_reverse($anc);
          // Parent page loop
          if ( !isset( $parents ) ) $parents = null;
          foreach ( $anc as $ancestor ) {
            $parents .= '<li class="tc-link"><a href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a></li>';
            $parents .= '<li> ' . $separator . ' </li>';
          }
          echo $parents;
          echo '<li class="">' . $post_type_archive_name . '</li>';
        } else {
          echo '<li class="">' . $post_type_archive_name . '</li>';
        }

      } else if ( is_archive() && !is_tax() && !is_category() && !is_tag() ) {
        
        echo '<li>' . post_type_archive_title($prefix, false) . '</li>';
      } else if ( is_archive() && is_tax() && !is_category() && !is_tag() ) { 
        // If post is a custom post type
        $post_type = get_post_type();
        
        if ($post_type === 'recipe') {
          $args = [
            'post_type' => 'page',
            'fields' => 'ids',
            'nopaging' => true,
            'meta_key' => '_wp_page_template',
            'meta_value' => 'page-' . $post_type . 's.php'
          ];
          $recipe_archive_page = get_posts($args);
          $parent_name = get_the_title($recipe_archive_page[0]);
          $parent_url = get_permalink($recipe_archive_page[0]);
          $grandparent = get_post_ancestors($recipe_archive_page[0]);
          
          if ($grandparent) {
            $grandparent_name = get_the_title($grandparent[0]);
            $grandparent_url = get_permalink($grandparent[0]);

            echo '<li class="tc-link"><a href="' . $grandparent_url . '" title="' . $grandparent_name . '">' . $grandparent_name . '</a></li>';
            echo '<li> ' . $separator . ' </li>';
          }

          echo '<li class="tc-link"><a href="' . $parent_url . '" title="' . $parent_name . '">' . $parent_name . '</a></li>';
          echo '<li> ' . $separator . ' </li>';

        } elseif($post_type !== 'post') {
          // If it is a custom post type display name and link
          $post_type_object = get_post_type_object($post_type);
          $post_type_archive = get_post_type_archive_link($post_type);
          echo '<li class="tc-link"><a href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
          echo '<li> ' . $separator . ' </li>';
        }
          
        $custom_tax_name = get_queried_object()->name;
        echo '<li>' . $custom_tax_name . '</li>';
      } else if ( is_single() ) {
        $post_type = get_post_type();
        $page_templates = array(
          'wine' => 'page-shop.php',
          'winecategory' => 'page-shop.php',
          'recipe' => 'page-recipes.php',
          'event' => 'page-events.php'
        );
        
        if ($post_type === 'winecategory' || $post_type === 'wine' || $post_type === 'recipe' || $post_type === 'event') {
          $args = [
            'post_type' => 'page',
            'meta_key' => '_wp_page_template',
            'meta_value' => $page_templates[$post_type]
          ];
          $pages = get_posts( $args );
          $page_parent = $pages[0]->post_parent;
          // if ($post_type === 'winecategory' || $post_type === 'wine') {
          //   echo '<li class="tc-link"><a href="' . get_home_url() . '" title="' . $home_title . '">' . $home_title . '</a></li>';
          //   echo '<li> ' . $separator . ' </li>';
          // } elseif ($page_parent) {
          if ($page_parent) {
            $post_type_archive_parent = get_permalink($page_parent);
            $post_type_archive_parent_name = get_the_title($page_parent);
            echo '<li class="tc-link"><a href="' . $post_type_archive_parent . '" title="' . $post_type_archive_parent_name . '">' . $post_type_archive_parent_name . '</a></li>';
            echo '<li> ' . $separator . ' </li>';
          }
          $page_id = $pages[0]->ID;
          $post_type_archive = get_permalink($page_id);
          $post_type_archive_name = get_the_title($page_id);

        } else {
          $post_type_archive = get_post_type_archive_link($post_type);
          $post_type_archive_name = ucfirst(basename($post_type_archive));
        }

        echo '<li class="tc-link"><a href="' . $post_type_archive . '" title="' . $post_type_archive_name . '">' . $post_type_archive_name . '</a></li>';
        echo '<li> ' . $separator . ' </li>';

        if($post_type === 'wine') {
          $initial_cat = get_field('type_varietal')['wine_type'];
          $type = get_field('wine_card_type');
          
          if($initial_cat === 'White' || $initial_cat === 'Rosé') {
            $cat = "whites-rose";
          } elseif($initial_cat === 'Culinary' || $initial_cat === 'Dessert' || $initial_cat === 'Gifting') {
            $cat = "culinary-gifting";
          } else {
            $cat = "red";
          }

          if ($initial_cat) {
            if ($type === 'featured' || $initial_cat === 'culinary') {
              $cat_page = get_page_by_path( 'culinary-gifting', OBJECT, 'winecategory' );
            } else {
              $cat_page = get_page_by_path( $cat, OBJECT, 'winecategory' );
            }
          }

          if ($cat_page) {
            $cat_page_id = $cat_page->ID;
            $cat_page_name = $cat_page->post_title;
            $cat_page_url = get_permalink($cat_page_id);
            echo '<li class="tc-link"><a href="' . $cat_page_url . '" title="' . $cat_page_name . '">' . $cat_page_name . '</a></li>';
            echo '<li> ' . $separator . ' </li>';
          } else {
            echo '<li class="tc-link"><span>' . $initial_cat . '</span></li>';
            echo '<li> ' . $separator . ' </li>';
          }
        }
        
        echo '<li>' . get_the_title() . '</li>';

      } else if ( is_category() ) {
        // Category page
        echo '<li>' . single_cat_title('', false) . '</li>';
      } else if ( is_page() ) {    
        // Standard page
        if( $post->post_parent ) {
          // If child page, get parents 
          $anc = get_post_ancestors( $post->ID );
              
          // Get parents in the right order
          $anc = array_reverse($anc);
              
          // Parent page loop
          if ( !isset( $parents ) ) $parents = null;
          foreach ( $anc as $ancestor ) {
            $parents .= '<li class="tc-link"><a href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a></li>';
            $parents .= '<li> ' . $separator . ' </li>';
          }
          // Display parent pages
          echo $parents;
          // Current page
          echo '<li>' . get_the_title() . '</li>';
        } else {
          // Just display current page with Home as parent if not parents
          // echo '<li class="tc-link"><a href="' . get_home_url() . '" title="' . $home_title . '">' . $home_title . '</a></li>';
          // echo '<li> ' . $separator . ' </li>';
          echo '<li>' . get_the_title() . '</li>'; 
        }
      } else if ( is_tag() ) {
        // Tag page
        // Get tag information
        $term_id        = get_query_var('tag_id');
        $taxonomy       = 'post_tag';
        $args           = 'include=' . $term_id;
        $terms          = get_terms( $taxonomy, $args );
        $get_term_id    = $terms[0]->term_id;
        $get_term_slug  = $terms[0]->slug;
        $get_term_name  = $terms[0]->name;
            
        // Display the tag name
        echo '<li>' . $get_term_name . '</li>';
      } elseif ( is_day() ) {
        // Day archive
        // Year link
        echo '<li class="tc-link"><a href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
        echo '<li> ' . $separator . ' </li>';
            
        // Month link
        echo '<li class="tc-link"><a href="' . get_month_link( get_the_time('Y'), get_the_time('m') ) . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</a></li>';
        echo '<li> ' . $separator . ' </li>';
            
        // Day display
        echo '<li>' . get_the_time('jS') . ' ' . get_the_time('M') . ' Archives</li>';
      } else if ( is_month() ) {
        // Month Archive
        // Year link
        echo '<li class="tc-link"><a href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
        echo '<li> ' . $separator . ' </li>';
            
        // Month display
        echo '<li>' . get_the_time('M') . ' Archives</li>';
      } else if ( is_year() ) {
        // Display year archive
        echo '<li>' . get_the_time('Y') . ' Archives</li>';
      } else if ( is_author() ) {
        // Auhor archive
        // Get the author information
        global $author;
        $userdata = get_userdata( $author );
            
        // Display author name
        echo '<li>' . 'Author: ' . $userdata->display_name . '</li>';
      } else if ( get_query_var('paged') ) {
        // Paginated archives
        echo '<li>'.__('Page') . ' ' . get_query_var('paged') . '</li>';
      } else if ( is_search() ) {
        // Search results page
        echo '<li>Search results for: ' . get_search_query() . '</li>';
      } elseif ( is_404() ) {
        // 404 page
        echo '<li>' . 'Error 404' . '</li>';
      }
      echo '</ul>';      
    }
  }