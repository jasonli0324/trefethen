<?php


/**
 * Load needed JS & CSS
 * ONLY for theme core JS and CSS.  Client spefic enqueuing should be added in theme root functions.php (toward bottom)
 */
function pshadow_scripts_and_styles() {

	// CSS
	$theme_css_path = get_stylesheet_directory() . '/assets/styles/style.css';
	wp_enqueue_style( 'customstyle', get_stylesheet_directory_uri() . '/assets/styles/style.css', array(), filemtime( $theme_css_path ), 'all' );
	// Scripts
	$theme_js_path = get_stylesheet_directory() . '/assets/javascript/theme.min.js';
	wp_enqueue_script( 'ajax-script', get_template_directory_uri() . '/assets/javascript/theme.min.js', array('jquery'), filemtime( $theme_js_path ), true );
  wp_localize_script( 'ajax-script', 'my_ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
    
}

// enqueue base scripts and styles
add_action( 'wp_enqueue_scripts', 'pshadow_scripts_and_styles', 999 );




/**
 * Remove jQuery Migrate dependancy
 */
function pshadow_jquery_migrate( $scripts ) {
	if ( ! is_admin() && ! empty( $scripts->registered['jquery'] ) ) {
		$jquery_dependencies = $scripts->registered['jquery']->deps;
		$scripts->registered['jquery']->deps = array_diff( $jquery_dependencies, array( 'jquery-migrate' ) );
	}
}
add_action( 'wp_default_scripts', 'pshadow_jquery_migrate' );


?>