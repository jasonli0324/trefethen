<?php

// Add Custom CSS to login page
function pshadow_login_css() {
	$login_css_path = get_stylesheet_directory() . '/assets/styles/login-style.css';
	wp_enqueue_style( 'pshadow_login_css', get_stylesheet_directory_uri() . '/assets/styles/login-style.css', array(), filemtime( $login_css_path ), 'all' );
}

// changing the logo link from wordpress.org to this site
function pshadow_login_url() {  return home_url(); }

// changing the alt text on the logo
function pshadow_login_title() { return get_option( 'blogname' ); }

// calling it only on the login page
add_action( 'login_enqueue_scripts', 'pshadow_login_css', 10 );
add_filter( 'login_headerurl', 'pshadow_login_url' );
add_filter( 'login_headertitle', 'pshadow_login_title' );


// Use selected header logo for login page
function custom_loginlogo() {
	$site_primary = get_field('site_primary_accent', 'options');

	echo '<style type="text/css">
	
	h1 a {background-image: url('.get_template_directory_uri().'/assets/images/torrin-black.png) !important; background-size: contain !important;}
	#wp-submit { background: ' . $site_primary['primary_accent_color'] . '}
	#wp-submit:hover { background: ' . adjustBrightness($site_primary['primary_accent_color'], -50) . '}
	</style>';
}
add_action('login_head', 'custom_loginlogo');


?>