<?php

// Flush rewrite rules for custom post types
add_action( 'after_switch_theme', 'pshadow_flush_rewrite_rules' );

// Flush your rewrite rules
function pshadow_flush_rewrite_rules() { flush_rewrite_rules(); }


/**
 * Modify custom post type array and then build cpt's
 */
function create_custom_post_types() {


	// add acf flex module post type
	global $custom_post_types, $theme_text_domain;
	array_push($custom_post_types, array(
		'name' => 'acf_flex_modules',
		'singular' => 'ACF Flex Module',
		'plural' => 'ACF Flex Modules',
		'desc' => 'Post Type for ACF flex module forms to be associated with',
		'public' => false,
		'show_ui' => true,
		'menu_position' => 999,
		'capability' => 'post',
		'icon' => '',
		'slug' => 'acf-flex-module',
		'has_archive' => false,
		'show_menu' => false
		)
	);

	// create custom post types
	foreach($custom_post_types as $posttype) {

		$pt_options = array( 'labels' => array(
			'name' => __( $posttype['plural'], $theme_text_domain ), /* This is the Title of the Group */
			'singular_name' => __( $posttype['singular'], $theme_text_domain ), /* This is the individual type */
			'all_items' => __( $posttype['plural'], $theme_text_domain ), /* the all items menu item */
			'add_new' => __( 'Add New ' . $posttype['singular'], $theme_text_domain ), /* The add new menu item */
			'add_new_item' => __( 'Add New ' . $posttype['singular'], $theme_text_domain ), /* Add New Display Title */
			'edit' => __( 'Edit ' . $posttype['singular'], $theme_text_domain ), /* Edit Dialog */
			'edit_item' => __( 'Edit ' . $posttype['singular'], $theme_text_domain ), /* Edit Display Title */
			'new_item' => __( 'New ' . $posttype['singular'], $theme_text_domain ), /* New Display Title */
			'view_item' => __( 'View ' . $posttype['singular'], $theme_text_domain ), /* View Display Title */
			'search_items' => __( 'Search ' . $posttype['plural'], $theme_text_domain ), /* Search Custom Type Title */ 
			'not_found' =>  __( 'Nothing found in the Database.', $theme_text_domain ), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __( 'Nothing found in Trash', $theme_text_domain ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( $posttype['desc'], $theme_text_domain ), /* Custom Type Description */
			'public' => $posttype['public'],
			'publicly_queryable' => $posttype['public'],
			'exclude_from_search' => true,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => $posttype['menu_position'], /* the order you want it to appear in the menu */ 
			'menu_icon' => $posttype['icon'], /* the icon for the custom post type menu */
			'rewrite'	=> array( 'slug' => $posttype['slug'], 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => $posttype['has_archive'], /* you can rename the slug here */
			'capability_type' => $posttype['capability'],
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky'),
			'show_in_nav_menus' => true,
			'show_in_menu' => $posttype['show_menu']
		); /* end of options */



		register_post_type( $posttype['name'], $pt_options );

	}

}
add_action( 'init', 'create_custom_post_types');



?>