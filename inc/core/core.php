<?php


/*************************************************************
Enable PHP error reporting
*************************************************************/


// If user config set, allow PHP error reporting
if ($error_reporting) {
	error_reporting(E_ALL);
	ini_set("display_errors", 1);
}



/*************************************************************
Register External CSS and JS
*************************************************************/
include(dirname(__FILE__) . '/setup-parts/enqueue-files.php');


/*************************************************************
Create Custom Post Types Defined in theme primary function.php
*************************************************************/
include(dirname(__FILE__) . '/setup-parts/cpt-builder.php');


/*************************************************************
Theme Initilization and Cleanup
*************************************************************/
include(dirname(__FILE__) . '/setup-parts/init-and-cleanup.php');


/*************************************************************
Custom Thumbnail Setup
*************************************************************/
include(dirname(__FILE__) . '/setup-parts/custom-thumbnails.php');


/*************************************************************
 Helper Functions
*************************************************************/
include(dirname(__FILE__) . '/setup-parts/helper-functions.php');




/*************************************************************
Allow SVG File Upload
*************************************************************/

function add_svg_to_upload_mimes( $upload_mimes ) {
	$upload_mimes['svg'] = 'image/svg+xml';
	$upload_mimes['svgz'] = 'image/svg+xml';
	return $upload_mimes;
}
add_filter( 'upload_mimes', 'add_svg_to_upload_mimes', 10, 1 );

// TODO: allow svg's to be previewed in media library




/*************************************************************
Allow Post Featured Images
*************************************************************/

function pshadow_theme_post_thumbnails() {
    add_theme_support( 'post-thumbnails' );
}
add_action( 'after_setup_theme', 'pshadow_theme_post_thumbnails' );




/*************************************************************
Plugin Check & Load Configuration
*************************************************************/

// display custom admin notice
function acf_custom_admin_notice() { 
	?>
	
	<div class="notice update-nag">
		<p><?php _e( 'Warning: Advanced Custom Fields is not installed or activated. The ACF plugin is required by this theme!' ); ?></p>
	</div>
	
	<?php 
}

// If ACF then load config
if ( class_exists('acf') ) require_once(dirname(__FILE__) . '/setup-plugins/advanced-custom-fields.php');
else add_action('admin_notices', 'acf_custom_admin_notice');

// make sure SEO meta box below acf
apply_filters( 'wpseo_metabox_prio', 'low' );



/*************************************************************
Parts Setup and Support
*************************************************************/

$page_banner_part;
function register_page_banner($banner_part) {
	global $page_banner_part;
	$page_banner_part = $banner_part;
}




/*************************************************************
Parts Setup and Support
*************************************************************/
require_once(dirname(__FILE__) . '/setup-parts/part-support.php');


/*************************************************************
Block Setup and Support
*************************************************************/
require_once(dirname(__FILE__) . '/setup-parts/block-support.php');


/*************************************************************
Flex Class for parts and block insertion
*************************************************************/
require_once(dirname(__FILE__) . '/setup-parts/flex.php');




/*************************************************************
Pust Yoast SEO meta to bottom
*************************************************************/

function yoasttobottom() {
	return 'low';
}
add_filter( 'wpseo_metabox_prio', 'yoasttobottom');

?>