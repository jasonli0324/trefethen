<?php


/*******************************
ACF JSON
*******************************/


// Define folder where custom ACF JSON is stored
$custom_acf_json_path = '/acf-theme-json';



// Update ACF JSON save point
function my_acf_json_save_point( $path ) {
	global $custom_acf_json_path;

    $path = get_stylesheet_directory() . $custom_acf_json_path;
    
    return $path;
}
// This can be disabled to remove fields from being shown in admin front end
add_filter('acf/settings/save_json', 'my_acf_json_save_point');




//Update ACF JSON load point to load JSON form folder not updated by front end
function my_acf_json_load_point( $paths ) {
    global $custom_acf_json_path;

    // remove original path (optional)
    unset($paths[0]);
    
    // append path
    $paths[] = get_stylesheet_directory() . $custom_acf_json_path;
    
    return $paths;
}
// Disabled at the moment because modules not built yet
add_filter('acf/settings/load_json', 'my_acf_json_load_point');




/*************************************************************
Add class to content block flex button popup
*************************************************************/


function flex_content_block_modal() {
   
    echo "<script>
    	jQuery( document ).ready(function() {
	      (function($){
	      	function addClass() {
	        	var flex_btn_script = $('.acf_theme_layout > .acf-input > .acf-flexible-content > .acf-actions + script');
	        	if (flex_btn_script.exists()) {
					if (flex_btn_script.html().indexOf('content_block_modal') == -1) {
						flex_btn_script.html( '<div class=\"acf-fc-popup content_block_modal\">' + flex_btn_script.html() + '</div>' );
					}
				}
			}
			addClass();
			acf.add_action('append', addClass);
	      })(jQuery);
	    });
    </script>";
}
function flex_modal() {
	if ( class_exists('acf') ) {
		add_action('acf/input/admin_footer', 'flex_content_block_modal');
	}
}
add_action( 'init', 'flex_modal');




/*************************************************************
Add Custom Colors to ACF color picker and rgba picker
*************************************************************/


function change_acf_color_picker() {
    global $parent_file;
    $acf_colors = create_custom_color_array('acf');

    if ($acf_colors == 'kill') return;

    $client_colors_acf = array();

    foreach ( $acf_colors as $value ) {
        $client_colors_acf[] = '#' . $value;
    }

    $client_colors_acf_jquery = json_encode($client_colors_acf);

    echo "<script>
      (function($){
        acf.add_action('ready append', function() {
          acf.get_fields({ type : 'color_picker'}).each(function() {
            $(this).iris({
              color: $(this).find('.wp-color-picker').val(),
              mode: 'hsv',
              palettes: ".$client_colors_acf_jquery.",
              change: function(event, ui) {
                $(this).find('.wp-color-result').css('background-color', ui.color.toString());
                $(this).find('.wp-color-picker').val(ui.color.toString());
              }
            });
          });
        }); 
      })(jQuery);
    </script>";
}
function acf_color_picker() {
	if ( class_exists('acf') ) {
		//add_action( 'acf/input/admin_head', 'change_acf_color_picker' );
	}
}


function set_acf_rgba_color_picker_palette() {
    global $parent_file;
    $acf_colors = create_custom_color_array('acf');

    if ($acf_colors == 'kill') return;

    $client_colors_acf = array();

    foreach ( $acf_colors as $value ) {
        $client_colors_acf[] = '#' . $value;
    }

    return $client_colors_acf;
}
add_filter('acf/rgba_color_picker/palette', 'set_acf_rgba_color_picker_palette');

add_action( 'init', 'acf_color_picker');




/*******************************
ACF add theme options pages
*******************************/


/**
 * ACF init
 */
function my_acf_init() {
	//global $theme_header_options_page, $theme_footer_options_page;

	if( function_exists('acf_add_options_page') ) {

		acf_add_options_page(array(
			'page_title' 	=> __('Theme Settings', 'sfs'),
			'menu_title' 	=> __('Theme Settings', 'sfs'),
			'menu_slug' 	=> 'theme-general-settings',
			'capability' 	=> 'administrator',
			'position'		=> 31,
			'icon_url' => 'dashicons-admin-generic',
			'redirect' 	=> false
		));

	}
}
add_action('acf/init', 'my_acf_init');





/*******************************
ACF Content Block Title
*******************************/


// Allows field designated 'content_block_title' to be used as flex content block title in admin
function my_acf_flexible_content_layout_title( $title, $field, $layout, $i ) {
	
	// original title
	$old_title = $title;
	$old_title = str_replace('BLOCK - ', '', $old_title);

	// remove layout title from text
	$title = '';
	
	// load text sub field if available
	if( $text = get_sub_field('content_block_name') ) {	
		$title .= '<b>' . $text . ' - (' . $old_title . ')</b>';	
	}
	else $title = $old_title;
	
	// return
	return $title;	
}
add_filter('acf/fields/flexible_content/layout_title', 'my_acf_flexible_content_layout_title', 10, 4);




/*******************************
ACF Form CSS
*******************************/


// Loads custom CSS into admin head when ACF form being used
function my_acf_admin_head() {
	$acf_css_path = get_stylesheet_directory() . '/assets/styles/acf-admin-style.css';
	wp_enqueue_style( 'acf_admin_css', get_stylesheet_directory_uri() . '/assets/styles/acf-admin-style.css', array(), filemtime( $acf_css_path ), 'all' );
}
add_action('acf/input/admin_head', 'my_acf_admin_head');





/*******************************
Speed up load time
*******************************/


add_filter('acf/settings/remove_wp_meta_box', '__return_true');		




/*******************************
Stay on active ACF tab when saving page
*******************************/

function handle_tab() {
	?>
	<script>
	jQuery(function(){
		function parseHash() {
		    var hash = window.location.hash.substring(1),
		        params = {};
		    if (hash.length < 5) {
		        return params;
		    }
		    var vars = hash.split('&');
		    for (var i = 0; i < vars.length; i++) {
		        if (!vars[i]) {
		            continue;
		        }
		        var pair = vars[i].split('=');
		        if (pair.length < 2) {
		            continue;
		        }
		        params[pair[0]] = pair[1];
		    }
		    return params;
		}
		var hashData = parseHash();
		if (hashData && hashData.acf_tab) {
		    jQuery('a.acf-tab-button[data-key="' + hashData.acf_tab + '"]').trigger('click');
		    jQuery('form#post').attr('action', '#&acf_tab=' + hashData.acf_tab);
		}
		jQuery('a.acf-tab-button').on('click', function(event) {
		    window.location.hash = '&acf_tab=' + jQuery(this).data('key');
		    jQuery('form#post').attr('action', '#&acf_tab=' + jQuery(this).data('key'));
		});
	});
	</script>
	<?php
}
add_action('acf/input/admin_footer', 'handle_tab');




/*******************************
add default image setting to ACF image fields
let's you select a defualt image
this is simply taking advantage of a field setting that already exists
*******************************/

  
add_action('acf/render_field_settings/type=image', 'add_default_value_to_image_field');
function add_default_value_to_image_field($field) {
	acf_render_field_setting( $field, array(
		'label'			=> 'Default Image',
		'instructions'		=> 'Appears when creating a new post',
		'type'			=> 'image',
		'name'			=> 'default_value',
	));
}




/*******************************
On page load auto close all repeater 
and flex content collapsible elements
*******************************/

function rdsn_acf_repeater_collapse() {
?>
<script type="text/javascript">
	jQuery(function($) {
		$('.acf-flexible-content .layout').addClass('-collapsed');
	});
</script>
<?php
}
add_action('acf/input/admin_head', 'rdsn_acf_repeater_collapse');



/*******************************
Add Google Maps API Key
*******************************/

add_filter('acf/settings/google_api_key', function () {
    //return 'AIzaSyAAiFNIA9sOSPf-QgiirVZmUHb0nDVyB38';
    return get_field('google_maps_api_key', 'options');
});


/*******************************
ACF Input types embedded
*******************************/


include(dirname(__FILE__) . '/acf-code-field/acf-code-field.php');
include(dirname(__FILE__) . '/acf-rgba-color-picker/acf-rgba-color-picker.php');
include(dirname(__FILE__) . '/acf-enhanced-message-field/acf-enhanced-message.php');

