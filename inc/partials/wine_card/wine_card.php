<?php
  $is_subprod = isset($sub_product) && $sub_product;
  if ($is_subprod) {
    $is_featured = false;
  } else {
    $is_featured = get_field('wine_card_type', $wineID) === 'featured';
  }
  $sku = get_field('sku', $wineID);
  $alternate_name = get_field('alternate_format_name', $wineID);
  $alternate_price = get_field('alternate_alternate_price', $wineID);
  $bottle_size = get_field('type_varietal', $wineID)['size'];
  $product_type = get_field('type_varietal', $wineID)['wine_type'];
  $bottle_photos = get_field('bottle_photos',$wineID);
  $gift_pack_photos = get_field('gift_pack_photos',$wineID);
  $is_cat_page = isset($is_cat_page) ? $is_cat_page : false;
?>
<article class="js-product relative items-center w-p-100 bg-white wine-card <?php echo !$is_cat_page && $is_featured ? "flex-ns wine-card--package" : ($is_subprod ? ' flex wine-card--sub-product' : ' flex w-p-50-ns'); ?>">
  <?php 
    $src = get_field('wine_card_type', $wineID) === 'standard' ? $bottle_photos['primary_photo'] : $gift_pack_photos['gift_pack_image']; 
    if(!$is_featured && $vivino_rating !== '' && (float)$vivino_rating >= 4) {
      $rating_unit = floor($vivino_rating);
      $rating_decimal = $vivino_rating - $rating_unit;
      $star_full = get_stylesheet_directory_uri().'/assets/images/star_full.svg';
      $star_empty = get_stylesheet_directory_uri().'/assets/images/star_empty.svg';
      $star_half = get_stylesheet_directory_uri().'/assets/images/star_half.svg';
    } 
  ?>
  <?php if ($is_subprod) : ?>
    <span class="absolute center-elem-v bd-round ta-center f-secondary bg-white tc-brown wine-card__counter">x <?php the_sub_field('number_of_products')?></span>
  <?php endif; ?>
  <a class="js-product-image <?php echo !$is_cat_page && $is_featured ? "db-s w-p-100 w-p-50-ns mr-s-ns bg-cover" : 'w-p-33 mr-s bg-height'; ?> h-p-100 bg-no-repeat bg-center wine-card__image" href="<?php echo get_permalink($wineID);?>" data-img="<?php echo $src; ?>" style="background-image: url('<?php echo $src; ?>')"></a>
  <div class="flex flex-column justify-between <?php echo !$is_cat_page && $is_featured ? "w-p-100 w-p-50-ns" : 'w-p-66'; ?> h-p-100 pv-xl-s pv-xxl pr-l pl-l pl-xl-l">
    <div>
      <h2 class="mb-m tc-dark-grey"><a class="js-wine-name wine-card__title" href="<?php echo get_permalink($wineID);?>"><?php echo get_field('wine_name', $wineID);?> <?php if(isset($dates)) echo $dates['vintage']?></a></h2>
      <?php if (!$is_featured && $vivino_rating && (int)$vivino_rating >= 4) : ?>
        <div class="flex flex-column-m items-center items-start-m pb-s f-secondary">
          <div class="flex mb-s-m">
            <span class="icon-vivino"></span>
            <div class="js-stars-container flex mr-s">
              <?php for ($i=0; $i < 5 ; $i++) : ?>
                <i class="relative f6 mr-xs star-under icon-star">
                  <i class="absolute top-0 left-0 dn mr-xs f6 of-hidden star-over icon-star"></i>
                </i>
              <?php endfor; ?>
            </div>
          </div>
          <div class="flex">
            <p class="js-rating-score f7 mr-xs"><?php echo $vivino_rating;?></p>
            <p class="js-rating-counts dn-s f7 tc-link"> (<span><?php echo $vivino_count;?></span> Reviews)</p>
          </div>
        </div>
      <?php endif; ?>
    </div>

    <div>
      <div v65remotejs="addToCartForm" productsku="<?php echo get_field('sku',$wineID);?>" style="display:none;"></div>
      <?php if (!$is_featured) : ?>
        <div class="js-price-display relative mb-l">
          <!-- <button class="js-price-choices"> -->
            <!-- <span class="dn-nl icon-caret"></span> -->
            <p class="f-secondary fw-300">$<?php echo $price ? $price : '0'; ?> / <?php echo $product_type === 'Dessert' || $product_type === 'Culinary' ? (get_field('type_varietal', $wineID)['item_denomination'] ? get_field('type_varietal', $wineID)['item_denomination'] : 'Item') : $bottle_size; ?></p>
          <!-- </button> -->
          <!-- <ul class="js-prices-list absolute dn bg-white wine-card__price-list">
            <li>
              <input class="absolute o-0" type="radio" name="wine-price-<?php echo $index; ?>" id="<?php echo $sku; ?>-bottle" checked>
              <label class="js-select-price js-price-bottle db f-secondary f7 pointer" for="<?php echo $sku; ?>-bottle">$<?php echo $price; ?> / Bottle</label>
            </li>
            <?php if ($alternate_name !== '') : ?>
              <li>
                <input class="absolute o-0" type="radio" name="wine-price-<?php echo $index; ?>" id="<?php echo $sku; ?>-<?php echo $alternate_name; ?>"> 
                <label class="js-select-price js-price-alternate db f-secondary f7 pointer" for="<?php echo $sku; ?>-<?php echo $alternate_name; ?>">$<?php echo $alternate_price . ' / ' . $alternate_name ?></label>
              </li>
            <?php endif; ?>
            <li class="dn">
              <input class="absolute o-0" type="radio" name="wine-price-<?php echo $index; ?>" id="<?php echo $sku; ?>-case"> 
              <label class="js-select-price js-price-case db f-secondary f7 pointer" for="<?php echo $sku; ?>-case"></label>
            </li>
          </ul> -->
        </div>
      <?php else:  ?>
        <?php $price_label = $product_type === 'Gift' || $product_type === 'Culinary' ? (get_field('type_varietal', $wineID)['item_denomination'] ? get_field('type_varietal', $wineID)['item_denomination'] : 'Item') : 'Pack';  ?>
        <p class="mb-l f-secondary fw-300">$<?php echo $price ? $price : '0';?> / <?php echo $price_label; ?></p>
      <?php endif; ?>
      <?php if (!$is_subprod) : ?>
        <div>
          <div class="js-shop-add-to-cart-container">
            <div class="js-shop-add-to-cart flex">
              <input class="quantity bdr-3 f6-s ta-center wine-card__input" type="number" value="1" />
              <button class="w-p-100 bdr-3 f-secondary f6-s tc-link td-40 btn-link wine-card__button">Add to Cart</button>
            </div>
            <!-- <button class="js-shop-case-cart">Buy a case</button>
            <?php if(!$is_featured) : ?>
              <p class="js-shop-case-cart-sold-out dn bdr-3 f-secondary ls-tinier tc-white bg-primary-text ta-center single-wine__sold-out">Cases sold out</p>
            <?php endif; ?> -->
          </div>
          <div class="js-card-sold-out dn">
            <p class="mb-m bdr-3 f-secondary fw-300 ls-tinier">Sold Out</p>
            <button class="w-p-100 bdr-3 f-secondary f6-s tc-link td-40 btn-link wine-card__button">Join Waitlist</button>
            <div class="js-waitlist-modal fixed top-0 left-0 flex justify-center items-center w-p-100 overlay waitlist-form">
              <div class="w-p-100 mxw-490 pv-xxl pv-xxxl-l ph-xl ph-xxl f-secondary bg-white waitlist-form">
                <div class="flex justify-end">
                  <button class="h5 icon-close js-close-waitlist"></button>
                </div>
                <div v65remotejs="form" class="remote-form" data-website="<?php echo get_field("store_url", "option");?>" data-formname="<?php echo get_field('waitlist_form', $wineID);?>" >&nbsp;</div>
              </div>
            </div>
          </div>
        </div>
      <?php endif; ?>
    </div>
  </div>
</article>
