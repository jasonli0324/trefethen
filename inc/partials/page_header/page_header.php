<?php 
  $title = !empty($title) ? $title : get_field('page_header_title');
  $intro = !empty($intro) ? $intro : get_field('page_header_intro');
?>
<div class="w-p-100 mxw-1952 mxw-1162-ds wrapper-small mh-auto ph-m pb-xl-l">
  <h1 class="<?php echo $intro ? 'mb-l-nl mb-m' : ''; ?> h1 f-title fw-300 tc-primary-text"><?php echo $title; ?></h1>
  <?php if (!empty($intro)) : ?>
    <div class="mxw-740 mb-xxl mb-l-m mb-xl-l tc-brown f3 f2-l intro-text"><?php echo $intro; ?></div>
  <?php endif; ?>
</div>