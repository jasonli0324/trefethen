<li class="w-p-100-nl mt-m-l mb-xxl mb-xxxl-l pb-m-l">
  <a href="<?php echo $url; ?>">
    <?php if ($thumbnail) : ?>
      <div class="mb-m bg-cover bg-center recipes-list__image" style="background-image: url('<?php echo $thumbnail; ?>')"></div>
    <?php endif; ?>
    <?php if($categories) : ?>
      <!-- <div class="mb-s">
        <?php foreach($categories as $category) : ?>
          <?php
            $cat_name = $category->name;
            $cat_url = get_term_link($category);
          ?>
          <a href="<?php echo $cat_url; ?>"  class="f-secondary f6-s fw-300 lh4 ls-tinier"><?php echo $cat_name; ?><?php if($j < $categories_amount) {echo ', '; } ?></a>
        <?php $j++; endforeach; ?>
      </div> -->
    <?php endif; ?>
    <?php if ($title) : ?>
      <h2 class="h5"><?php echo $title; ?></h2>
    <?php endif; ?>
  </a>
</li>