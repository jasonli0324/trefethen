<?php
  $label_title = $label['promo_slideshow_label_title'];
  $label_icon = $label['promo_slideshow_label_icon'];
?>
<li class="relative">
  <div class="w-p-100-nl bg-cover bg-center rotating-promo__image" style="background-image: url('<?php echo $image; ?>')"></div>
  <div class="absolute-l center-elem-v-l w-p-100-nl ml-m-nl ph-xl ph-xxl-l pv-xxl pv-xxxl-l bg-white rotating-promo__content">
    <p class="pv-m-nl pb-xl-l f-secondary uppercase f7 ls-small"><span class="pr-s f3 va-middle icon-<?php echo $label_icon; ?>"></span> <?php echo $label_title; ?></p>
    <?php if($title) : ?>
      <h2 class="mb-m mb-xl-l f2"><?php echo $title; ?></h2>
    <?php endif; ?>
    <?php if ($link) : ?>
      <a href="<?php echo $link_url; ?>" class="f-secondary lh4 fw-300 ls-tinier tc-link"><?php echo $link_title; ?></a>
    <?php endif; ?>
  </div>
</li>