<?php if($section_title) : ?>
  <div class="relative z-0 mxw-1952 mh-auto ph-m-ns">
    <p class="absoltue top-0 <?php if(isset($align_title) && $align_title === 'right') { echo 'ta-right'; }; ?> f-title fw-300 lh1 section-title tc-green-white">
      <?php echo $section_title; ?>
    </p>
  </div>
<?php endif; ?>