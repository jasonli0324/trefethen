<li class="w-p-100-nl <?php echo $amount > 3 ? 'mb-xxl mb-xxxl-l pb-m-l' : 'mb-xxl mb-xl-l'; ?> f-secondary">
  <?php if ($link) : ?>
    <a href="<?php echo $link_url; ?>" class="db mb-l mb-xxl-l bg-cover bg-center <?php echo $block_class_identifier; ?>__image" style="background-image: url('<?php echo $image; ?>')"></a>
  <?php else : ?>
    <div class="db mb-l mb-xxl-l bg-cover bg-center <?php echo $block_class_identifier; ?>__image" style="background-image: url('<?php echo $image; ?>')"></div>
  <?php endif; ?>
  <?php if ( $title ) : ?>
    <h2 class="mb-m lh2 ta-center-s <?php echo $is_double ? 'f-title h3 fw-300' : 'f-primary h5'; ?>"><?php echo $title; ?></h2>
  <?php endif; ?>
  <?php if ( $description ) : ?>
    <p class="<?php if ($is_double && $link) {echo 'mb-m'; }; ?> f6-s fw-300 lh4 ls-tinier"><?php echo $description; ?></p>
  <?php endif; ?>
  <?php if ( $is_double && $link_title ) : ?>
    <a href="<?php echo $link_url; ?>" class="relative dib f6-nl lh4 fw-300 h-tc-link-hover td-40 featured-link"><?php echo $link_title; ?><span class="pl-xs pl-s-ns tc-link icon-link-arrow"></span></a>
  <?php endif; ?>
</li>