<div class="<?php echo !$promotion_full_width && (isset($is_single_wine) && !$is_single_wine) ? 'mxw-1952 mh-auto ph-m-l' : ''; ?>">
  <section class="w-p-100 <?php echo $promotion_image ? 'pa-xl' : 'pv-xxl pv-xxxl-l' ?> <?php echo $promotion_full_width ? $block_class_identifier . '--full-width' : 'flex-ns items-center justify-between mxw-900'; ?> bg-taupe-bright <?php echo $promotion_image ? 'pa-xl' : 'ph-xxl ph-xxxl-l'; ?> <?php echo $block_class_identifier; ?>">
    <?php if($promotion_full_width) : ?>
      <div class="flex-ns items-center mxw-1952 mxw-1162-ds wrapper-small mh-auto ph-m">
    <?php endif; ?>
    <div class="flex-ns items-center">
      <?php if($promotion_image) : ?>
        <div class="mr-xl-ns mb-l-s bg-cover bg-center <?php echo $block_class_identifier; ?>__image" style="background-image: url('<?php echo $promotion_image; ?>')"></div>
      <?php endif; ?>
      <p class="mb-l-s <?php echo $promotion_full_width && !$promotion_image ? 'mr-xxxl-l' : ''; ?> <?php echo $promotion_image ? 'pl-s-ns' : 'pl-xxl-l'; ?> h4 f-title fw-300 tc-primary-text <?php echo $block_class_identifier; ?>__copy"><?php echo $promotion_copy; ?></p>
    </div>
    <a class="relative <?php if($promotion_full_width) { echo 'ml-m-l'; } ?> f-secondary f6-nl fw-300 ls-tinier featured-link" href="<?php echo $promotion_link_url; ?>"><?php echo $promotion_link_copy ? $promotion_link_copy : 'Learn More'; ?><span class="pl-s tc-link icon-link-arrow"></span></a>
    <?php if($promotion_full_width) : ?>
      </div>
    <?php endif; ?>
  </section>
</div>