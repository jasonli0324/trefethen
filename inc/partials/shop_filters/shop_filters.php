<div class="mb-xxl mb-xxxl-l">
  <div class="flex items-center f-secondary">
    <p class="dn-nl mr-s mr-m-l uppercase ls-medium filters__main-label">Filter by:</p>
    <button class="js-open-modal dn-l mb-m f6 ls-medium uppercase tc-primary-text"><span class="icon-plus"></span> Show Filters</button>
    <div class="js-filters-container fixed relative-l center-elem-nl w-p-100 bg-white of-hidden-nl o-0-nl filters__container" data-package="<?php if (isset($filter_bottle_cats) ) {echo $filter_bottle_cats; }?>" data-cat="<?php if (isset($filter_cats) ) {echo $filter_cats; }?>">
      <div class="flex justify-around-nl pt-xl-nl pb-l-nl pv-s-l ph-l ph-m-l">
        <?php if(isset($shop_filters)) : ?>
          <?php if(isset($recipes_filter)) : ?>
            <?php foreach($shop_filters as $filter) : ?>
              <?php 
                $filter_name = $filter->name;
                $filter_slug = $filter->slug;
                // $filter_items = get_term_children($filter->term_id, $filter->taxonomy);
                $filter_items = get_terms(array(
                  'taxonomy' => $filter->taxonomy,
                  'parent' => $filter->term_id,
                  'orderby' => 'term_order'
                 ));
              ?>
              <div class="js-individual-filter z-1 mr-xl-l" data-recipeParentCat="<?php if(isset($current_cat_parent)) {echo $current_cat_parent; } ?>" data-recipeCat="<?php if (isset($current_recipe_cat)) { echo $current_recipe_cat; } ?>">
                <button class="js-wine-filter relative z-4 f7-s f5-m lh4 tc-primary-text h-tc-link td-40 filters__label"><?php echo $filter_name; ?><span class="dn-nl icon-caret"></span></button>
                <div class="js-filter-dropdown absolute left-0 z-3 w-p-100 bg-white of-hidden td-40 filters__select-box">
                  <ul class="w-p-100 pv-l-l ph-l ph-xl-l">
                    <?php foreach ($filter_items as $item) : ?>
                      <?php 
                        $item_name = get_term( $item )->name; 
                        $item_slug = get_term( $item )->slug; 
                      ?>
                      <li class="js-recipe-filter dib w-p-49 mb-m ph-s-nl pb-xs f6-s fw-300 td-40 h-tc-link pointer" data-parent="<?php echo $filter_slug; ?>" data-id="<?php echo $item_slug;?>">
                        <?php echo $item_name;?>
                      </li>
                    <?php endforeach; ?>
                  </ul>
                </div>
              </div>

            <?php endforeach; ?>
          <?php else :?>

            <?php if(in_array('Type', $shop_filters) || (isset($is_types) && $is_types)) : ?>
              <div class="js-individual-filter z-1 mr-xl-l">
                <button class="js-wine-filter relative z-4 f7-s f5-m lh4 tc-primary-text td-40 h-tc-link filters__label">Category<span class="dn-nl icon-caret"></span></button>
                <div class="js-filter-dropdown absolute left-0 z-3 w-p-100 bg-white of-hidden td-40 filters__select-box">
                  <ul class="w-p-100 pv-l-l ph-l ph-xl-l">
                    <?php foreach ($wine_types as $wine_type) : ?>
                      <li class="js-type-varietal-category js-type-filter dib w-p-49 mb-m ph-s-nl pb-xs f6-s fw-300 td-40 h-tc-link pointer" data-id="<?php echo $wine_type;?>">
                        <?php echo $wine_type;?>
                      </li>
                    <?php endforeach; ?>
                  </ul>
                </div>
              </div>
            <?php endif; ?>
            
            <?php if (in_array('Varietal', $shop_filters) || ( (isset($$is_vintage) && $is_vintage) || (isset($$is_types) && $is_types) )) : ?>
              <div class="js-individual-filter z-1 mr-xl-l">
                <button class="js-wine-filter relative z-4 f7-s f5-m lh4 tc-primary-text td-40 h-tc-link filters__label">Varietal<span class="dn-nl icon-caret"></span></button>
                <div class="js-filter-dropdown absolute left-0 z-3 w-p-100 bg-white of-hidden td-40 filters__select-box">
                  <ul class="w-p-100 pv-l-l ph-l ph-xl-l">
                    <?php foreach ($wine_varietals as $wine_varietal) : ?>
                      <li class="js-type-varietal-category js-varietal-filter dib w-p-49 mb-m ph-s-nl pb-xs f6-s fw-300 td-40 h-tc-link pointer" data-key="<?php echo $wine_varietal['key'];?>" data-id="<?php echo $wine_varietal['title'];?>">
                        <?php echo $wine_varietal['title'];?>    
                      </li>
                    <?php endforeach; ?>
                  </ul>
                </div>
              </div>
            <?php endif; ?>
            
            <?php if (in_array('Size', $shop_filters) || ( (isset($$is_vintage) && $is_vintage) || (isset($$is_types) && $is_types) || (isset($is_varietals) && $is_varietals) )) : ?>
              <div class="js-individual-filter z-1 mr-xl-l">
                <button class="js-wine-filter relative z-4 f7-s f5-m lh4 tc-primary-text td-40 h-tc-link filters__label">Size<span class="dn-nl icon-caret"></span></button>
                <div class="js-filter-dropdown absolute left-0 z-3 w-p-100 bg-white of-hidden td-40 filters__select-box">
                  <ul class="w-p-100 pv-l-l ph-l ph-xl-l">
                    <?php foreach ($wine_sizes as $wine_size) : ?>
                      <li class="js-size-category dib w-p-49 mb-m ph-s-nl pb-xs f6-s fw-300 td-40 h-tc-link pointer" data-id="<?php echo $wine_size;?>">
                        <?php echo $wine_size;?>    
                      </li>
                    <?php endforeach; ?>
                  </ul>
                </div>
              </div>
            <?php endif; ?>
            
            <?php if (in_array('Vintage', $shop_filters) || ((isset($is_types) && $is_types) || (isset($is_varietals) && $is_varietals)) ) : ?>
              <div class="js-individual-filter z-1 mr-xl-l">
                <button class="js-wine-filter relative z-4 f7-s f5-m lh4 tc-primary-text td-40 h-tc-link filters__label">Vintage<span class="dn-nl icon-caret"></span></button>
                <div class="js-filter-dropdown absolute left-0 z-3 w-p-100 bg-white of-hidden td-40 filters__select-box">
                  <ul class="w-p-100 pv-l-l ph-l ph-xl-l">
                    <?php foreach ($vintages as $vintage) : ?>
                      <li class="js-vintage-category dib w-p-49 mb-m ph-s-nl pb-xs f6-s fw-300 td-40 h-tc-link pointer" data-id="<?php echo $vintage;?>">
                        <?php echo $vintage;?>
                      </li>
                    <?php endforeach; ?>
                  </ul>
                </div>
              </div>
            <?php endif; ?>
            
            <?php if (in_array('Price', $shop_filters) || ((isset($is_types) && $is_types) || (isset($is_varietals) && $is_varietals) || (isset($is_varietals) && $is_varietals) ) ) : ?>
              <div class="js-individual-filter z-1 mr-xl-l">
                <button class="js-wine-filter relative z-4 f7-s f5-m lh4 tc-primary-text td-40 h-tc-link filters__label">Price<span class="dn-nl icon-caret"></span></button>
                <div class="js-filter-dropdown absolute left-0 z-3 w-p-100 bg-white of-hidden td-40 filters__select-box filters__select-box-price">
                  <div class="w-p-100 mb-xxl mb-m-l pt-l-l ph-xl">
                    <div class="mb-xl ph-s-nl ph-m">
                      <div id="rangeSlider" data-min="<?php echo $min;?>" data-max="<?php echo $max;?>"></div>
                    </div>
                    <p id="amount" class="lh4 ta-center fw-300 ls-tinier">$<?php echo $min;?> - $<?php echo $max;?></p>
                  </div>
                  <div class="dn-l ta-center">
                    <button class="js-close-modal w-p-100 bdr-3 f-secondary f6-s tc-link td-40 btn-link filters__btn">Submit</button>
                  </div>
                </div>
              </div>
            <?php endif; ?>
          <?php endif; ?>

        <?php endif; ?>
        
        <button class="js-close-modal absolute bottom-24 center-elem-h z-1 dn-l f6 tc-primary-text ls-tinier"><span class="pr-s fw-800 icon-close"></span> Close Filters</button>
      </div>
    </div>
    <span class="js-bg-modal fixed top-0 left-0 dn-l w-p-100 h-p-100 td-60 filters__modal-bg"></span>
  </div>
  <div class="flex items-center justify-between-nl f-secondary">
    <div class="js-filtering-category-contatiner flex"></div>
    <button class="js-clear-filter dn f7 lh4 ls-tiny tc-primary-text">Reset Filters</button>
  </div>
</div>