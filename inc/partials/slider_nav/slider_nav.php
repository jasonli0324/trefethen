<?php
  $is_horizontal = isset($nav_horizontal) ? $nav_horizontal : false;
?>
<div class="js-slider-nav dn-nl pa-m f-secondary f7 bg-white slider-nav <?php if($is_horizontal) { echo 'slider-nav--h flex-l items-center'; }; ?> <?php echo $block_class_identifier; ?>__nav">
  <button class="js-slider-prev icon-promo-arrow tc-brown"></button>
  <p class="<?php echo $is_horizontal ? 'mh-l' : 'mv-m' ?> tc-brown"><span class="js-current-slide">1</span> / <span class="js-total-slides"><?php echo $slides; ?></span></p>
  <button class="js-slider-next icon-promo-arrow tc-brown"></button>
</div>