<?php
  $photos = get_field('visit_experience_photos', $itemID);
  $thumbnail = $photos['visit_experience_thumbnail_photo']['url'];
  $title = get_field('visit_experience_title', $itemID);
  $description = get_field('visit_experience_description', $itemID, false);
  $price = get_field('visit_experience_cost_summary', $itemID);
  $price_details = get_field('visit_experience_payment_details', $itemID);
  $winedirect_tock = get_field('visit_experience_use_winedirect_tock', $itemID);
  $reservation_link = $winedirect_tock ? get_field('reservation_link', 'options') : get_field('visit_experience_reservation_link', $itemID);
?>
<article class="flex-ns <?php echo $has_mb ? 'mb-xxl mb-xxxl-l pb-m-l' : ''; ?> f-secondary <?php echo $block_class_identifier; ?>">
  <div class="db mr-m-ns mb-l-s shrink-0 bg-center bg-cover <?php echo $block_class_identifier; ?>__image" style="background-image: url('<?php echo $thumbnail; ?>')"></div>
  <div class="mxw-490 pl-s-ns">
    <h2 class="mb-m f-title fw-300 h3"><?php echo $title; ?></h2>
    <p class="mb-l mb-xl-l f6-nl fw-300 lh4 ls-tiny"><?php echo $description; ?></p>
    <div class="flex-l items-center">
      <?php 
        $reservation_link_type = $reservation_link['visit_experience_reservation_link_type'];
        if ($reservation_link_type === 'email') {
          $reservation_url = 'mailto:' . $reservation_link['experience_reservation_email'];
        } elseif ($reservation_link_type === 'phone') {
          $reservation_url = 'tel:' . $reservation_link['experience_reservation_phone'];
        } elseif ($reservation_link_type === 'url') {
          $reservation_url = $reservation_link['experience_reservation_url'];
        } else {
          $reservation_url = '#';
        }
      ?>
      <a href="<?php echo $reservation_url; ?>" target="blank" class="shrink-0 db mb-m-nl mr-xs-l pv-s ph-m bdr-3 tc-white ta-center bg-link h-bg-link td-40 <?php echo $block_class_identifier; ?>__btn-link">Reserve Now</a>
      <div class="pl-m-l">
        <p class="f6-s lh4 ls-tinier"><?php echo $price; ?></p>
        <p class="f7"><?php echo $price_details; ?></p>
      </div>
    </div>
  </div>
</article>