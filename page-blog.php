<?php 
	/**
	* Template Name: Blog Layout
	*
	* This is the template used for Shop page
	*/

	get_header();
?>

<section class="mb-xxxl-l pb-xxl pb-xxxl-l posts-list">
	<div class="mxw-1952 mxw-1162-ds wrapper-small mh-auto ph-m mb-xl">
		<?php custom_breadcrumbs(); ?>
	</div>
	<?php include( locate_template( 'inc/partials/page_header/page_header.php', false, false ) ); ?>
	<div class="pt-m">
		<?php
			$post_args = array (
				'post_type' => 'post',
				'posts_per_page' => -1,
			);
			$posts = new WP_Query($post_args);

			if ($posts->have_posts()) :
		?>
			<?php while($posts->have_posts()) : $posts->the_post(); ?>
				<?php 
					$id = get_the_ID();
					$url = get_permalink();
					$photo = get_field('blog_post_photo')['url'];
					$title = get_the_title();
					$date = get_the_date();
					$cats = wp_get_post_categories( $id );
					$cat_count = count($cats);
					$flexible_content = get_field('additional_flexible_content')['content_blocks'];
					foreach($flexible_content as $content) {
						if ($content['acf_fc_layout'] === 'text_block' && $content['text_block_header_text']) {
							$intro = $content['text_block_header_text'];
							break;
						}
					}
					$k = 1;
				?>
				<article class="mb-xxl mb-xxxl-l pb-m-l">
					<div class="mxw-1952 mh-auto mb-m mb-xl-l ph-m">
						<?php if($photo) : ?>
							<div class="bg-cover bg-center posts-list__image" style="background-image: url('<?php echo $photo; ?>')"></div>
						<?php else : ?>
							<div class="bdb-1 posts-list__border"></div>
						<?php endif; ?>
					</div>
					<div class="mxw-1952 mxw-1162-ds wrapper-small mh-auto ph-m">
						<div class="mxw-670">
							<?php if ($title) : ?>
								<h2 class="mb-m f-title h2"><?php echo $title; ?></h2>
							<?php endif; ?>
							<?php if($cat_count > 0) : ?>
								<ul class="f-secondary lh4 ls-tinier">
									<?php foreach($cats as $cat) : ?>
										<?php $cat_name = get_category($cat)->name; ?>
										<li class="dib"><?php echo $cat_name; ?><?php if($k < $cat_count) echo ', '?></li>
									<?php $k++; endforeach; ?>
								</ul>
							<?php endif; ?>
							<?php if ($date) : ?>
								<p class="mb-m f-secondary f6-s lh4 fw-300 ls-tinier"><?php echo $date; ?></p>
							<?php endif; ?>
							<?php if ($intro) : ?>
								<div class="mb-m mb-xl-l tc-brown f3 f2-l"><?php echo $intro; ?></div>
							<?php endif; ?>
							<?php if ($url) : ?>
								<a href="<?php echo $url; ?>" class="relative dib f-secondary lh4 fw-300 h-tc-link-hover td-40 featured-link">Read<span class="pl-xs pl-s-ns tc-link icon-link-arrow"></span></a>
							<?php endif; ?>
						</div>
					</div>
				</article>
			<?php 
				$intro = ''; 
				endwhile; 
			wp_reset_postdata();
			endif; 
			?>
	</div>
</section>
<?php get_footer();?>